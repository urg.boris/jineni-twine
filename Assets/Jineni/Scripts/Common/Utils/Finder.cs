﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine.Assertions;
using Jineni.DialogSystem.Graph;

namespace Jineni.Utils
{

    //[ExecuteInEditMode] - if all works delete
    public static class Finder
    {
        public static EntryPoint[] GetAllEntryPoints()
        {
            return Resources.LoadAll<EntryPoint>("Dialogs");
        }
        public static MultiOption[] GetAllMultiOptions()
        {
            return Resources.LoadAll<MultiOption>("Dialogs");
        }


#if UNITY_EDITOR
        public static SceneAsset[] GetAllScenes()
        {
            string[] guids = AssetDatabase.FindAssets("t: SceneAsset");
            SceneAsset[] foundInstances = new SceneAsset[guids.Length];
            string path;
            for (int i = 0; i < guids.Length; i++)         //probably could get optimized 
            {
                path = AssetDatabase.GUIDToAssetPath(guids[i]);
                foundInstances[i] = AssetDatabase.LoadAssetAtPath<SceneAsset>(path);
            }
            return foundInstances;
        }

        public static T[] GetScriptableObjects<T>() where T : ScriptableObject
        {
            string[] guids = AssetDatabase.FindAssets("t:" + typeof(T).Name);  //FindAssets uses tags check documentation for more info
            //Debug.Log("guids"+ guids.Length);
            T[] foundInstances = new T[guids.Length];
            
            for (int i = 0; i < guids.Length; i++)         //probably could get optimized 
            {
                
                string path = AssetDatabase.GUIDToAssetPath(guids[i]);
                //Debug.Log(path + "  " + guids[i]);
                foundInstances[i] = AssetDatabase.LoadAssetAtPath<T>(path);
            }

            return foundInstances;
        }

        public static T[] GetSubScriptableObjects<T>() where T : ScriptableObject
        {
            //!!TODO tohle je spravna cesta jak nacitat scriptable sub typy, pravdepodobne i hlavni typy
            T[] foundTypedInstances = Resources.LoadAll<T>("Dialogs");
            return foundTypedInstances;
        }

        public static T[] GetScriptableObjectsWithout<T>(T[] skip) where T : ScriptableObject
        {
            string[] guids = AssetDatabase.FindAssets("t:" + typeof(T).Name);  //FindAssets uses tags check documentation for more info
            //Debug.Log("guids"+ guids.Length);
            List<T> foundInstances = new List<T>();//T[guids.Length];
            T instance;
            string path;
            bool saveInstance;
            for (int i = 0; i < guids.Length; i++)         //probably could get optimized 
            {
                path = AssetDatabase.GUIDToAssetPath(guids[i]);
                instance = AssetDatabase.LoadAssetAtPath<T>(path);
                saveInstance = true;
                for (int j = 0; j < skip.Length; j++)
                {
                    if (instance == skip[j])
                    {
                        saveInstance = false;
                    }
                }
                if (saveInstance)
                {
                    foundInstances.Add(AssetDatabase.LoadAssetAtPath<T>(path));
                }
            }
            return foundInstances.ToArray();
        }


        public static T GetLoneScriptableObjects<T>() where T : ScriptableObject
        {
            string[] guids = AssetDatabase.FindAssets("t:" + typeof(T).Name);
            //Debug.Log(typeof(T).Name+ "  "+guids.Length);

            if(guids.Length==0)
            {
                return null;
            } else if (guids.Length==1)
            {
                T instance = null;
                string path = AssetDatabase.GUIDToAssetPath(guids[0]);
                instance = AssetDatabase.LoadAssetAtPath<T>(path);
                return instance;
            } else
            {
                Assert.IsTrue(false, "more scriptable object instances of " + typeof(T).Name + ", found -" + guids.Length);
                return null;
            }

            
        }

        public static bool ExistsNameAndType<T>(string name)
        {
            string[] guids = AssetDatabase.FindAssets(name+" t:" + typeof(T).Name);
            //Debug.Log(typeof(T).Name+ "  "+guids.Length);

            if (guids.Length == 0)
            {
                return false;
            } else
            {
                return true;
            }
        }
#endif
    }

}
