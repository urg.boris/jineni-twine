﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Jineni.Game.Paralax
{
    public class ParalaxMover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public ParalaxManager paralaxManager;
        public ParalaxManager.MoveTo moveTo;

        //Detect if the Cursor starts to pass over the GameObject
        public void OnPointerEnter(PointerEventData pointerEventData)
        {
            paralaxManager.moveTo = moveTo;
        }

        //Detect when Cursor leaves the GameObject
        public void OnPointerExit(PointerEventData pointerEventData)
        {
            paralaxManager.moveTo = ParalaxManager.MoveTo.nowhere;
        }
    }
}
