﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Jineni.Game.Paralax
{
    [System.Serializable] 
    public class ParalaxPlain
    {
        public Transform parent;
        public float leftLimit;
        public float rightLimit;
        public float width;
    }

    public class ParalaxManager : MonoBehaviour
    {
        //todo center before?
        [Range(0, 1f)]
        public float paralaxPosition;

        public SpriteRenderer[] spritesForParalax;
        ParalaxPlain[] paralaxPlains;

        public float movementSpeed;

        [HideInInspector]
        public MoveTo moveTo;
        public enum MoveTo
        {
            left,
            right,
            nowhere
        }

        void Start()
        {
            float screenHeightInUnits = Camera.main.orthographicSize * 2;
            float screenWidthInUnits = screenHeightInUnits * Screen.width / Screen.height;

            Assert.IsNotNull(spritesForParalax);
            float spriteWidth;
            float limitOffset;

            paralaxPlains = new ParalaxPlain[spritesForParalax.Length];
            Vector3 newPos;
            for (int i = 0; i < spritesForParalax.Length; i++)
            {
                //!! set x to center for corect function
                newPos = spritesForParalax[i].transform.position;
                newPos.x = 0;
                spritesForParalax[i].transform.position = newPos;


                spriteWidth = spritesForParalax[i].bounds.size.x;
                limitOffset = (spriteWidth - screenWidthInUnits) / 2;

                Assert.IsTrue(spriteWidth >= screenWidthInUnits);
                paralaxPlains[i] = new ParalaxPlain
                {
                    parent = spritesForParalax[i].transform.parent,
                    width = spriteWidth,
                    leftLimit = limitOffset,
                    rightLimit = -limitOffset
                };
            }
            paralaxPosition = 0.5f;
            UpdateParalax();
        }

        public void MoveLeft()
        {
            paralaxPosition -= movementSpeed*Time.deltaTime;
            UpdateParalax();
        }

        public void MoveRight()
        {
            paralaxPosition += movementSpeed * Time.deltaTime;
            UpdateParalax();
        }

        void UpdateParalax()
        {
            paralaxPosition = Mathf.Clamp01(paralaxPosition);
            //Debug.Log(paralaxPosition);
            Vector3 newPos;
            for (int i = 0; i < paralaxPlains.Length; i++)
            {
                newPos = paralaxPlains[i].parent.position;
                newPos.x = Mathf.Lerp(paralaxPlains[i].leftLimit, paralaxPlains[i].rightLimit, paralaxPosition);
                paralaxPlains[i].parent.position = newPos;
            }
        }

        // Update is called once per frame
        void Update()
        {
            switch (moveTo)
            {
                case MoveTo.left:
                    MoveLeft();
                    break;
                case MoveTo.right:
                    MoveRight();
                    break;
                case MoveTo.nowhere:
                    break;
                default:
                    break;
            }
        }
    }
}
