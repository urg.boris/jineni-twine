﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using Jineni.DialogSystem.Graph;

namespace Jineni.Game.GameStateBlocks
{
    //[CreateAssetMenu(fileName = "EP", menuName = "Jineni/GameState/new EntryPointState", order = 6)]
    [System.Serializable]
    public class EntryPointState
    {
        //TODO AssetDatabase.FindAssets("co l:architecture t:texture2D", "p1-EP-2 t:entryPoint" - chech if one
        public string id;

        public bool IsNotEmpty() {
            return id != "";
        }
    }
}
