﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using Jineni.DialogSystem.Graph;

namespace Jineni.Game.GameStateBlocks
{
    [System.Serializable]
    public class InventoryState
    {
        public int oddness;
        public int[] itemsInInventory;

    }
}
