﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using Jineni.DialogSystem.Graph;
using System;

namespace Jineni.Game.GameStateBlocks
{
    [Serializable]
    public class CharacterStateTransformIndex
    {
        public int characterId;
        public int transformId;
    }
    [Serializable]
    public class CharacterStateActivityIndex
    {
        public int characterId;
        public int activitiyId;
    }

    [Serializable]
    public class LocationState
    {
        public string editorName;
        public int id;
        public int nameIndex = 0;
        public bool isOpened;
        public EntryPointState onOpenEntryPoint;

        [SerializeField]
        private List<CharacterStateTransformIndex> charactersTransformsIndexes;
        [SerializeField]
        private List<CharacterStateActivityIndex> charactersActivitiesIndexes;

        public List<CharacterStateTransformIndex> CharacterTransformIndices
        {
            get
            {
                return charactersTransformsIndexes;
            }
            set
            {
                charactersTransformsIndexes = value;
            }
        }

        public List<CharacterStateActivityIndex> CharactersActivitiesIndexes
        {
            get
            {
                return charactersActivitiesIndexes;
            }
            set
            {
                charactersActivitiesIndexes = value;
            }
        }
    }
}
