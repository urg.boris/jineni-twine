﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using Jineni.DialogSystem.Graph;

namespace Jineni.Game.GameStateBlocks
{
   [System.Serializable]
    public class CharacterState 
    {
        public string editorName;
        public int id;
        public int nameIndex = 0;

        public bool visible;
        public bool mouseInteractive;
        public bool talkative;

        public EntryPointState dialogEntryPoint;
        public EntryPointState examineEntryPoint;
    }
}
