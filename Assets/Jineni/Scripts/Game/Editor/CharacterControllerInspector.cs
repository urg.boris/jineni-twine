﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Jineni.DialogSystem.Graph;
using Jineni.DialogSystem;
using UnityEngine.Assertions;

namespace Jineni.Game.BuildingBlocks
{
    [CustomEditor(typeof(CharacterController))]
    public class CharacterControllerInspector : Editor
    {
        CharacterController characterController;
        CharacterCore character;
        GUIStyle gs = new GUIStyle();

        /*
        SerializedProperty characterField;
        SerializedProperty animatorField;
        SerializedProperty boxColliderField;
        SerializedProperty activitiesField;
        SerializedProperty transformsField;*/

        void OnEnable()
        {
            characterController = serializedObject.targetObject as CharacterController;
            character = characterController.character;
            gs.wordWrap = true;
        }

        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Check Integrity"))
            {

                Assert.IsNotNull(characterController);
                Assert.IsNotNull(characterController.character);

                Siren.EditorQuestion("check character " + characterController.character.GetEditorName() + " integrity");
                if(characterController.IsCorrupted())
                {
                    EditorGUIUtility.PingObject(characterController);
                    Assert.IsTrue(false, character.editorName+" - " +characterController.corruptionReport);
                    return;
                }
                Siren.EditorOkLog("character " + character.editorName + " integrity looks good");
            }

            if (character != null)
            {
                //EditorGUILayout.BeginHorizontal();
                //EditorGUILayout.BeginVertical();
                EditorGUIUtility.labelWidth = 200;
                EditorGUI.BeginChangeCheck();
                character.currentDialogEntryPoint = EditorGUILayout.ObjectField("Current dialog entry point", character.currentDialogEntryPoint, typeof(EntryPoint), false) as EntryPoint;
                if (EditorGUI.EndChangeCheck())
                {
                    EditorUtility.SetDirty(character);
                }
                //EditorGUILayout.EndVertical();

                //EditorGUILayout.BeginHorizontal();
               // EditorGUILayout.BeginVertical();
                EditorGUIUtility.labelWidth = 200;
                EditorGUI.BeginChangeCheck();
                character.currentExamineEntryPoint = EditorGUILayout.ObjectField("Current examine entry point", character.currentExamineEntryPoint, typeof(EntryPoint), false) as EntryPoint;
                if (EditorGUI.EndChangeCheck())
                {
                    EditorUtility.SetDirty(character);
                }
                //EditorGUILayout.EndVertical();

                if (GUILayout.Button("Open current dialog"))
                {
                    DialogEditor.OpenEditor(character.currentDialogEntryPoint.parentGraph);
                }

                int activititiesCount = characterController.HowManyActivitiesAreThere();
                if (activititiesCount > 0)
                {
                    EditorGUILayout.LabelField("There Are " + activititiesCount + " Possible Activities");
                }
                int transformsCount = characterController.HowManyTransformsAreThere();
                if (transformsCount > 0)
                {
                    EditorGUILayout.LabelField("There Are " + transformsCount + " Possible Transforms");
                } 

                if(activititiesCount==0 && transformsCount==0)
                {
                    EditorGUILayout.LabelField("There Are No Possible Activities And\n No Possible Transforms - is character static?", gs);
                }
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("Set Core Active"))
                {
                    character.SetActiveState(true, true);
                }
                if (GUILayout.Button("Set Core Inactive"))
                {
                    character.SetActiveState(false, true);
                }
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("Set Current Transform"))
                {
                    characterController.SetCurrentTransform();
                }
                if (GUILayout.Button("Set Default Transform [0]"))
                {
                    characterController.SetDefaultTransform();
                }
                EditorGUILayout.EndHorizontal();
            }

            DrawDefaultInspector();

            if (character != null)
            {


                EditorGUILayout.LabelField("!!Click Save Possibilities After Edit!!", EditorStyles.boldLabel);
                GUI.skin.button.fontStyle = FontStyle.Bold;
                if (GUILayout.Button("SAVE POSSIBILITIES"))
                {
                    serializedObject.ApplyModifiedProperties();
                    LocationController.Instance.location.SaveStorages(character, characterController.transformsStorage, characterController.activitiesStorage);
                }
                GUI.skin.button.fontStyle = FontStyle.Normal;

                if (GUILayout.Button("Add Current Transform As New Possible Transform"))
                {
                    LocationController.Instance.location.AddNewTransform(character, characterController.transform);
                    characterController.UpdateLists();
                }
            }
        }
    }
}