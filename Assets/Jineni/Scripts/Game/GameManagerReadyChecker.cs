﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;

namespace Jineni.Game
{
    public class GameManagerReadyChecker : MonoBehaviour
    {

        public GameObject gameManagerPrefab;
        public GameObject inventoryMenuPrefab;
        public GameObject dialogMenuPrefab;
        public GameObject mapMenuPrefab;


        void Awake()
        {
            GameObject singleton;

            if (GameManager.Instance == null)
            {
                singleton = Instantiate(gameManagerPrefab);
                singleton.name = typeof(GameManager).Name.ToString() + " (Singleton)";
            }


            if (InventoryManager.Instance == null) {
                singleton = Instantiate(inventoryMenuPrefab);
                singleton.name = typeof(InventoryManager).Name.ToString() + " (Singleton)";
            }

            if (DialogManager.Instance == null)
            {
                singleton = Instantiate(dialogMenuPrefab);
                singleton.name = typeof(DialogManager).Name.ToString() + " (Singleton)";
            }

            if (MapManager.Instance == null)
            {
                singleton = Instantiate(mapMenuPrefab);
                singleton.name = typeof(MapManager).Name.ToString() + " (Singleton)";
            }

            
        }
    }
}
