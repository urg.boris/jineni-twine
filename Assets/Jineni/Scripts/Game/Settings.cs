﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Utils;

namespace Jineni.Game
{
    public enum Lang
    {
        cs,
        en
    };
    [CreateAssetMenu(fileName = "Settings", menuName = "Jineni /Internal/Base/Settings", order = 61)]
    public class Settings : ScriptableObject
    {
        public Lang currentLang;
        public float speechVolume;
        public float musicVolume;
        //TODO co jeste



    }
}
