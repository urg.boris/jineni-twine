﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Assertions;

namespace Jineni.Game.BuildingBlocks
{
    public class LocationInMenu: MonoBehaviour
    {
        public LocationCore location;
        public Button button;

        private void Awake()
        {
            Assert.IsNotNull(button);
            Assert.IsNotNull(location);
        }

        private void OnEnable()
        {
            //Debug.Log("on enable"+name);
            button.onClick.AddListener(delegate { ClickOnLocation(); });
        }

        void ClickOnLocation()
        {
            //TODO loading lokace
            GameManager.Instance.VisitLocation(location);
            MapManager.Instance.Close();
        }

        private void OnDisable()
        {
            //Debug.Log("on disable" + name);
            button.onClick.RemoveAllListeners();
        }
    }
}
