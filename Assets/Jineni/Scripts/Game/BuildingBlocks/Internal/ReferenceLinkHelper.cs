﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.DialogSystem;
using UnityEngine.Serialization;
using Jineni.DialogSystem.Graph;
using Jineni.Game.BuildingBlocks;
using UnityEngine.Assertions;

namespace Jineni.Game.BuildingBlocks.Internals
{
    [Serializable]
    public class CharacterCharToCharacter
    {
        public string charString;
        public CharacterCore character;
    }
    [CreateAssetMenu(fileName= "ReferenceLinkHelper", menuName = "Jineni/Internal/ReferenceLinkHelper", order = 80)]
    public class ReferenceLinkHelper : ScriptableObject
    {

        [Header("CoreGameObjects")]
        public Inventory inventory;
        [Header("Dialog")]
        [FormerlySerializedAs("player")]
        public CharacterCore playerCharacter;
        [FormerlySerializedAs("enviroment")]
        public CharacterCore enviromentCharacter;
        [Header("Default Item/characters Entry Points")]
        public EntryPoint defaultNotWorkingCombinationEntryPoint;
        public EntryPoint defaultItemExaminationEntryPoint;
        public EntryPoint defaultCharacterExaminationEntryPoint;
        public Settings settings;
        public List<CharacterCharToCharacter> CharToCharacters;

        public CharacterCore GetCharacterByChar(string charString)
        {
            //CharacterCore character = null;
            foreach (var characterInList in CharToCharacters)
            {
                if (characterInList.charString == charString)
                {
                    return characterInList.character;
                }
            }

            return enviromentCharacter;
        }

        //TODO budou tady asi i odkazy na vsechny lokace? predmety? postavy? 

        private void Awake()
        {
            Assert.IsNotNull(playerCharacter);
            Assert.IsNotNull(enviromentCharacter);
            Assert.IsNotNull(defaultNotWorkingCombinationEntryPoint);
            Assert.IsNotNull(defaultItemExaminationEntryPoint);
            Assert.IsNotNull(defaultCharacterExaminationEntryPoint);
            Assert.IsNotNull(settings);
        }
    }
}

