﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using Jineni.DialogSystem.Graph;
using UnityEngine.Serialization;


namespace Jineni.Game.BuildingBlocks.Internals
{
    public abstract class ItemCombination : ScriptableObject {
        
        [FormerlySerializedAs("item1")]
        public ItemCore object1;
        public EntryPointLinkable object2;

        public EntryPoint entryPoint;
        [HideInInspector]
        public string corruptionReport;

        public bool IsCorrupted()
        {
            if(object1 == null)
            {
                corruptionReport = "source item not set";
                return true;
            }
            if (object2 == null)
            {
                corruptionReport = "reacting object not set";
                return true;
            }
            if (entryPoint == null)
            {
                corruptionReport = "entryPoint for combination not set";
                return true;
            }

            return false;
        }

    }
}
