﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using Jineni.DialogSystem.Graph;
using UnityEngine.Serialization;

namespace Jineni.Game.BuildingBlocks.Internals
{
    [CreateAssetMenu(fileName = "ItemCombinationsManager", menuName = "Jineni/Internal/Base/ItemCombinationsManager", order = 31)]
    public class ItemCombinationsManager : ScriptableObject {
        [FormerlySerializedAs("itemCombinations")]
        public List<ItemItemCombination> itemEntries = new List<ItemItemCombination>(100);
        [FormerlySerializedAs("itemCharacterCombinations")]
        public List<ItemCharacterCombination> characterEntries = new List<ItemCharacterCombination>(100);
        public ReferenceLinkHelper referenceLinkHelper;

        [HideInInspector]
        public int lastEntryIndex;


        public EntryPoint GetEntryPointFromEntryBy(ItemCore item1, ItemCore item2)
        {
            for (int i = 0; i < itemEntries.Count; i++)
            {
                if(itemEntries[i].object1 == item1 && itemEntries[i].object2 == item2)
                {
                    return itemEntries[i].entryPoint;
                }
                if(itemEntries[i].object1 == item2 && itemEntries[i].object2 == item1)
                {
                    return itemEntries[i].entryPoint;
                }
            }
            return referenceLinkHelper.defaultNotWorkingCombinationEntryPoint;
        }

        public EntryPoint GetEntryPointFromEntryBy(ItemCore item1, CharacterCore item2)
        {
            for (int i = 0; i < characterEntries.Count; i++)
            {
                if (characterEntries[i].object1 == item1 && characterEntries[i].object2 == item2)
                {
                    return characterEntries[i].entryPoint;
                }
                if (characterEntries[i].object1 == item2 && characterEntries[i].object2 == item1)
                {
                    return characterEntries[i].entryPoint;
                }
            }
            return referenceLinkHelper.defaultNotWorkingCombinationEntryPoint;
        }

        public List<ItemItemCombination> GetFilledOrUnfilledEntriesBy(ItemCore item)
        {
            List<ItemItemCombination> selectedItemCombinations = new List<ItemItemCombination>();
            for (int i = 0; i < itemEntries.Count; i++)
            {
                if(itemEntries[i].object1 == item || itemEntries[i].object2 == item || itemEntries[i].object1==null || itemEntries[i].object2==null)
                {
                    selectedItemCombinations.Add(itemEntries[i]);
                }
            }
            return selectedItemCombinations;
        }

        public List<ItemCharacterCombination> GetFilledOrUnfilledEntriesBy(CharacterCore character)
        {
            List<ItemCharacterCombination> selectedItemCombinations = new List<ItemCharacterCombination>();
            for (int i = 0; i < characterEntries.Count; i++)
            {
                if (characterEntries[i].object2 == character || characterEntries[i].object1 == null || characterEntries[i].object2 == null)
                {
                    selectedItemCombinations.Add(characterEntries[i]);
                }
            }
            return selectedItemCombinations;
        }


        
    }
}
