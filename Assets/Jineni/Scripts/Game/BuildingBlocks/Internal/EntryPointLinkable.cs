﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Jineni.Game.BuildingBlocks.Internals
{
    /// <summary>
    /// this object or combination with other object will point to some entry point
    /// </summary>
    public abstract class EntryPointLinkable : CoreBuildingBlock
    {
        //!!does nothing just for editor targeting
    }
}
