﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using UnityEngine.Assertions;
using UnityEngine.Serialization;


namespace Jineni.Game.BuildingBlocks
{
    public class MapManager : MonoBehaviour, IStateResetable
    {
        public GameManager gameManager;
        //TODO zatim je map object useles, vse v locationsOnMap, ale otevirani lokaci bude asi map
        [FormerlySerializedAs("map")]
        //public World world;
        public Canvas menu;

        public List<LocationInMenu> locationsOnMap;
        [HideInInspector]
        public bool isOpened = false;
        [HideInInspector]
        public bool mapChanged = false;

        static MapManager _instance = null;
        public static MapManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<MapManager>();
                }
                //!! must remain null if doesnt exists, checker will create it
                return _instance;
            }
        }

        private void Awake()
        {
            //Assert.IsNotNull(world);
            Assert.IsNotNull(menu);
            //Assert.IsTrue(world.locations.Count > 0);

            if (_instance == null)
            {
                _instance = this;
            }
            else if (_instance != this)
            {
                Destroy(gameObject);
            }
            //!!is part of GameManager
            //DontDestroyOnLoad(gameObject);

            Init();
        }

        public void ResetToDefaultState()
        {
            SetVisibility(false);
            mapChanged = true;
        }
        /*
        public void GameStateWaChanged()
        {
            SetVisibility(false);
            mapChanged = true;
        }*/

        private void Init()
        {
            SetVisibility(false);
            mapChanged = true;
        }


        //TODO do editor
        public void UpdateRefList()
        {
            menu.gameObject.SetActive(true);
            locationsOnMap = new List<LocationInMenu>(FindObjectsOfType<LocationInMenu>());
            menu.gameObject.SetActive(false);
        }

        public void SetVisibility(bool opended)
        {
            isOpened = opended;
            menu.gameObject.SetActive(isOpened);
        }

        public void Open()
        {
            if (!gameManager.CanMapBeOpened())
            {
                return;
            }
            gameManager.ChangeCursorToDefault();
            SetVisibility(true);
            ShowLocationsOnMap();
        }

        public void Close()
        {
            if (isOpened)
            {
                SetVisibility(false);
                gameManager.ChangeCursorToDefault();
            }
        }

        public void Toogle()
        {
            if (isOpened)
            {
                Close();
            }
            else
            {
                Open();
            }
        }

        void ShowLocationsOnMap()
        {
            if (mapChanged)
            {
                for (int i = 0; i < locationsOnMap.Count; i++)
                {
                    if(locationsOnMap[i].location.isOpened == true)
                    {
                        //!! activate with listeners only when needed
                        if(locationsOnMap[i].gameObject.activeSelf==false)
                        {
                            locationsOnMap[i].gameObject.SetActive(true);
                        }
                        
                    } else
                    {
                        //!! deactivate with listeners only when needed
                        if (locationsOnMap[i].gameObject.activeSelf == true)
                        {
                            locationsOnMap[i].gameObject.SetActive(false);
                        }
                    }
                }
                mapChanged = false;
            }
            else
            {
                //!!everything already there
            }
        }


    }
}
