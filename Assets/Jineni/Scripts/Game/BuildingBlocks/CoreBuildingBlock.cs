﻿using UnityEngine;

namespace Jineni.Game.BuildingBlocks
{
    public abstract class CoreBuildingBlock : ScriptableObject, IStateResetable
    {
        public int uniqueId = -1;

        public abstract void ResetToDefaultState();
    }
}
