﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.DialogSystem;
using System;
using UnityEditor;
using UnityEngine.Assertions;
using Jineni.Localization;
using Jineni.Utils;
using Jineni.DialogSystem.Graph;
using Jineni.Game.GameStateBlocks;

namespace Jineni.Game.BuildingBlocks
{

    [CreateAssetMenu(menuName = "Jineni/new location", order = 1)]
    public class LocationCore : CoreBuildingBlock, IEditorNamable
    {
        public string editorName = "name for editor";
        public Translation[] names;
        public int currentNameIndex = 0;  
        //TODO nedat do gamemanager
        public int sceneIndex = -1;
        public bool isOpened = false;
        public EntryPoint onOpenEntryPoint;
        public EntryPoint defaultOnOpenEntryPoint;
        //TODO ifcorupted - unique transformsStorage names

        [Serializable]
        public class CharacterTransforms
        {
            public CharacterCore character;
            public TransformsStorage transformsStorage;
        }
        [Serializable]
        public class CharacterActivities
        {
            public CharacterCore character;
            public ActivitiesStorage activitiesStorage;
        }

        [SerializeField]
        private List<CharacterTransforms> allPossibleCharactersTransforms;
        [SerializeField]
        private List<CharacterActivities> allPossibleCharactersActivities;

        public string GetEditorName()
        {
            return "L"+uniqueId+" - "+ editorName;
        }

        public string GetCurrentName(Lang lang)
        {
            
            if (!ArrayHelper.IsIndexInArrayRange(currentNameIndex, name.Length))
            {
                Siren.EditorWarn("not in index range");
                return names[0].Get(lang); ;
            }
            return names[currentNameIndex].Get(lang);
        }

        public void SetActivityIndexFor(int index, CharacterCore character)
        {
            ActivitiesStorage storage = GetActivitiesFor(character);
            if (storage == null)
            {
                Siren.EditorWarn("cant change activity for character " + character.GetEditorName() + ", character not found in activities list");
                return;
            }
            storage.SetIndex(index);
        }

        public void SetTransformIndexFor(int index, CharacterCore character)
        {
            TransformsStorage storage = GetTransformsFor(character);
            if (storage == null)
            {
                Siren.EditorWarn("cant change transform for character " + character.GetEditorName() + ", character not found in transform list");
                return;
            }
            storage.SetIndex(index);
        }

        public List<CharacterStateTransformIndex> GetCharactersTransformsIndexes()
        {
            
            List<CharacterStateTransformIndex> indexes = new List<CharacterStateTransformIndex>();
            if(allPossibleCharactersTransforms.Count == 0)
            {
                return indexes;
            }
            for (int i = 0; i < allPossibleCharactersTransforms.Count; i++)
            {
                if(allPossibleCharactersTransforms[i]==null || allPossibleCharactersTransforms[i].character==null || allPossibleCharactersTransforms[i].transformsStorage==null)
                {
                    Assert.IsFalse(true, "some weird inconsistency in allPossibleCharactersTransforms, loc "+editorName);
                    continue;
                }
                indexes.Add(new CharacterStateTransformIndex()
                {
                    characterId = allPossibleCharactersTransforms[i].character.uniqueId,
                    transformId = allPossibleCharactersTransforms[i].transformsStorage.currentIndex
                });
            }
            return indexes;
        }

        public List<CharacterStateActivityIndex> GetCharactersActivitiesIndexes()
        {
            List<CharacterStateActivityIndex> indexes = new List<CharacterStateActivityIndex>();
            if (allPossibleCharactersActivities.Count == 0)
            {
                return indexes;
            }
            for (int i = 0; i < allPossibleCharactersActivities.Count; i++)
            {
                if (allPossibleCharactersActivities[i] == null || allPossibleCharactersActivities[i].character == null || allPossibleCharactersActivities[i].activitiesStorage == null)
                {
                    Assert.IsFalse(true, "some weird inconsistency in allPossibleCharactersTransforms, loc " + editorName);
                    continue;
                }
                indexes.Add(new CharacterStateActivityIndex()
                {
                    characterId = allPossibleCharactersActivities[i].character.uniqueId,
                    activitiyId = allPossibleCharactersActivities[i].activitiesStorage.currentIndex
                });
            }
            return indexes;
        }

        public void SetCharactersTransformsIndexes(List<CharacterStateTransformIndex> indexes)
        {
            for (int i = 0; i < allPossibleCharactersTransforms.Count; i++)
            {
                for (int j = 0; j < indexes.Count; j++)
                {
                    if(allPossibleCharactersTransforms[i].character.uniqueId == indexes[j].characterId)
                    {
                        allPossibleCharactersTransforms[i].transformsStorage.SetIndex(indexes[j].transformId);
                        //??na co tu byl removeat index?
                       // indexes.RemoveAt(j);
                        //!! break one loop, go to next allPossibleCharactersTransforms
                        break;
                    }
                }
             }
        }

        public void SetCharactersActivitiesIndexes(List<CharacterStateActivityIndex> indexes)
        {
            for (int i = 0; i < allPossibleCharactersActivities.Count; i++)
            {
                for (int j = 0; j < indexes.Count; j++)
                {
                    if (allPossibleCharactersActivities[i].character.uniqueId == indexes[j].characterId)
                    {
                        allPossibleCharactersActivities[i].activitiesStorage.SetIndex(indexes[j].activitiyId);
                        //??na co tu byl removeat index?
                        // indexes.RemoveAt(j);
                        //!! break one loop, go to next allPossibleCharactersTransforms
                        break;
                    }
                }
            }
        }
      

        public void SetNameIndex(int index)
        {
            if (!ArrayHelper.IsIndexInArrayRange(index, names.Length))
            {
                Siren.EditorWarn("cant change name index; out of boundaries; i=" + index + "; count=" + names.Length);
                return;
            }
            currentNameIndex = index;
        }

        public void OpenNotice()
        {
            //todo notice
            isOpened = true;
            MapManager.Instance.mapChanged = true;
        }


        public void CloseNotice()
        {
            //todo notice
            isOpened = false;
            MapManager.Instance.mapChanged = true;
        }

        public void SetIsOpened()
        {
            //todo defensife check?
            isOpened = true;
        }

        public void SetIsNotOpened()
        {
            //todo defensife check?
            isOpened = false;
        }

        void SetTransformsListIfIsNot()
        {
            if (allPossibleCharactersTransforms == null)
            {
                allPossibleCharactersTransforms = new List<CharacterTransforms>(50);
            }
        }

        public TransformsStorage GetTransformsFor(CharacterCore character)
        {
            SetTransformsListIfIsNot();
            foreach (var item in allPossibleCharactersTransforms)
            {
                if(item.character == character)
                {
                    return item.transformsStorage;
                }
            }
            return null;
        }

        public void AddNewTransform(CharacterCore character, Transform transform)
        {
            TransformsStorage isThereAlready = GetTransformsFor(character);

            if (isThereAlready==null)
            {
                TransformsStorage transformsForOneCharacter = new TransformsStorage();
                transformsForOneCharacter.AddNewTransform(transform);
                CharacterTransforms characterTransformsEntry = new CharacterTransforms()
                {
                    character = character,
                    transformsStorage = transformsForOneCharacter
                };
                allPossibleCharactersTransforms.Add(characterTransformsEntry);
            } else
            {
                isThereAlready.AddNewTransform(transform);
            }
        }

        int GetCharacterIndexInTransformList(CharacterCore character)
        {
            SetTransformsListIfIsNot();
            if (allPossibleCharactersTransforms.Count == 0)
            {
                return -1;
            }
            for (int i = 0; i < allPossibleCharactersTransforms.Count; i++)
            {
                if(allPossibleCharactersTransforms[i].character == character)
                {
                    return i;
                }
            }
            return -1;
        }


        public string[] GetPossibleCharacterTransformNames(CharacterCore character)
        {
            TransformsStorage transformsForOneCharacter = GetTransformsFor(character);
            if (transformsForOneCharacter == null)
            {
                return new string[0];
            }
            return transformsForOneCharacter.GetNames();
        }

        public void ChangeCharacterTransform(CharacterCore character, int index)
        {
            TransformsStorage transformsForOneCharacter = GetTransformsFor(character);
            Assert.IsNotNull(transformsForOneCharacter);
            if (transformsForOneCharacter == null)
            {
                //!!cant change, stay where you are
                Siren.EditorWarn("cant change transform for " + character.GetEditorName() + " in location" + GetEditorName() + "; no TransformsStorage");
                return;
            }
            transformsForOneCharacter.SetIndex(index);
        }

        public void RestoreTransformStorage(CharacterCore character, TransformsStorage transformsStorage)
        {
            CharacterTransforms characterTransforms = new CharacterTransforms()
            {
                character = character,
                transformsStorage = transformsStorage
            };

            int index = GetCharacterIndexInTransformList(character);
            if (index == -1)
            {
                allPossibleCharactersTransforms.Add(characterTransforms);
            }
            else
            {
                allPossibleCharactersTransforms[index] = characterTransforms;
            }
        }

        /// <summary>
        /// ////////////////////
        /// </summary>

        void SetActivitiesListIfIsNot()
        {
            if (allPossibleCharactersTransforms == null)
            {
                allPossibleCharactersTransforms = new List<CharacterTransforms>(50);
            }
        }

        public ActivitiesStorage GetActivitiesFor(CharacterCore character)
        {
            SetActivitiesListIfIsNot();
            foreach (var item in allPossibleCharactersActivities)
            {
                if (item.character == character)
                {
                    return item.activitiesStorage;
                }
            }
            return null;
        }

        /*
        public void AddNewTransform(CharacterCore character, Transform transform)
        {
            TransformsStorage isThereAlready = GetTransformsFor(character);

            if (isThereAlready == null)
            {
                TransformsStorage transformsForOneCharacter = new TransformsStorage();
                transformsForOneCharacter.AddNewTransform(transform);
                CharacterTransforms characterTransformsEntry = new CharacterTransforms()
                {
                    character = character,
                    transformsStorage = transformsForOneCharacter
                };
                allPossibleCharactersTransforms.Add(characterTransformsEntry);
            }
            else
            {
                isThereAlready.AddNewTransform(transform);
            }
        }*/

        int GetCharacterIndexInActivitiesList(CharacterCore character)
        {
            SetActivitiesListIfIsNot();
            if (allPossibleCharactersActivities.Count == 0)
            {
                return -1;
            }
            for (int i = 0; i < allPossibleCharactersActivities.Count; i++)
            {
                if (allPossibleCharactersActivities[i].character == character)
                {
                    return i;
                }
            }
            return -1;
        }


        public string[] GetPossibleCharacterActivityNames(CharacterCore character)
        {
            ActivitiesStorage activities = GetActivitiesFor(character);
            if (activities == null)
            {
                return new string[0];
            }
            return activities.GetNames();
        }

        public void ChangeCharacterActivities(CharacterCore character, int index)
        {
            //Debug.Log("se snazim");
            ActivitiesStorage activities = GetActivitiesFor(character);

            if (activities == null)
            {
                Siren.EditorWarn("cant change activities for " + character.GetEditorName() + " in location" + GetEditorName() + "; no ActivitiesStorage");
                return;
            }
            activities.SetIndex(index);
        }

        public bool IsCharacterInActivityState(CharacterCore character, int index)
        {
            int currentIndex = GetCurrentActivityIndexFor(character);
            return currentIndex == index;
        }

        public int GetCurrentActivityIndexFor(CharacterCore character)
        {
            SetActivitiesListIfIsNot();
            foreach (var item in allPossibleCharactersActivities)
            {
                if (item.character == character)
                {
                    return item.activitiesStorage.currentIndex;
                }
            }
            return 0;
        }

        public void RestoreActivitiesStorage(CharacterCore character, ActivitiesStorage activities)
        {
            CharacterActivities characterActivities = new CharacterActivities()
            {
                character = character,
                activitiesStorage = activities
            };

            int index = GetCharacterIndexInActivitiesList(character);
            if (index == -1)
            {
                allPossibleCharactersActivities.Add(characterActivities);
            }
            else
            {
                allPossibleCharactersActivities[index] = characterActivities;
            }
        }

        public void ChangeEntryPoint(EntryPoint ep)
        {
            onOpenEntryPoint = ep;
        }

        public override void ResetToDefaultState()
        {
            onOpenEntryPoint = defaultOnOpenEntryPoint;
            currentNameIndex = 0;
            isOpened = false;
        }

#if UNITY_EDITOR

        public void SaveStorages(CharacterCore character, TransformsStorage transformsStorage, ActivitiesStorage activitiesStorage)
        {
            RestoreTransformStorage(character, transformsStorage);
            RestoreActivitiesStorage(character, activitiesStorage);
            Siren.EditorNotice("SaveStorages");

            //!!musi tu byt
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
        }


#endif

    }
}
