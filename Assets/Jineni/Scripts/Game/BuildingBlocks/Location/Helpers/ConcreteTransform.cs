﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Jineni.Game.BuildingBlocks
{
    [System.Serializable]
    public class ConcreteTransform
    {
        public string name;
        public Vector3 position;
        public Quaternion rotation;
        public Vector3 scale;

        public ConcreteTransform(Transform transform)
        {
            position = transform.localPosition;
            rotation = transform.rotation;
            scale = transform.localScale;
        }
    }
}
