﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Utils;
using UnityEngine.Assertions;

namespace Jineni.Game.BuildingBlocks
{
    [System.Serializable]
    public class ActivitiesStorage
    {
        public string[] activities;
        public int currentIndex = 0;

#if UNITY_EDITOR
        [HideInInspector]
        public string corruptionReport;
#endif

        public string GetCurrent()
        {
            //!!if not set
            if (GetCount() == 0)
            {
                Siren.EditorNotice("activityNames not defined");
                return null;
            }

            if (currentIndex == -1)
            {
                //!!it means show no activity
                //Siren.EditorNotice("currentIndex == -1");
                return null;
            }


           
            if (!ArrayHelper.IsIndexInArrayRange(currentIndex, GetCount()))
            {
                Siren.EditorWarn("currentActivityIndex > activityNames.Length;" + currentIndex + ">" + GetCount());
                return null;
            }
            return activities[currentIndex];
        }

        public string[] GetNames()
        {
            if (GetCount() == 0)
            {
                return new string[0];
            }
            else
            {
                return activities;
            }

        }
        
        public int GetCount()
        {
            if (activities == null)
            {
                return 0;
            }
            return activities.Length;
        }

        public void SetIndex(int index)
        {
            if (index == -1 || GetCount() == 0)
            {
                return;
            }
            if (!ArrayHelper.IsIndexInArrayRange(index, GetCount()))
            {
                Assert.IsTrue(false, "cant change activity index, index not in boundaries;i=" + index + " length=" + GetCount());
                return;
            }
            currentIndex = index;
        }

        public bool HasSome()
        {
            return GetCount() > 0;
        }

#if UNITY_EDITOR

        public bool IsCorrupted()
        {
            if (currentIndex == 0 && GetCount() == 0)
            {
                return false;
            }

            if (!ArrayHelper.IsIndexInArrayRange(currentIndex, GetCount()))
            {
                corruptionReport = "index not in array range: " + currentIndex + " is not in - [0," + GetCount() + "]";
                return true;
            }
            for (int i = 0; i < activities.Length; i++)
            {
                if (activities[i] == "")
                {
                    corruptionReport = "activity i=" + i + " name is empty string";
                    return true;
                }
            }            

            return false;
        }

#endif  

    }
}
