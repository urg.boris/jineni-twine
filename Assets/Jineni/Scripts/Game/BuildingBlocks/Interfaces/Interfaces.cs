﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Jineni.Game.BuildingBlocks
{
    public interface ICoreStateApliable
    {
        void ApplyCoreState();
    }
    public interface IStateResetable
    {
        void ResetToDefaultState();
    }

    public interface IEditorNamable
    {
        string GetEditorName();
    }
}
