﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Jineni.Game.BuildingBlocks
{
    public class AudioManager : MonoBehaviour, IStateResetable
    {
        public GameManager gameManager;
        //TODO zatim je map object useles, vse v locationsOnMap, ale otevirani lokaci bude asi map

        public AudioSource audioSource;
        public AudioClip[] musicClips;


        static AudioManager _instance = null;
        public static AudioManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<AudioManager>();
                }
                //!! must remain null if doesnt exists, checker will create it
                return _instance;
            }
        }

        private void Awake()
        {
            //Assert.IsNotNull(world);
           // Assert.IsNotNull(menu);
            //Assert.IsTrue(world.locations.Count > 0);

            if (_instance == null)
            {
                _instance = this;
            }
            else if (_instance != this)
            {
                Destroy(gameObject);
            }
            //!!is part of GameManager
            //DontDestroyOnLoad(gameObject);

            Init();
        }

        private void Init()
        {
            audioSource.Stop();
            audioSource.clip = musicClips[0];
            audioSource.Play();
        }

        public void ResetToDefaultState()
        {

        }
    }
}
