﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using TMPro;
using Jineni.DialogSystem;
using Jineni.DialogSystem.Graph;
using Jineni.DialogSystem.FlowControl.Triggers;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Jineni.Game.BuildingBlocks.Internals;

namespace Jineni.Game.BuildingBlocks
{
    public class DialogManager : MonoBehaviour, IStateResetable
    {
        //todo max_options
        //public const int MAX_OPTIONS = 5;
        //public const string PLAYER_NAME = "Já";


        public GameManager gameManager;
        public TextMeshProUGUI replicText;
        public Canvas dynamicCanvas;
        public GameObject dialogOptionsContainer;
        public GameObject DialogOptionPrefab;
        public Button[] optionButtons;

        [Header("SomeTimePassed")]
        public GameObject someTimePassedUI;
        public Image someTimePassedBackground;
        public TMP_Text caption;
        public AudioSource audioSource;
        

        [HideInInspector]
        public bool dialogIsRunning = false;
        [HideInInspector]
        public bool isSomeTimePassedTriggerRunning = false;


        private int chosenActiveDialogOption;
        WaitForSeconds showReplicTextForTime;
        public ReferenceLinkHelper referenceLinkHelper;

        //!!prevent jumping back to same corutine
        IEnumerator replicStopCorutine;

        Lang Lang
        {
            get
            {
                return referenceLinkHelper.settings.currentLang;
            }
        }

        public static DialogManager _instance = null;
        public static DialogManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<DialogManager>();
                }
                //!! must remain null if doesnt exists, checker will create it
                return _instance;
            }
        }



        void Awake()
        {
            Assert.IsNotNull(replicText);
            Assert.IsNotNull(dynamicCanvas);
            Assert.IsNotNull(dialogOptionsContainer);
            //Assert.IsNotNull(optionButtons);

            if (_instance == null)
            {
                _instance = this;
            }
            else if (_instance != this)
            {
                Destroy(gameObject);
            }
            //!!is part of GameManager
            //DontDestroyOnLoad(gameObject);

            Init();
        }
        /*
        public void GameStateWasLoaded()
        {
            //!!dialog shouldnt be running
            StopCoroutine("HandleDialog");
            StopCoroutine("WaitForReplicShowTime");
            dialogIsRunning = false;
            HideDialogMenu();
            someTimePassedUI.SetActive(false);
        }*/

        public void ResetToDefaultState()
        {
            //!!dialog coturines are stoped by gamemanager activate/deactivate this gameobject
            ForceEndDialog();
            someTimePassedUI.SetActive(false);
        }


        private void Init() {
            HideDialogMenu();
            someTimePassedUI.SetActive(false);
        }
        

        void AttachOptionListeners()
        {
            if (optionButtons.Length > 0)
            {
                for (int i = 0; i < optionButtons.Length; i++)
                {
                    int index = i;
                    optionButtons[i].onClick.AddListener(delegate { OptionHasBeenChosen(index); });
                }
            }
        }

        void DestroyDialogOptionVisuals()
        {
            if (optionButtons.Length > 0)
            {
                for (int i = 0; i < optionButtons.Length; i++)
                {
                    optionButtons[i].onClick.RemoveAllListeners();
                    Destroy(optionButtons[i].gameObject);
                }
            }
            optionButtons = new Button[0];
        }

        void OptionHasBeenChosen(int i)
        {
            chosenActiveDialogOption = i;
            dialogOptionsContainer.SetActive(false);
            //Debug.Log("OptionHasBeenChosen"+i);
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        void HideDialogMenu()
        {
            DestroyDialogOptionVisuals();
            dynamicCanvas.gameObject.SetActive(false);
        }

        void ShowDialogMenu()
        {
            dynamicCanvas.gameObject.SetActive(true);
        }
        /*
        private void ShowHideContentAccordingly()
        {
            DetachListeners();
            dynamicCanvas.gameObject.SetActive(dialogIsRunning);
        }*/

        public void StartExamination(ItemCore item)
        {
            EntryPoint entryPoint = item.currentExamineEntryPoint;
            if(entryPoint!=null)
            {
                StartDialog(entryPoint);
            } else
            {
                StartDialog(referenceLinkHelper.defaultItemExaminationEntryPoint);
            }
            
        }

        public void StartExamination(CharacterCore character)
        {
            EntryPoint entryPoint = character.currentExamineEntryPoint;
            if (entryPoint != null)
            {
                StartDialog(entryPoint);
            }
            else
            {
                StartDialog(referenceLinkHelper.defaultCharacterExaminationEntryPoint);
            }
        }

        public void StartDialog(EntryPoint entryPoint)
        {
            if(dialogIsRunning || !gameManager.CanDialogStart())
            {
                return;
            }
            dialogIsRunning = true;
            ShowDialogMenu();
            dialogOptionsContainer.SetActive(false);
            StartCoroutine(HandleDialog(entryPoint));
        }


        void ResetLastChosenOption()
        {
            //Debug.Log("reseting");
            chosenActiveDialogOption = -1;
        }

        Lang GetCurrentLang()
        {
            return gameManager.settings.currentLang;
        }

        string GetCharacterInfoFrom(VocalNode node)
        {
            if(node.speaker == referenceLinkHelper.playerCharacter)
            {
                return string.Concat(node.speaker.GetCurrentName(GetCurrentLang()), ": ");
            }
            if(node.speaker == referenceLinkHelper.enviromentCharacter)
            {
                return "";
            }
            if(node.speaker != null)
            {
                return string.Concat(node.speaker.GetCurrentName(GetCurrentLang()), ": ");
            }
            Siren.EditorWarn("node "+node.name+" does not have character");
            return "";
        }

        IEnumerator WaitForReplicShowTime(int replicLength)
        {
            //Debug.Log("starting");
            float showReplicTime = Mathf.Max(2, replicLength * 0.1f);
            float replicAlreadyShownTime = 0;
            while (replicAlreadyShownTime < showReplicTime)
            {
                if (Input.GetKeyUp(KeyCode.Space) || Input.GetMouseButtonDown(2))
                {
                    StopReplicSound();
                    yield return new WaitForSeconds(0.05f);
                    yield break;
                }
                //Debug.Log("end replic");
                replicAlreadyShownTime += Time.deltaTime;
                yield return null;
            }
            StopReplicSound();
            //Debug.Log("stoping timeend");
        }

        void PlayReplicSound(AudioClip audio)
        {
            //audioSource.clip = audio;
            audioSource.PlayOneShot(audio);
        }

        void StopReplicSound()
        {
            //audioSource.clip = audio;
            audioSource.Stop();
        }

        //TODO zmerit pamet a nepredelat?
        IEnumerator HandleDialog(EntryPoint entryPoint)
        {
            Assert.IsNotNull(entryPoint);
            Node currentNode = entryPoint.GetNextNode();
            /*if(currentNode is Replic || currentNode is MultiOption)
            {
                ShowDialogMenu();

            }*/
            while (currentNode!=null)
            {
                //!!if some timepass is first node, prevent no dialog window
                ShowDialogMenu();
                //!!just in case long condition/trigger/whatever at beginning to show last replic NOT!
                replicText.text = "";
                //Debug.Log("node in dialog loo " + currentNode.name);
                if (currentNode is Replic)
                {
                    Replic replic = (currentNode as Replic);  
                    replicText.text = string.Concat(GetCharacterInfoFrom(replic), replic.GetText(Lang));
                    /* if (replicStopCorutine != null)
                     {
                         StopCoroutine(replicStopCorutine);
                     }
                     replicStopCorutine =*/
                    PlayReplicSound(replic.replicAudio);
                    yield return WaitForReplicShowTime(replicText.text.Length);
                    currentNode = currentNode.GetNextNode();
                } else if(currentNode is MultiOption)
                {
                    //Debug.Log("is multi");
                    MultiOption multiOption = (currentNode as MultiOption);
                    replicText.text = string.Concat(GetCharacterInfoFrom(multiOption), multiOption.GetText(Lang));

                    dialogOptionsContainer.SetActive(true);
                    ResetLastChosenOption();

                    List<DialogOption> dialogOptions = multiOption.GetActiveDialogOptions();
                    //Debug.Log("before");
                    //multiOption.GetCurrentStateHash();
                    optionButtons = new Button[dialogOptions.Count];

                    for (int i = 0; i < dialogOptions.Count; i++)
                    {
                        GameObject instance =  Instantiate(DialogOptionPrefab, dialogOptionsContainer.transform);
                        //TODO prolinkovanej script
                        optionButtons[i] = instance.GetComponentInChildren<Button>();
                        //!!kolik moznosti ma byt, videt 
                        //TODO optimalizace?
                        instance.GetComponentInChildren<Text>().text = dialogOptions[i].GetText(Lang);
                        if(dialogOptions[i].isVisited)
                        {
                            instance.GetComponentInChildren<Image>().color = new Color(1, 1, 1, 0.5f);
                        } else
                        {
                            instance.GetComponentInChildren<Image>().color = new Color(1, 1, 1, 0.8f);
                        }
                        instance.gameObject.SetActive(true);

                        //TODO delegates, jako ze se ceka na delegata a ne na corutinu?

                    }
                    PlayReplicSound(multiOption.replicAudio);
                    AttachOptionListeners();
                    //!!wait for pick event
                    while (chosenActiveDialogOption < 0)
                    {
                        yield return null;
                    }
                    StopReplicSound();
                    //Debug.Log("chose" + chosenActiveDialogOption+" real="+ dialogOptions[chosenActiveDialogOption].indexWithinMultiOption);
                    //Debug.Log("chosenOption" + chosenOption);
                    dialogOptions[chosenActiveDialogOption].WasVisited();
                    gameManager.MultiOptionChagned(multiOption);
                    //Debug.Log("after");
                    //multiOption.GetCurrentStateHash();
                    DestroyDialogOptionVisuals();
                    //todo check
                    currentNode = multiOption.GetNextNodeFromOption(dialogOptions[chosenActiveDialogOption].indexWithinMultiOption);
                }
                else if (currentNode is Condition)
                {
                    //!!must be here to prevent long condition/trigger/whatever at beginning to show last replic
                    HideDialogMenu();
                    //Debug.Log("is condi");
                    Condition conditional = (currentNode as Condition);

                    if(conditional.IsTrue())
                    {
                        //!!TRUE
                        currentNode = conditional.GetNextNodeFromOption(0);
                    } else
                    {
                        //!!FALSE
                        currentNode = conditional.GetNextNodeFromOption(1);
                    }
                }
                else if (currentNode is Trigger)
                {
                    //!!must be here to prevent long condition/trigger/whatever at beginning to show last replic
                    HideDialogMenu();
                    Trigger trigger = (currentNode as Trigger);
                    for (int i = 0; i < trigger.triggers.Count; i++)
                    {
                        if (trigger.triggers[i].type is DelayTrigger)
                        {
                            yield return new WaitForSeconds(trigger.triggers[i].floatParam);

                        } else if (trigger.triggers[i].type is SomeTimePassed) {
                            HideDialogMenu();
                            int remianingTriggersInNodeCount = trigger.triggers.Count - i - 1;
                            //Debug.Log("remianingTriggersInNode" + remianingTriggersInNodeCount);
                            List<ConcreteTrigger> remianingTriggersInNode = new List<ConcreteTrigger>();
                            if (remianingTriggersInNodeCount>0)
                            {
                                //int index = 0;
                                for (int j = i+1; j < trigger.triggers.Count; j++)
                                {
                                    Debug.Log("trigger.triggers[i].type" + trigger.triggers[j].type);
                                    remianingTriggersInNode.Add(trigger.triggers[j]);
                                    //index++;
                                }
                            }
                            yield return SomeTimePassing(trigger.triggers[i].floatParam, trigger.triggers[i].stringParam, remianingTriggersInNode);
                        }
                        else
                        {                       
                           trigger.triggers[i].Invoke();
                        }
                        
                    }

                    currentNode = currentNode.GetNextNode();


                }
                else
                {
                    //!! hra je dialog driven, kdyz je tohle posrany tak bude asi i cela hra
                    Assert.IsTrue(false, "unknown node type");
                    Siren.Error("unknown node type");
                }


                //Debug.Log(currentNode.GetType());

            }
            //Debug.Log("loopend" + currentNode);
            EndDialog();
        }

        public void EndDialog()
        {
            if (!dialogIsRunning)
            {
                return;
            }
            dialogIsRunning = false;
            HideDialogMenu();
        }



        void ForceEndDialog()
        {
            dialogIsRunning = false;
            HideDialogMenu();
        }


        /*
        public void RunSomeTimePassedTrigger(Node runAfterFinnish=null)
        {
            if(gameManager.CanSomeTimePassedTriggerBeRun())
            {
               // Debug.Log("play it");
                someTimePassedUI.SetActive(true);
                isSomeTimePassedTriggerRunning = true;
               // animator.Play("AfterSomeTime-hidden");
                animator.SetTrigger("run");
            }
        }*/
/*
        public void FinnishSomeTimePassed()
        {
            if (isSomeTimePassedTriggerRunning == true)
            {
                //Debug.Log("finnished");
                someTimePassedUI.SetActive(false);
                isSomeTimePassedTriggerRunning = false;
            }
        }*/

        IEnumerator SomeTimePassing(float time, string text, List<ConcreteTrigger> remianingTriggersInNode)
        {
            someTimePassedUI.SetActive(true);
            isSomeTimePassedTriggerRunning = true;
            Color backgroundColor = someTimePassedBackground.color;
            backgroundColor.a = 0;
            someTimePassedBackground.color = backgroundColor;

            Color captionColor = caption.color;
            captionColor.a = 0;
            caption.color = captionColor;
            caption.text = text;
            yield return DoTimePassedFadeIn();
            for (int i = 0; i < remianingTriggersInNode.Count; i++)
            {
                remianingTriggersInNode[i].Invoke();
            }
            HideDialogMenu();
            yield return new WaitForSeconds(time);
            yield return DoTimePassedFadeOut();
            //Debug.Log("end");
        }

        private IEnumerator DoTimePassedFadeIn()
        {
            float elapsedTime = 0.0f;
            float ratio;
            Color captionColor = caption.color;
            Color backgroundColor = someTimePassedBackground.color;
            float fadeTime = 2f;
            while (elapsedTime < fadeTime)
            {
                yield return new WaitForEndOfFrame();
                elapsedTime += Time.deltaTime;
                ratio = Mathf.Clamp01(elapsedTime / fadeTime);
                //Debug.Log("ratio" + ratio);
                backgroundColor.a = ratio;
                someTimePassedBackground.color = backgroundColor;
                captionColor.a = ratio;
                caption.color = captionColor;
            }
            someTimePassedUI.SetActive(true);
        }

        private IEnumerator DoTimePassedFadeOut()
        {
            someTimePassedUI.SetActive(true);
            float elapsedTime = 0.0f;
            float fadeTime = 3f;
            float ratio;
            Color captionColor = caption.color;
            Color backgroundColor = someTimePassedBackground.color;
            while (elapsedTime < fadeTime)
            {
                yield return new WaitForEndOfFrame();
                elapsedTime += Time.deltaTime;
                ratio = 1.0f - Mathf.Clamp01(elapsedTime / fadeTime);
                backgroundColor.a = ratio;
                someTimePassedBackground.color = backgroundColor;
                captionColor.a = ratio;
                //Debug.Log("ratio" + ratio);
                caption.color = captionColor;
            }

            someTimePassedUI.SetActive(false);
            isSomeTimePassedTriggerRunning = false;
            ShowDialogMenu();
        }


    }
}
