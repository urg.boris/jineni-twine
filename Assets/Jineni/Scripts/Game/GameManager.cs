﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using UnityEngine.SceneManagement;
using Jineni.Game.GameStateBlocks;
using UnityEngine.Assertions;
using Jineni.Utils;
using Jineni.Game.BuildingBlocks.Internals;
using UnityEditor;
using Jineni.DialogSystem.Graph;
using HtmlAgilityPack;


namespace Jineni.Game
{
    public class GameManager : MonoBehaviour
    {
        //!! shown by inspector
        [HideInInspector]
        public GameState providedGameState;
        public GameState loadOnStart;
        [Header("SubManagers")]
        public MapManager mapManager;
        public DialogManager dialogManager;
        public InventoryManager inventoryManager;
        [Header("General")]
        public Texture2D cursor;
        Camera _mainCamera;
        public Settings settings;
        public ReferenceLinkHelper referenceLinkHelper;
        [Header("For Saves")]
        public List<LocationCore> locations;
        public List<CharacterCore> characters;
        public List<ItemCore> items;
        public List<MultiOption> changedMultiOptions;

        public EntryPoint[] entryPoints;
        public string[] entryPointsNames;

        public MultiOption[] multiOptions;
        public string[] multiOptionsNames;

        //time passing in dialog
        public enum GameplayFocus
        {
            mainMenu,
            location,
            dialogRunning,
            mapIsOpened,
            inventroryIsOpened, 
        }

        private GameplayFocus _currentInteractivyFocus;
        public GameplayFocus CurrentInteractivityFocus
        {
            get
            {
                return _currentInteractivyFocus;
            }
            set
            {
                _currentInteractivyFocus = value;
            }
        }

        LocationCore currentLocation;

        public Camera MainCamera
        {
            get
            {
                if (_mainCamera == null)
                {
                    _mainCamera = Camera.main;
                }
                return _mainCamera;
            }
        }
        static GameManager _instance = null;
        public static GameManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<GameManager>();
                }
                //!! must remain null if doesnt exists, checker will create it
                return _instance;
            }
        }

        private void Awake()
        {
            if (_instance == null)
            {
                //!! set only once
                _instance = this;
                //TODO mechanism loadovani
//#if UNITY_EDITOR
               // ResetAllForNewGame();
//#endif
            }
            else if (_instance != this)
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);
        }

        private void Start()
        {
            LoadGameStateOnStart();
            if(LocationController.Instance==null)
            {
                //!!this might be main menu
                return;
                
               // Siren.Error("")
            }
            LocationController.Instance.Run();
        }

        public void LoadGameStateOnStart()
        {
            //Debug.Log("adsdasdasdas");
            if (loadOnStart != null)
            {
                LoadGameStateFromSO(loadOnStart);
            }
        }

        public void LoadProvidedGameState()
        {
            LoadGameStateFromSO(providedGameState);
#if UNITY_EDITOR
            if (EditorApplication.isPlaying)
            {
                LocationController.Instance.Run();
            }
#endif
        }
        /*
        void ResetAllForNewGame()
        {
#if UNITY_EDITOR
            UpdateRefLinks();
#endif
            ResetToDefaultState();
        }*/

        public void ResetToDefaultState()
        {
            for (int i = 0; i < locations.Count; i++)
            {
                locations[i].ResetToDefaultState();
            }

            for (int i = 0; i < characters.Count; i++)
            {
                //Debug.Log(characters[i].name);
                characters[i].ResetToDefaultState();
            }
            LocationController.Instance.ApplyCharactersCoreState();

            for (int i = 0; i < items.Count; i++)
            {
                items[i].ResetToDefaultState();
            }

            changedMultiOptions.Clear();
            for (int i = 0; i < multiOptions.Length; i++)
            {
                multiOptions[i].ResetToDefaultState();
            }

            inventoryManager.ResetToDefaultState();
            mapManager.ResetToDefaultState();
            //!! stop running dialogs
            StopCorutinesOnObject(dialogManager.gameObject);
            dialogManager.ResetToDefaultState();
           
        }

        public void ResetAllToDefaultState()
        {
            ResetToDefaultState();
#if UNITY_EDITOR
            if (EditorApplication.isPlaying)
            {
                LocationController.Instance.Run();
            }
#endif
        }

        void StopCorutinesOnObject(GameObject gameObject)
        {
            gameObject.SetActive(false);
            gameObject.SetActive(true);
        }

        public void ChangeCursorToInteractive()
        {
            Cursor.SetCursor(cursor, Vector2.zero, CursorMode.Auto);
        }

        public void ChangeCursorToDefault()
        {
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        }

        public void MultiOptionChagned(MultiOption multiOption)
        {
            if (changedMultiOptions == null)
            {
                changedMultiOptions = new List<MultiOption>(50);
            }
            if (changedMultiOptions.Contains(multiOption))
            {
                //!!few option, no optimalisation
                int index = changedMultiOptions.IndexOf(multiOption);
                changedMultiOptions[index] = multiOption;
            }
            else
            {
                changedMultiOptions.Add(multiOption);
            }
        }

        //===================================GETTERS=========================================

        public void GetAllEntrypoints()
        {
            entryPoints = Finder.GetAllEntryPoints();
            entryPointsNames = new string[entryPoints.Length];
            for (int i = 0; i < entryPoints.Length; i++)
            {
                entryPointsNames[i] = entryPoints[i].uniqueId;
            }
        }

        public void GetAllMultiOptions()
        {
            multiOptions = Finder.GetAllMultiOptions();
            multiOptionsNames = new string[multiOptions.Length];
            for (int i = 0; i < multiOptions.Length; i++)
            {
                multiOptionsNames[i] = multiOptions[i].uniqueId;
            }
        }

        public EntryPoint GetEntryPointByName(string name)
        {
            //Debug.Log("getting" + name);
            if(name=="")
            {
                return null;

            }
            int index = System.Array.IndexOf(entryPointsNames, name);
            //Debug.Log("getting" + name+" index"+index);
            if (index==-1)
            {
                return null;
            } else
            {
                return entryPoints[index];
            }
        }

        public MultiOption GetMultiOptionByName(string name)
        {
            //Debug.Log("getting" + name);
            if (name == "")
            {
                return null;

            }
            int index = System.Array.IndexOf(multiOptionsNames, name);
            //Debug.Log("getting" + name+" index"+index);
            if (index == -1)
            {
                return null;
            }
            else
            {
                return multiOptions[index];
            }
        }


        public LocationCore GetLocationById(int id)
        {
            //Debug.Log("GetLocationById getting id" + id);
            for (int i = 0; i < locations.Count; i++)
            {
                if (locations[i].uniqueId == id)
                {
                    return locations[i];
                }
            }
            return null;
        }

        public CharacterCore GetCharacterById(int id)
        {
            //Debug.Log("GetLocationById getting id" + id);
            if(characters==null)
            {
                return null;
            }
            for (int i = 0; i < characters.Count; i++)
            {
                if (characters[i].uniqueId == id)
                {
                    return characters[i];
                }
            }
            return null;
        }

        public ItemCore GetItemById(int id)
        {
            //Debug.Log("GetLocationById getting id" + id);
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].uniqueId == id)
                {
                    return items[i];
                }
            }
            return null;
        }

        //===================================MANAGERS=========================================

        public bool CanInventoryBeOpened()
        {
            //TODO
            return !dialogManager.dialogIsRunning && !inventoryManager.isOpened;
        }


        public bool CanDialogStart()
        {
            //!!TODO mechenika mluveni
            return !mapManager.isOpened;
        }

        public bool CanCharacterInteract()
        {
            //!!TODO mechenika mluveni
            return !mapManager.isOpened && !dialogManager.dialogIsRunning && !inventoryManager.isOpened;
        }

        public bool CanItemBeExamined()
        {
            //!!TODO mechenika mluveni
            return !mapManager.isOpened && !dialogManager.dialogIsRunning;
        }

        public bool CanMapBeOpened()
        {
            //!!TODO mechenika mapy
            return !mapManager.isOpened && !dialogManager.dialogIsRunning;
        }
        /*
        public bool CanSomeTimePassedTriggerBeRun()
        {
            //!!TODO mechenika
            return !dialogManager.isSomeTimePassedTriggerRunning;
        }
        */
        //=================================LOCATIONS=================================

        public void CurrentLocationChanged(LocationCore location)
        {
            currentLocation = location;
            ChangeCursorToDefault();
        }

        public void VisitLocation(LocationCore location)
        {
            Assert.IsNotNull(location);
            //!! to get actual camera from next location
            _mainCamera = null;

            if (location.sceneIndex <= 0 || SceneManager.sceneCount < location.sceneIndex)
            {
                Assert.IsTrue(false, "cant load " + location.editorName + " scene index=" + location.sceneIndex);
                //!!major fuck up
                Siren.Error("cant load " + location.editorName + " scene index=" + location.sceneIndex);
                return;
            }
            SceneManager.LoadScene(location.sceneIndex);
            currentLocation = location;
        }

        public bool IsCharacterInActivityState(LocationCore location, CharacterCore character, int activityIndex)
        {
            int locationIndex = locations.IndexOf(location);
            Assert.IsTrue(locationIndex != -1);
            if (locationIndex == -1)
            {
                //!! cant change animation, stay in the same
                return false;
            }
            return locations[locationIndex].IsCharacterInActivityState(character, activityIndex);

            
        }

        public void ChangeLocationCharacterToActivity(LocationCore location, CharacterCore character, int activityIndex, bool immediately = false)
        {
            int locationIndex = locations.IndexOf(location);
            Assert.IsTrue(locationIndex != -1);
            if (locationIndex == -1)
            {
                //!! cant change animation, stay in the same
                return;
            }
            locations[locationIndex].ChangeCharacterActivities(character, activityIndex);
            if (immediately)
            {
                if (LocationController.Instance.location == locations[locationIndex])
                {
                    LocationController.Instance.ApplyCharacterActivity(character);
                }
            }
        }

        public void ChangeLocationCharacterTransform(LocationCore location, CharacterCore character, int transformIndex, bool immediately = false)
        {
            int locationIndex = locations.IndexOf(location);
            Assert.IsTrue(locationIndex != -1);
            if (locationIndex == -1)
            {
                //!! cant change animation, stay in the same
                return;
            }
            locations[locationIndex].ChangeCharacterTransform(character, transformIndex);
            if (immediately)
            {
                if (LocationController.Instance.location == locations[locationIndex])
                {
                    LocationController.Instance.ApplyCharacterTransform(character);
                }
            }
        }

        private void Update()
        {
            if (Input.GetKeyUp(KeyCode.Space))
            {
                //TODO kdy ajak
                //!!if dialog running - controll by space only dialog
                if(!dialogManager.dialogIsRunning)
                {
                    inventoryManager.Toogle();
                }
                
            }
            if (Input.GetKeyUp(KeyCode.M))
            {
                //TODO kdy ajak
                mapManager.Toogle();
            }
#if UNITY_EDITOR
            if (Input.GetKeyUp(KeyCode.L))
            {
                LoadProvidedGameState();
            }
#endif
        }






        public void LoadGameStateFromSO(GameState gameStateToLoad)
        {
            //Debug.Log("loadstate");
            Assert.IsNotNull(gameStateToLoad, "cant load game state, is null");
            if (gameStateToLoad == null)
            {
                //TODO major fuck up?
                Assert.IsTrue(false, "cant load game state, is null");
                return;
            }

            ResetToDefaultState();

            //!!LOAD LOCATION WITH CHARACTERS
            LocationCore location;
            LocationState locationState;
            for (int i = 0; i < gameStateToLoad.locations.Length; i++)
            {
                locationState = gameStateToLoad.locations[i];
                location = GetLocationById(locationState.id);
                Assert.IsNotNull(location, "cant change location from gameState, id=" + locationState.id + " not found in main location list");
                if (location == null)
                {
                    //!! major fuckup
                    Siren.Error("cant change location from gameState, id=" + locationState.id + " not found in main location list");
                    continue;
                }
                location.SetNameIndex(locationState.nameIndex);
                location.isOpened = locationState.isOpened;
               // Debug.Log(location.editorName);
                //Debug.Log(location.editorName);
                if (locationState.onOpenEntryPoint.IsNotEmpty())
                {
                    //Debug.Log("isnotemp");
                    location.onOpenEntryPoint = GetEntryPointByName(locationState.onOpenEntryPoint.id);
                } else {
                    //Debug.Log("istempy");
                    location.onOpenEntryPoint = null;
                }
                

                location.SetCharactersTransformsIndexes(locationState.CharacterTransformIndices);
                location.SetCharactersActivitiesIndexes(locationState.CharactersActivitiesIndexes);

            }

            CharacterCore character;
            CharacterState characterState;
            for (int j = 0; j < gameStateToLoad.characters.Length; j++)
            {
                characterState = gameStateToLoad.characters[j];
                character = GetCharacterById(characterState.id);
                Assert.IsNotNull(character, "cant change character from gameState, id=" + characterState.id + " not found in main character list");
                if (character == null)
                {
                    //!! major fuckup
                    Siren.Error("cant change character from gameState, id=" + characterState.id + " not found in main character list");
                    continue;
                }

                if (characterState.dialogEntryPoint.IsNotEmpty())
                {
                    character.currentDialogEntryPoint = GetEntryPointByName(characterState.dialogEntryPoint.id);
                } else
                {
                    character.currentDialogEntryPoint = null;
                }

                if (characterState.examineEntryPoint.IsNotEmpty())
                {
                    character.currentExamineEntryPoint = GetEntryPointByName(characterState.examineEntryPoint.id);
                } else
                {
                    character.currentExamineEntryPoint = null;
                }


                character.SetNameIndex(characterState.nameIndex);
                character.visible = characterState.visible;
                character.mouseInteractive = characterState.mouseInteractive;
                character.talkative = characterState.talkative;

            }

            LocationController.Instance.ApplyCharactersCoreState();

            //MultiOptionState multiOptionState;
            MultiOption multiOption;
            for (int i = 0; i < gameStateToLoad.multiOptions.Length; i++)
            {
                multiOption = GetMultiOptionByName(gameStateToLoad.multiOptions[i].id);
                Assert.IsNotNull(multiOption, "cant find "+ gameStateToLoad.multiOptions[i].id);
                multiOption.SetDialogOptionStatesTo(gameStateToLoad.multiOptions[i].dialogOptionStates);
            }

            ItemCore item;
            for (int i = 0; i < gameStateToLoad.items.Length; i++)
            {
                item = GetItemById(gameStateToLoad.items[i].id);
                item.SetNameIndex(gameStateToLoad.items[i].nameIndex);
                item.currentExamineEntryPoint = GetEntryPointByName(gameStateToLoad.items[i].examineEntryPoint.id);
            }

            referenceLinkHelper.inventory.items.Clear();
            for (int i = 0; i < gameStateToLoad.inventory.itemsInInventory.Length; i++)
            {
                referenceLinkHelper.inventory.items.Add(GetItemById(gameStateToLoad.inventory.itemsInInventory[i]));
            }

            if (LocationController.Instance.location.uniqueId != gameStateToLoad.currentLocationId)
            {
                VisitLocation(GetLocationById(gameStateToLoad.currentLocationId));
            } else
            {
                currentLocation = GetLocationById(gameStateToLoad.currentLocationId);
            }

            

            Siren.EditorOkLog("game loaded");
        }


#if UNITY_EDITOR

        public void SaveGameStateToSO(string name)
        {
            if (name == "")
            {
                Assert.IsTrue(false, "cant load game state, is null");
                return;
            }
            if (AssetDatabase.LoadAssetAtPath("Assets/Jineni/Game/GameStates/" + name + ".asset", typeof(GameState)) != null)
            {
                Assert.IsTrue(false, "Assets/Jineni/Game/GameStates/" + name + ".asset already exists");
                return;
            }

            UpdateRefLinks();

            GameState gameState = ScriptableObject.CreateInstance<GameState>();
            gameState.name = name;
            AssetDatabase.CreateAsset(gameState, "Assets/Jineni/Game/GameStates/" + name + ".asset");

            LocationState locationState;
            CharacterState characterState;
            ItemState itemState;
            MultiOptionState multiOptionState;


            //!!LOAD LOCATION WITH CHARACTERS
            gameState.locations = new LocationState[locations.Count];
            for (int i = 0; i < locations.Count; i++)
            {
                //!! nebude se reference vztekat?
                locationState = new LocationState
                {
                    editorName = "L"+locations[i].uniqueId+"-"+locations[i].editorName,
                    id = locations[i].uniqueId,

                    nameIndex = locations[i].currentNameIndex,
                    isOpened = locations[i].isOpened
                };

                if (locations[i].onOpenEntryPoint != null)
                {
                    EntryPointState onOpenEP = new EntryPointState
                    {
                        id = locations[i].onOpenEntryPoint.uniqueId.ToString()
                    };

                    locationState.onOpenEntryPoint = onOpenEP;
                }

                locationState.CharacterTransformIndices = locations[i].GetCharactersTransformsIndexes();
                locationState.CharactersActivitiesIndexes = locations[i].GetCharactersActivitiesIndexes();

                gameState.locations[i] = locationState;
            }
            gameState.currentLocationId = LocationController.Instance.location.uniqueId;

            gameState.characters = new CharacterState[characters.Count];
            for (int i = 0; i < characters.Count; i++)
            {
                //!! nebude se reference vztekat?
                characterState = new CharacterState
                {
                    editorName = (characters[i].uniqueId>100 ? "O" : "P") + characters[i].uniqueId + "-" + characters[i].editorName,
                    id = characters[i].uniqueId,                    
                    nameIndex = characters[i].currentNameIndex,
                    visible = characters[i].visible,
                    mouseInteractive = characters[i].mouseInteractive,
                    talkative = characters[i].talkative
                };

                if (characters[i].currentDialogEntryPoint != null)
                {
                    EntryPointState dialogEp = new EntryPointState
                    {
                        id = characters[i].currentDialogEntryPoint.uniqueId.ToString()
                    };
                    characterState.dialogEntryPoint = dialogEp;
                }

                if (characters[i].currentExamineEntryPoint != null)
                {
                    EntryPointState examineEp = new EntryPointState
                    {
                        id = characters[i].currentExamineEntryPoint.uniqueId.ToString()
                    };
                    characterState.examineEntryPoint = examineEp;
                }

                gameState.characters[i] = characterState;
            }

            gameState.items = new ItemState[items.Count];
            for (int i = 0; i < items.Count; i++)
            {
                itemState = new ItemState
                {
                    editorName = "I" + items[i].uniqueId + "-" + items[i].editorName,
                    id = items[i].uniqueId,
                    nameIndex = items[i].currentNameIndex
                };
                //!! nebude se reference vztekat?
                if (items[i].currentExamineEntryPoint != null)
                {
                    EntryPointState examinationEntryPoint = new EntryPointState
                    {
                        id = items[i].currentExamineEntryPoint.uniqueId.ToString()
                    };
                    itemState.examineEntryPoint = examinationEntryPoint;
                }

                gameState.items[i] = itemState;
            }

            List<ItemCore> itemsInInventory = referenceLinkHelper.inventory.items;
            gameState.inventory.itemsInInventory = new int[itemsInInventory.Count];
            for (int i = 0; i < itemsInInventory.Count; i++)
            {
                gameState.inventory.itemsInInventory[i] = itemsInInventory[i].uniqueId;
            }


            gameState.multiOptions = new MultiOptionState[changedMultiOptions.Count];
            List<DialogOptionState> dialogOptionStates = new List<DialogOptionState>();
            DialogOptionState dialogOptionState;
            for (int i = 0; i < changedMultiOptions.Count; i++)
            {
                dialogOptionStates.Clear();
                for (int j = 0; j < changedMultiOptions[i].dialogOptions.Count; j++)
                {
                    dialogOptionState = new DialogOptionState()
                    {
                        isActive = changedMultiOptions[i].dialogOptions[j].isCurrentlyActive,
                        isVisited = changedMultiOptions[i].dialogOptions[j].isVisited
                    };
                    dialogOptionStates.Add(dialogOptionState);
                }
                //!! nebude se reference vztekat?
                multiOptionState = new MultiOptionState
                {
                    id = changedMultiOptions[i].uniqueId,
                    dialogOptionStates = dialogOptionStates
                };

                gameState.multiOptions[i] = multiOptionState;
            }


            EditorUtility.SetDirty(gameState);
            AssetDatabase.SaveAssets();

            Debug.Log(JsonUtility.ToJson(gameState));
        }
        

        public void UpdateRefLinks()
        {
            CharacterCore[] skipChars = new CharacterCore[2] { referenceLinkHelper.playerCharacter, referenceLinkHelper.enviromentCharacter };
            characters = new List<CharacterCore>(Finder.GetScriptableObjectsWithout(skipChars));
            locations = new List<LocationCore>(Finder.GetScriptableObjects<LocationCore>());
            items = new List<ItemCore>(Finder.GetScriptableObjects<ItemCore>());
            GetAllEntrypoints();
            GetAllMultiOptions();
            mapManager.UpdateRefList();
            changedMultiOptions.Clear();
            Siren.EditorNotice("GameManager reference lists updated");
            //!! todo fucked up
           /* dialogManager.gameObject.transform.localScale = new Vector3(1.1f, 1.1f, 1.1f);
            dialogManager.gameObject.transform.localScale = new Vector3(1, 1, 1);*/
            /* EditorApplication.MarkSceneDirty();
             EditorUtility.SetDirty(this);
             AssetDatabase.SaveAssets();*/
        }
#endif
    }
}