﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;
#if UNITY_EDITOR
using UnityEditor.Callbacks;
using Jineni.DialogSystem.Graph;
using Jineni.Game;
using Jineni.Utils;
using Jineni.EditorWindows;

namespace Jineni.DialogSystem
{
    public class DialogEditor : EditorWindow
    {
        /*static DialogEditor _instance = null;
        public static DialogEditor Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = Finder.GetLoneScriptableObjects<DialogEditor>();// FindObjectOfType<DialogEditor>();
                    if (_instance == null)
                    {
                        //!! nasledujici radek je zasadni, jinak se editor rozbije
                        _instance = CreateInstance<DialogEditor>() as DialogEditor;
                    }
                }
                return _instance;
            }
        }*/

        public static Lang currentLang;

        private static Vector2 offset;
        private static Vector2 drag;
       // static Vector2 draggetGraphOffset;
        public static float zoom = 1;
        private static Vector2 mousePosition;

        //private static string krumloficationName;
        //[HideInInspector]
        public static Dialog currentDialog;

        static Port selectedConnectionOutPort = null;
        static Port selectedConnectionInPort = null;

/*

        private const float kZoomMin = 0.1f;
        private const float kZoomMax = 10.0f;

        private readonly Rect _zoomArea = new Rect(0.0f, 75.0f, 1200.0f, 300.0f - 100.0f);
        private float _zoom = 1.0f;
        private Vector2 _zoomCoordsOrigin = Vector2.zero;

        Vector2 lastZoomMousePosition;
        */

        //!!open by doublecklick
        [OnOpenAsset(0)]
        public static bool OnOpen(int instanceID, int line)
        { 
            //Debug.Log("trying to open"+Time.timeSinceLevelLoad);
            Dialog dialogToOpen = EditorUtility.InstanceIDToObject(instanceID) as Dialog;
            SettingsOfEditor settingsOfEditor = Finder.GetLoneScriptableObjects<SettingsOfEditor>();
            if (dialogToOpen != null)
            {
                currentDialog = dialogToOpen;
                settingsOfEditor.lastOpenedDialog = currentDialog;
                OpenWindow();
                return true;
            }

            Node nodeToOpen = EditorUtility.InstanceIDToObject(instanceID) as Node;
            if (nodeToOpen != null)
            {
                currentDialog = nodeToOpen.parentGraph;
                settingsOfEditor.lastOpenedDialog = currentDialog;
                OpenWindow();
                return true;
            }

            Port portToOpen = EditorUtility.InstanceIDToObject(instanceID) as Port;
            if (portToOpen != null)
            {
                currentDialog = portToOpen.attachedToNode.parentGraph;
                settingsOfEditor.lastOpenedDialog = currentDialog;
                OpenWindow();
                return true;
            }

            Connection connectionToOpen = EditorUtility.InstanceIDToObject(instanceID) as Connection;
            if (connectionToOpen != null)
            {
                currentDialog = connectionToOpen.inPort.attachedToNode.parentGraph;
                settingsOfEditor.lastOpenedDialog = currentDialog;
                OpenWindow();
                return true;
            }
            return false;
        }

        static void OpenWindow()
        {
            // Get existing open window or if none, make a new one:
            DialogEditor window = GetWindow<DialogEditor>();
            window.titleContent = new GUIContent(GetKrumloficationName());
            window.wantsMouseMove = true;
            window.Show();
            FocusWindowIfItsOpen<DialogEditor>();

            SettingsOfEditor settingsOfEditor = Finder.GetLoneScriptableObjects<SettingsOfEditor>();
            currentLang = settingsOfEditor.currentLang; 
            selectedConnectionInPort = null;
            selectedConnectionOutPort = null;
        }


        static string GetKrumloficationName()
        {
            if (currentDialog == null)
            {
                return "new dialog";
            } else
            {
                return currentDialog.name;
            }
        }

        public static void OpenEditor(Dialog krumlofication)
        {
            currentDialog = krumlofication;
            //Debug.Log(krumlofication.name);
            OpenWindow();
        }
        

        public static EntryPoint CreateNewDialog(string name, string assetFolderPath)
        {
            Dialog newDialog = CreateInstance<Dialog>();
            newDialog.name = name;

            AssetDatabase.CreateAsset(newDialog, assetFolderPath + name + ".asset");
            AssetDatabase.SaveAssets();
            return newDialog.CreateNode<EntryPoint>(new Vector2(50, 150)) as EntryPoint;
        }
        
        private void OnGUI()
        {

            // GUI.backgroundColor = DialogEditorStyles.colors.fieldBackground;

            DrawGrids();



            //skoro, ale hodne experimental
            ApplyZoom();

            DrawNodes();

            DrawConnections();

            DrawConnectionLine(Event.current);

            ProcessNodeEvents(Event.current);
            ProcessEvents(Event.current);

            EndZoom();
            //ApplyZoom();

            DrawControlButtons();


            if (GUI.changed)
            {
                Repaint();
            }
        }

        bool HasDialog()
        {
            return currentDialog != null;
        }

        static void HasDialogCheck()
        {
            if (currentDialog == null)
            {
                Assert.IsTrue(false, "editor lost focus, reopen dialog");
                throw new System.Exception("current dialog editor window lost focus");
                //TODO zjistit kdy se deje a zas nejak vypokusovat

                /*
                OpenWindow();
                SettingsOfEditor settingsOfEditor = Finder.GetLoneScriptableObjects<SettingsOfEditor>();
                if(settingsOfEditor==null) //if(settingsOfEditor==null || settingsOfEditor.lastOpenedDialog == null)
                {
                    Assert.IsTrue(false, "editor lost focus, reopen dialog");
                }
                //todo reopen
                currentDialog = settingsOfEditor.lastOpenedDialog;
               */
                
            }
        }

        void DrawControlButtons()
        {
            //Debug.Log(zoom + "  "+ drag);
            GUILayout.BeginArea(new Rect(0, 20+10*zoom, Screen.width, 300));
            if (GUILayout.Button("Focus On First Entry point"))
            {
                FocusOnFirstEP();

            }
            if (GUILayout.Button("SAVE(for certainty, just in case of unexpected"))
            {
                if (HasDialog())
                {
                    Debug.Log("SAVED " + Time.realtimeSinceStartup);
                    AssetDatabase.SaveAssets();
                }

            }
            if (GUILayout.Button("Test Dialog Integrity"))
            {
                if (HasDialog())
                {
                    IntegrityCheckerEditor.TestOneDialog(currentDialog);
                }
            }
            if (GUILayout.Button("Apply dialog name change to node names"))
            {
                if (HasDialog())
                {
                    ApplyNameChange();
                }
            }
            if (GUILayout.Button("switch cestina/anglictina"))
            {

                //int currentLang = Setting.Lang == 0  ? Setting.Lang = 1 : Setting.Lang=0;
                currentLang = currentLang == Lang.cs ? Lang.en : Lang.cs;
                Debug.Log(currentLang);

                if(HasDialog())
                {
                    currentDialog.SwitchLang(currentLang);
                }
                
            }
            GUILayout.EndArea();
        }

        void FocusOnFirstEP()
        {
            if (!HasDialog())
            {
                Siren.EditorError("no dialog selected");
                return;
            }
            EntryPoint firstEP = null;
            for (int i = 0; i < currentDialog.nodes.Count; i++)
            {
                if(currentDialog.nodes[i] is EntryPoint)
                {
                    firstEP = currentDialog.nodes[i] as EntryPoint;
                    break;
                }
            }
            if(firstEP == null)
            {
                Siren.EditorWarn("there is no EntryPoint in Dialog");
                return;
            }
            drag = new Vector2(50, 150) - firstEP.rect.TopLeft();
            currentDialog.OnDrag(drag);
        }

        public void ApplyNameChange()
        {
            ApplyNameChangeFor(currentDialog);
        }

        public static void ApplyNameChangeFor(Dialog dialog)
        {
            string nodeName;
            Node node;
            for (int i = 0; i < dialog.nodes.Count; i++)
            {
                node = dialog.nodes[i];
                nodeName = string.Concat(dialog.name, "-", node.nameWithinDialog);
                node.uniqueId = nodeName;
                node.name = nodeName;
                //node.name = dialog.name;
                if (node.inPort != null)
                {
                    node.inPort.name = string.Concat(nodeName, "-", Node.INPORT);
                }

                for (int j = 0; j < node.outPorts.Count; j++)
                {
                    node.outPorts[j].name = string.Concat(nodeName, "-", Node.OUTPORT);
                }
            }

            for (int i = 0; i < dialog.connections.Count; i++)
            {
                dialog.connections[i].name = string.Concat(dialog.connections[i].inPort.attachedToNode.uniqueId, " -- ", dialog.connections[i].outPort.attachedToNode.uniqueId);
            }
            AssetDatabase.SaveAssets();
        }

        void DrawGrids()
        {
            DrawGrid(20, 0.2f, Color.gray);
            DrawGrid(100, 0.4f, Color.gray);
        }

        private void DrawConnections()
        {
            HasDialogCheck();
            currentDialog.DrawConnection();
        }

        private void DrawNodes()
        {
            HasDialogCheck();
            currentDialog.DrawNodes();

        }

        private void ApplyZoom()
        {
            //!!dont know, asi nechat neco dela?
            //GUI.EndClip();
            //Debug.Log("apply zomm" + zoom);
            //TODO opravdu dle pozice mysi
            GUI.EndGroup();
            Matrix4x4 oldMatrix = GUI.matrix;
            //new Rect(0.0f, 75.0f, 600.0f, 300.0f - 100.0f);
           // new Rect(0.0f, 75.0f, Screen.width, Screen.height * 5 - 100.0f);
            //!!nechat velky
            GUI.BeginGroup(new Rect(0.0f, 0, Screen.width*5, Screen.height*5));
            //Scale my gui matrix
            Matrix4x4 Translation = Matrix4x4.TRS(new Vector2(0, 0), Quaternion.identity, Vector3.one);
            Matrix4x4 Scale = Matrix4x4.Scale(new Vector3(zoom, zoom, 1.0f));
            GUI.matrix = Translation * Scale * Translation.inverse;

            GUI.matrix = oldMatrix;

            //Debug.Log(Event.current.type);
            /*if(Event.current.type== EventType.ScrollWheel)
            {
                lastZoomMousePosition = mousePosition;
            }*/

            GUIUtility.ScaleAroundPivot(Vector2.one / zoom, Vector2.zero);
            //GUIUtility.ScaleAroundPivot(Vector2.one / zoom, lastZoomMousePosition);
            /* GUI.EndGroup();
             GUI.BeginGroup(new Rect(0.0f, 0, Screen.width, Screen.height));*/

        }

        void EndZoom()
        {
            GUI.EndGroup();
            GUI.BeginGroup(new Rect(0.0f, 0, Screen.width*5, Screen.height*5));
        }

        private void DrawConnectionLine(Event e)
        {
            //Debug.Log(selectedInPort + " xxxx " + selectedOutPort);
            if (selectedConnectionOutPort != null && selectedConnectionInPort == null)
            {
                //Tools.DrawConnectionLine(selectedInPort.AbsCenter, e.mousePosition);

                Handles.DrawBezier(
                    selectedConnectionOutPort.centerInAbsolute,
                    e.mousePosition,
                    selectedConnectionOutPort.centerInAbsolute + Vector2.left * 50f,
                    e.mousePosition - Vector2.left * 50f,
                    DialogEditorStyles.connectionColor,
                    null,
                    DialogEditorStyles.connectionLineSize
                );

                GUI.changed = true;
            }

            if (selectedConnectionInPort != null && selectedConnectionOutPort == null)
            {

                //Tools.DrawConnectionLine(selectedOutPort.AbsCenter, e.mousePosition);
                Handles.DrawBezier(
                    selectedConnectionInPort.centerInAbsolute,
                    e.mousePosition,
                    selectedConnectionInPort.centerInAbsolute - Vector2.left * 50f,
                    e.mousePosition + Vector2.left * 50f,
                    DialogEditorStyles.connectionColor,
                    null,
                    DialogEditorStyles.connectionLineSize
                );

                GUI.changed = true;
            }
        }

        private void DrawGrid(float gridSpacing, float gridOpacity, Color gridColor)
        {
            int widthDivs = Mathf.CeilToInt(position.width / gridSpacing);
            int heightDivs = Mathf.CeilToInt(position.height / gridSpacing);

            Handles.BeginGUI();
            Handles.color = new Color(gridColor.r, gridColor.g, gridColor.b, gridOpacity);

            offset += drag * 0.5f;
            Vector3 newOffset = new Vector3(offset.x % gridSpacing, offset.y % gridSpacing, 0);

            for (int i = 0; i < widthDivs; i++)
            {
                Handles.DrawLine(new Vector3(gridSpacing * i, -gridSpacing, 0) + newOffset, new Vector3(gridSpacing * i, position.height, 0f) + newOffset);
            }

            for (int j = 0; j < heightDivs; j++)
            {
                Handles.DrawLine(new Vector3(-gridSpacing, gridSpacing * j, 0) + newOffset, new Vector3(position.width, gridSpacing * j, 0f) + newOffset);
            }

            Handles.color = Color.white;
            Handles.EndGUI();
        }



        private void OnClickAddReplic(Vector2 mousePosition)
        {
            HasDialogCheck();
            //currentKrumlofication.CreateReplic(mousePosition);
            currentDialog.CreateNode<Replic>(mousePosition);
        }

        private void OnClickAddEntryPoint(Vector2 mousePosition)
        {
            HasDialogCheck();
            currentDialog.CreateNode<EntryPoint>(mousePosition);
        }


        private void OnClickAddMultiOption(Vector2 mousePosition)
        {
            HasDialogCheck();
            //currentKrumlofication.CreateMultiOption(mousePosition);
            currentDialog.CreateNode<MultiOption>(mousePosition);
        }

        private void OnClickAddConditional(Vector2 mousePosition)
        {
            HasDialogCheck();
            currentDialog.CreateNode<Condition>(mousePosition);
        }

        private void OnClickAddTrigger(Vector2 mousePosition)
        {
            HasDialogCheck();
            currentDialog.CreateNode<Trigger>(mousePosition);
        }

        

        public static void OnClickRemoveNode(Node node)
        {
            HasDialogCheck();
            currentDialog.RemoveNode(node);
        }


        public static void OnClickNodeInPort(Port inPort)
        {
            
            //!!node inport is connection outport
            selectedConnectionOutPort = inPort;


            //Debug.Log("selected in");
            if (selectedConnectionInPort != null)
            {
                if (selectedConnectionOutPort.attachedToNode != selectedConnectionInPort.attachedToNode)
                {
                    //Debug.Log("selected in and out");
                    CreateConnection();
                    ClearConnectionSelection();
                }
                else
                {
                    ClearConnectionSelection();
                }
            }
        }

        public static void OnClickNodeOutPort(Port outPort)
        {
            //!!node inport is connection outport
            //Debug.Log("selected out");
            selectedConnectionInPort = outPort;

            if (selectedConnectionOutPort != null)
            {
                if (selectedConnectionOutPort.attachedToNode != selectedConnectionInPort.attachedToNode)
                {
                   // Debug.Log("selected out and in");

                    CreateConnection();
                    ClearConnectionSelection();
                }
                else
                {
                    ClearConnectionSelection();
                }
            }
        }

        public static void OnClickRemoveConnection(Connection connection)
        {
            HasDialogCheck();
            currentDialog.RemoveConnectionAndSaveAssets(connection);
        }


        public static void CreateConnection()
        {
            HasDialogCheck();
            currentDialog.CreateConnection(selectedConnectionInPort, selectedConnectionOutPort);
        }

        public static void ClearConnectionSelection()
        {
            selectedConnectionOutPort = null;
            selectedConnectionInPort = null;
        }        


        private void ProcessNodeEvents(Event e)
        {

            HasDialogCheck();
            currentDialog.ProcessEvents(e);
        }

        private void ProcessEvents(Event e)
        {
            drag = Vector2.zero;
            mousePosition = e.mousePosition;
            switch (e.type)
            {
                case EventType.ScrollWheel:
                    if (e.delta.y > 0)
                    {
                        
                        if (zoom < 2f)
                        {
                            //Debug.Log(zoom);
                            zoom += 0.01f;
                        }
                    }
                    else
                    {
                        //Debug.Log(zoom);
                        if (zoom > 0.3f)
                        {
                            //Debug.Log(zoom);
                            zoom -= 0.01f;
                        }
                    }
                    GUI.changed = true;
                    break;
                case EventType.MouseDown:
                    if (e.button == 0)
                    {
                        ClearConnectionSelection();
                    }

                    if (e.button == 1)
                    {
                        //Debug.Log("second");
                        ProcessContextMenu(e.mousePosition);
                    }
                    break;

                case EventType.MouseDrag:
                    if (e.button == 2)
                    {
                        OnDrag(e.delta);
                    }
                    break;
            }
        }

        private void OnDrag(Vector2 delta)
        {
            //Debug.Log(delta);
            drag = delta;
            HasDialogCheck();
            currentDialog.OnDrag(delta);
        }

        private void ProcessContextMenu(Vector2 mousePosition)
        {
            GenericMenu genericMenu = new GenericMenu();
            genericMenu.AddItem(new GUIContent("add ReplicNode"), false, () => OnClickAddReplic(mousePosition));
            genericMenu.AddItem(new GUIContent("add EntryPoint"), false, () => OnClickAddEntryPoint(mousePosition));
            genericMenu.AddItem(new GUIContent("add MultiOption"), false, () => OnClickAddMultiOption(mousePosition));
            genericMenu.AddItem(new GUIContent("add Conditinal"), false, () => OnClickAddConditional(mousePosition));
            genericMenu.AddItem(new GUIContent("add Trigger"), false, () => OnClickAddTrigger(mousePosition));
            //Debug.Log("ShowAsContext");
            genericMenu.ShowAsContext();
        }


    }
    
}
#endif