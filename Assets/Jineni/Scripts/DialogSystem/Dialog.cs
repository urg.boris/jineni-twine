﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Linq;
using Jineni.DialogSystem.Graph;
using Jineni.Game;


namespace Jineni.DialogSystem
{
    [System.Serializable]
    [CreateAssetMenu(menuName = "Jineni/new Dialog(flow control)", order = 0)]
    public class Dialog : ScriptableObject
    {
        //TODO zmerit, pouzit i jinde?
        const int NODE_LIMIT = 50;
        const int CONNECTION_LIMIT = 50;

        [SerializeField] public List<Node> nodes = new List<Node>(NODE_LIMIT);

        [SerializeField] public List<Connection> connections = new List<Connection>(CONNECTION_LIMIT);

        /* [SerializeField]
         public List<EntryPoint> entryPoints = new List<EntryPoint>();*/
        [HideInInspector] public int i = 0;


        //=============================================  EDITOR  ==============================================
#if UNITY_EDITOR
        [HideInInspector] public string corruptionReport;

        public Node CreateNode<T>(Vector2 position) where T : Node
        {
            if (nodes == null)
            {
                nodes = new List<Node>(NODE_LIMIT);
            }

            T node = ScriptableObject.CreateInstance<T>();
            string nodeName = GetNodeNameByType(node) + "-" + i++;
            node.Init(this, position, nodeName);
            nodes.Add(node);

            AssetDatabase.AddObjectToAsset(node, this);
            AssetDatabase.SaveAssets();
            //!!PORTY PROTOZE SOU SCRIPTABLE OBJ, TAK PRAVDEPODOBNE MUSI BYT NODE ASSETT JIZ ULOZEN V ASSETS, PREDTIM CO SE VYTVARI
            node.RunAfterAssetSaved();

            return node;
        }

        public void RemoveAllNodesAndConnections()
        {
            Object[] objs = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(this));

            //List<ScriptableObject> ofType = new List<ScriptableObject>();

            foreach (Object o in objs)
            {
                //skip parent
                if (this == o)
                {
                    continue;
                }

                // Debug.Log(o.name);
                DestroyImmediate(o, true);
            }

            connections = new List<Connection>();
            nodes = new List<Node>();
            AssetDatabase.SaveAssets();
        }

        public bool IsEmpty()
        {
            return nodes.Count == 0;
        }

        string GetNodeNameByType(Node node)
        {
            if (node is Condition)
            {
                return "C";
            }
            else if (node is EntryPoint)
            {
                return "EP";
            }
            else if (node is MultiOption)
            {
                return "MO";
            }
            else if (node is Replic)
            {
                return "R";
            }
            else if (node is Trigger)
            {
                return "T";
            }

            //!!no error handling because verka is comming
            return "";
        }

        public Node FindCorruptedNode()
        {
            for (int i = 0; i < nodes.Count; i++)
            {
                if (nodes[i].IsCorrupted())
                {
                    return nodes[i];
                }
            }

            return null;
        }

        public bool IsCorrupted()
        {
            Node node = FindCorruptedNode();
            if (node)
            {
                corruptionReport = name + " is corrupted: " + node.name + ": " + node.corruptionReport;
                return true;
            }

            return false;
        }

        public void SwitchLang(Lang lang)
        {
            //!! deselect focus, jinak se spravne nezmeni v textboxu
            GUI.FocusControl(null);
            for (int i = 0; i < nodes.Count; i++)
            {
                nodes[i].SwitchLangTo(DialogEditor.currentLang);
            }
        }


        public void DrawNodes()
        {
            if (nodes != null)
            {
                for (int i = 0; i < nodes.Count; i++)
                {
                    nodes[i].Draw();
                }
            }
        }

        public void DrawConnection()
        {
            //Debug.Log("DrawConnection");
            if (connections != null)
            {
                for (int i = 0; i < connections.Count; i++)
                {
                    connections[i].Draw();
                }
            }
        }

        public void OnDrag(Vector2 delta)
        {
            if (nodes != null)
            {
                for (int i = 0; i < nodes.Count; i++)
                {
                    nodes[i].Drag(delta);
                }
            }

            GUI.changed = true;
        }

        public void CreateConnection(Port selectedInPort, Port selectedOutPort)
        {
            if (connections == null)
            {
                connections = new List<Connection>(CONNECTION_LIMIT);
            }

            for (int i = 0; i < connections.Count; i++)
            {
                if (connections[i].inPort == selectedInPort && connections[i].outPort == selectedOutPort)
                {
                    Debug.Log("Connection already exists");
                    return;
                }
            }

            Connection newConnection = ScriptableObject.CreateInstance(typeof(Connection)) as Connection;
            newConnection.Init(selectedInPort, selectedOutPort);


            string name = selectedInPort.attachedToNode.name + " -- " + selectedOutPort.attachedToNode.name;
            newConnection.name = name; // UnityEditor.ObjectNames.NicifyVariableName(name);
            connections.Add(newConnection);

            AssetDatabase.AddObjectToAsset(newConnection, this);
            AssetDatabase.SaveAssets();
        }

        public void RemoveConnectionAndSaveAssets(Connection connection)
        {
            RemoveConnection(connection);
            AssetDatabase.SaveAssets();
        }

        public void RemoveConnection(Connection connection)
        {
            connection.outPort.attachedToNode.inPort.connections.Remove(connection);
            for (int i = 0; i < connection.inPort.attachedToNode.outPorts.Count; i++)
            {
                connection.inPort.attachedToNode.outPorts[i].connections.Remove(connection);
            }

            connections.Remove(connection);
            Object.DestroyImmediate(connection, true);
        }

        public void RemoveNode(Node node)
        {
            //Debug.Log("Remove node");
            if (connections != null)
            {
                List<Connection> connectionsToRemove = new List<Connection>();

                for (int i = 0; i < connections.Count; i++)
                {
                    //Debug.Log("connections.Count"+ connections.Count+"  "+i);
                    for (int j = 0; j < node.outPorts.Count; j++)
                    {
                        if (connections[i].inPort == node.outPorts[j])
                        {
                            connectionsToRemove.Add(connections[i]);
                        }
                    }

                    if (connections[i].outPort == node.inPort)
                    {
                        connectionsToRemove.Add(connections[i]);
                    }
                }

                for (int i = 0; i < connectionsToRemove.Count; i++)
                {
                    RemoveConnection(connectionsToRemove[i]);
                }

                connectionsToRemove = null;
            }

            node.OnRemoveNode();
            nodes.Remove(node);
            node.DestoryPortsFromAssets();
            Object.DestroyImmediate(node, true);
            AssetDatabase.SaveAssets();
        }

        public void ProcessEvents(Event e)
        {
            if (nodes != null)
            {
                for (int i = nodes.Count - 1; i >= 0; i--)
                {
                    bool guiChanged = nodes[i].ProcessEvents(e);

                    if (guiChanged)
                    {
                        GUI.changed = true;
                    }
                }
            }
        }
#endif

//=============================================  FOR GAME  ==============================================

        public Node StartFrom(EntryPoint entryPoint)
        {
            return entryPoint.GetNextNode();
        }
/*
public void BackupToXML()
{
    XmlSerializer serializer = new XmlSerializer(typeof(Hero));
    StreamWriter writer = new StreamWriter("hero.xml");
    serializer.Serialize(writer.BaseStream, knight);
    writer.Close();
}
*/
    }
}