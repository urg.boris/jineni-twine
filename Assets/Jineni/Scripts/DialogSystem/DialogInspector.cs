﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using Jineni.EditorWindows;

namespace Jineni.DialogSystem
{
    [CustomEditor(typeof(Dialog))]
    public class DialogInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            Dialog dialog = (Dialog)target;
            if (GUILayout.Button("Open Editor Window"))
            {
                DialogEditor.OpenEditor(dialog);
            }
            if (GUILayout.Button("Test This Dialog"))
            {
                IntegrityCheckerEditor.TestOneDialog(dialog);
            }
            if (GUILayout.Button("Apply dialog name change to node names"))
            {
                DialogEditor.ApplyNameChangeFor(dialog);
            }

            DrawDefaultInspector();
        }
    }
}
#endif