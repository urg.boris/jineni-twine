﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.DialogSystem;

namespace Jineni.DialogSystem.FlowControl.Conditions
{

    //[CreateAssetMenu(menuName = "Jineni/editor-conditionType", order = 22)]
    //[System.Serializable]
    public abstract class ConditionType : ScriptableObject
    {
        //public ScriptableObject tester;

        public abstract bool IsTrue(ScriptableObject variable);
    }
}
