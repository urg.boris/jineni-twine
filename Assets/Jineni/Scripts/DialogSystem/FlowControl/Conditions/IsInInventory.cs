﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;

namespace Jineni.DialogSystem.FlowControl.Conditions
{
    [CreateAssetMenu(fileName = "IsInInventory", menuName = "Jineni/Internal/Condition/IsInInventory", order = 2)]
    public class IsInInventory : ConditionType
    {
        public Inventory inventory;

        public override bool IsTrue(ScriptableObject variable)
        {
            //return (variable as Item).currentNameIndex == 0;
            return inventory.IsInInventory(variable as ItemCore);
        }
    }
}
