﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using UnityEngine.Assertions;

namespace Jineni.DialogSystem.FlowControl.Conditions
{
    [CreateAssetMenu(fileName = "OddnessReached", menuName = "Jineni/Internal/Condition/OddnessReached", order = 5)]
    public class OddnessReached : ConditionType
    {
        public Inventory inventory;

        public override bool IsTrue(ScriptableObject variable)
        {
            Assert.IsTrue(false, GetType().Name + " must have two arguments");
            Siren.Error(GetType().Name + " must have two arguments");
            return false;
        }

        public bool IsTrue(int intensity)
        {
            return inventory.IsOddnessReached(intensity);
        }
    }
}
