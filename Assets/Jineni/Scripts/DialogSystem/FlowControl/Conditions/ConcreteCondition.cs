﻿using Jineni.Game.BuildingBlocks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Jineni.DialogSystem.FlowControl.Conditions
{

    public class ConcreteCondition : ScriptableObject
    {
        public ConditionType type;
        public ScriptableObject variable;
        public int intParam = -1;
        public CharacterCore character;
        public string[] stringOptions;


        public bool IsTrue()
        {
            if (type is OddnessReached)
            {
                return (type as OddnessReached).IsTrue(intParam);
            }
            if (type is CharacterIsInActivityState)
            {
                return (type as CharacterIsInActivityState).IsTrue(variable, character, intParam);
            }
            else
            {
                return type.IsTrue(variable);
            }
            
        }
    }
}
