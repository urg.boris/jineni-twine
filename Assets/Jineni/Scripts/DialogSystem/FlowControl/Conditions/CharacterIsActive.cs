﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;

namespace Jineni.DialogSystem.FlowControl.Conditions
{
    [CreateAssetMenu(fileName = "CharacterIsActive", menuName = "Jineni/Internal/Condition/CharacterIsActive", order = 1)]
    public class CharacterIsActive : ConditionType
    {
        public override bool IsTrue(ScriptableObject variable)
        {
            return ((variable as CharacterCore).talkative);
        }
    }
}
