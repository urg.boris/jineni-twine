﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using Jineni.Game;
using UnityEngine.Assertions;

namespace Jineni.DialogSystem.FlowControl.Conditions
{
    [CreateAssetMenu(fileName = "CharacterIsInActivityState", menuName = "Jineni/Internal/Condition/CharacterIsInActivityState", order = 1)]
    public class CharacterIsInActivityState : ConditionType
    {
        public bool IsTrue(ScriptableObject variable, CharacterCore character, int activityIndex)
        {
            return GameManager.Instance.IsCharacterInActivityState((variable as LocationCore), character, activityIndex);
        }

        public override bool IsTrue(ScriptableObject variable)
        {
            Assert.IsTrue(false, GetType().Name + " must have 3 arguments");
            Siren.Error(GetType().Name + " must have 3 arguments");
            return false;
        }
    }
}
