﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "LocationClose", menuName = "Jineni/Internal/Trigger/Location/LocationClose")]
    public class LocationClose : LocationTrigger
    {
        //public ScriptableObject tester;

        public override void Invoke(ScriptableObject variable)
        {
            (variable as LocationCore).CloseNotice();
        }
    }
}
