﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using Jineni.Game;
using UnityEngine.Assertions;
using Jineni.DialogSystem.Graph;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "LocationEntryPointChange", menuName = "Jineni/Internal/Trigger/Location/LocationEntryPointChange")]
    public class LocationEntryPointChange : LocationTrigger
    {
        public override void Invoke(ScriptableObject variable)
        {
            Assert.IsTrue(false, this.GetType().ToString() + " must have three arguments");
            Siren.Error(this.GetType().ToString() + " must have three arguments");
        }
        
        public void Invoke(ScriptableObject variable, EntryPoint ep)
        {
            (variable as LocationCore).ChangeEntryPoint(ep);
        }
    }
}
