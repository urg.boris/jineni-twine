﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using Jineni.DialogSystem.Graph;
using UnityEngine.Assertions;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "CharacterChangeDialogEntryPoint", menuName = "Jineni/Internal/Trigger/Character/CharacterChangeDialogEntryPoint")]
    public class CharacterChangeDialogEntryPoint : CharacterTrigger
    {
        //public ScriptableObject tester;

        public void Invoke(ScriptableObject variable, EntryPoint entryPoint)
        {
            (variable as CharacterCore).SetDialogEntryPoint(entryPoint);
        }
    }
}
