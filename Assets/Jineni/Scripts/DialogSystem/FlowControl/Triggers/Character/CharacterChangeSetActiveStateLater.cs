﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "CharacterChangeSetActiveStateLater", menuName = "Jineni/Internal/Trigger/Character/CharacterChangeSetActiveStateLater")]
    public class CharacterChangeSetActiveStateLater : CharacterChangeYesNoPropertyTrigger
    {
        public override void Invoke(ScriptableObject variable, bool yesNoProperty)
        {
            (variable as CharacterCore).SetActiveState(yesNoProperty);
        }
    }
}
