﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "CharacterChangeMouseInteractivityLater", menuName = "Jineni/Internal/Trigger/Character/CharacterChangeMouseInteractivityLater")]
    public class CharacterChangeMouseInteractivityLater : CharacterChangeYesNoPropertyTrigger
    {

        public override void Invoke(ScriptableObject variable, bool yesNoProperty)
        {
            (variable as CharacterCore).SetMouseInteractivity(yesNoProperty);
        }
    }
}
