﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "CharacterChangeMouseInteractivityNow", menuName = "Jineni/Internal/Trigger/Character/CharacterChangeMouseInteractivityNow")]
    public class CharacterChangeMouseInteractivityNow : CharacterChangeYesNoPropertyTrigger
    {

        public override void Invoke(ScriptableObject variable, bool yesNoProperty)
        {
            (variable as CharacterCore).SetMouseInteractivity(yesNoProperty, true);
        }
    }
}
