﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.DialogSystem.Graph;
using Jineni.Game;

namespace Jineni.DialogSystem.FlowControl.Triggers
{

    [CreateAssetMenu(fileName = "OptionMakeInactive", menuName = "Jineni/Internal/Trigger/General/OptionMakeInactive")]
    public class OptionMakeInactive : OptionChangeTrigger
    {

        public override void Invoke(ScriptableObject variable, int intParam)
        {
            MultiOption multiOption = (variable as MultiOption);
            multiOption.SetOptionState(intParam, false);
            GameManager.Instance.MultiOptionChagned(multiOption);
        }
    }
}
