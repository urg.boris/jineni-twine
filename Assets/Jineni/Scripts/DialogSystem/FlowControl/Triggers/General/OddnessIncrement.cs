﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using UnityEngine.Assertions;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "OddnessIncrement", menuName = "Jineni/Internal/Trigger/General/OddnessIncrement")]
    public class OddnessIncrement : OddnessTrigger
    {
        public override void Invoke(int i)
        {
            inventory.IncrementOddness(i);
        }
    }
}
