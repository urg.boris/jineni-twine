﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;


namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "OddnessDecrement", menuName = "Jineni/Internal/Trigger/General/OddnessDecrement")]
    public class OddnessDecrement : OddnessTrigger
    {
        public override void Invoke(int i)
        {
            inventory.DecrementOddness(i);
        }
    }
}
