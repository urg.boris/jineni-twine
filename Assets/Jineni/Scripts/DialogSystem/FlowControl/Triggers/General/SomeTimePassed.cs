﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using UnityEngine.Assertions;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "SomeTimePassed", menuName = "Jineni/Internal/Trigger/General/SomeTimePassed")]
    public class SomeTimePassed : TriggerType
    {
        public override void Invoke(ScriptableObject variable)
        {
            Assert.IsTrue(false, GetType().Name + " must have two arguments");
            //Siren.Error(GetType().Name + " must have two arguments");
        }

        public void Invoke(float time, string message)
        {
            //!!  is handled by dialog controller
            Assert.IsTrue(false, GetType().Name + " should be handeled by dialog manager");
        }
    }
}
