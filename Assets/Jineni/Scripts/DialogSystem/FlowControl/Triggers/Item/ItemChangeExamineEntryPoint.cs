﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using Jineni.DialogSystem.Graph;
using UnityEngine.Assertions;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "ItemChangeExamineEntryPoint", menuName = "Jineni/Internal/Trigger/Item/ItemChangeExamineEntryPoint")]
    public class ItemChangeExamineEntryPoint : TriggerType
    {
        public override void Invoke(ScriptableObject variable)
        {
            Assert.IsTrue(false, GetType().Name + " must have two arguments");
            Siren.Error(GetType().Name + " must have two arguments");
        }

        public void Invoke(ScriptableObject variable, EntryPoint entryPoint)
        {
            (variable as ItemCore).ChangeExamineEP(entryPoint);
        }
    }
}
