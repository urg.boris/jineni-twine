﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "ItemNotHave", menuName = "Jineni/Internal/Trigger/Item/ItemNotHave")]
    public class ItemNotHave : InventoryTrigger
    {
        public override void Invoke(ScriptableObject variable)
        {
            inventory.NotHaveItem(variable as ItemCore);
        }
    }
}
