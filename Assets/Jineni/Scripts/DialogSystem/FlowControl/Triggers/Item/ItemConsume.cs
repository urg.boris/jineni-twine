﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "ItemConsume", menuName = "Jineni/Internal/Trigger/Item/ItemConsume")]
    public class ItemConsume : InventoryTrigger
    {
        public override void Invoke(ScriptableObject variable)
        {
            inventory.ConsumeItem(variable as ItemCore);
        }
    }
}
