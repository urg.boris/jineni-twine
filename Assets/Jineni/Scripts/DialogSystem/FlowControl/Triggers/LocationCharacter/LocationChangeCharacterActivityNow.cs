﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using Jineni.DialogSystem.Graph;
using Jineni.Game;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "LocationChangeCharacterActivityNow", menuName = "Jineni/Internal/Trigger/LocationCharacter/LocationChangeCharacterActivityNow")]
    public class LocationChangeCharacterActivityNow : LocationChangeCharacterActivityTrigger
    {
        public override void Invoke(ScriptableObject variable, CharacterCore character, int activityIndex)
        {
            GameManager.Instance.ChangeLocationCharacterToActivity((variable as LocationCore), character, activityIndex, true);
        }
    }
}
