﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using UnityEngine.Assertions;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    //[CreateAssetMenu(fileName = "ItemTrigger", menuName = "Jineni/Internal/Trigger/ItemTrigger", order = 33)]
    public abstract class OddnessTrigger : TriggerType
    {
        public Inventory inventory;
        public override void Invoke(ScriptableObject variable)
        {
            Assert.IsTrue(false, GetType().Name + " must have two arguments");
            Siren.Error(GetType().Name + " must have two arguments");
        }
        public abstract void Invoke(int i);
    }
}
