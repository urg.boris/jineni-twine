﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    //[CreateAssetMenu(fileName = "ItemTrigger", menuName = "Jineni/Internal/Trigger/ItemTrigger", order = 33)]
    public abstract class InventoryTrigger : TriggerType
    {
        public Inventory inventory;

    }
}
