﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using Jineni.DialogSystem.Graph;
using UnityEngine.Assertions;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    public abstract class LocationChangeCharacterTrigger : TriggerType
    {

        public override void Invoke(ScriptableObject variable)
        {
            Assert.IsTrue(false, this.GetType().ToString() + " must have three arguments");
            Siren.Error(this.GetType().ToString() + " must have three arguments");
        }

        public abstract void Invoke(ScriptableObject variable, CharacterCore character, int activityIndex);


    }
}
