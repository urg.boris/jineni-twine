﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using Jineni.DialogSystem.Graph;
using UnityEngine.Assertions;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    public abstract class CharacterChangeYesNoPropertyTrigger : CharacterTrigger
    {
        public abstract void Invoke(ScriptableObject variable, bool yesNoproperty);
    }
}
