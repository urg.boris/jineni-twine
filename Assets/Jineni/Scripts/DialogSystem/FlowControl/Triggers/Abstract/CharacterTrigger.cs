﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using UnityEngine.Assertions;

namespace Jineni.DialogSystem.FlowControl.Triggers
{

    //[CreateAssetMenu(fileName = "CharacterTrigger", menuName = "Jineni/Internal/Trigger/CharacterTrigger", order = 31)]
    public abstract class CharacterTrigger : TriggerType
    {
        public override void Invoke(ScriptableObject variable)
        {
            Assert.IsTrue(false, GetType().Name + " must have two arguments");
            Siren.Error(GetType().Name + " must have two arguments");
        }
    }
}
