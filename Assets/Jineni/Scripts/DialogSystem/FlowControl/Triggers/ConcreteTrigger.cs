﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.DialogSystem.Graph;
using Jineni.Game.BuildingBlocks;

namespace Jineni.DialogSystem.FlowControl.Triggers
{

    public class ConcreteTrigger : ScriptableObject
    {
        public TriggerType type;
        public ScriptableObject variable;
        public int intParam = -1;
        public EntryPoint entryPoint;
        public string stringParam;
        public bool boolParam;
        public CharacterCore character;
        public float floatParam;
        public string[] stringOptions;

        public virtual void Invoke()
        {
            if (type is ItemChangeExamineEntryPoint)
            {
                (type as ItemChangeExamineEntryPoint).Invoke(variable, entryPoint);
            }
            else if (type is CharacterChangeDialogEntryPoint)
            {
                (type as CharacterChangeDialogEntryPoint).Invoke(variable, entryPoint);
            }
            else if (type is CharacterChangeExamineEntryPoint)
            {
                (type as CharacterChangeExamineEntryPoint).Invoke(variable, entryPoint);
            }
            else if(type is CharacterChangeName)
            {
                (type as CharacterChangeName).Invoke(variable, intParam);
            }
            else if (type is LocationChangeCharacterTrigger)
            {
                (type as LocationChangeCharacterTrigger).Invoke(variable, character, intParam);
            }
            else if (type is CharacterChangeYesNoPropertyTrigger)
            {
                (type as CharacterChangeYesNoPropertyTrigger).Invoke(variable, boolParam);
            }
            else if (type is OptionChangeTrigger)
            {
                (type as OptionChangeTrigger).Invoke(variable, intParam);
            }
            else if (type is OddnessTrigger)
            {
                (type as OddnessTrigger).Invoke(intParam);
            } else if (type is FloatTrigger)
            {
                (type as FloatTrigger).Invoke(floatParam);
            }
            else if (type is LocationEntryPointChange)
            {
                (type as LocationEntryPointChange).Invoke(variable, entryPoint);
            }
            else if (type is OddnessDecrement)
            {
                (type as OddnessDecrement).Invoke(intParam);
            }
            //!! SomeTimePassed se spousti dialogem
            /*else if (type is SomeTimePassed)
            {
                (type as SomeTimePassed).Invoke(floatParam, stringParam);
            }*/
            else
            {
                //Debug.Log("dasdas");
                type.Invoke(variable);
            }
            
        }
    }
}
