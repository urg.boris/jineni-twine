﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Jineni.Game.BuildingBlocks;
using Jineni.Game;
using Jineni.Localization;

namespace Jineni.DialogSystem.Graph
{
    public abstract class VocalNode : Node
    {
        public CharacterCore speaker;
        public AudioClip replicAudio;
        public Vector2 scroll;
        public Lang currentLang;
        public Translation translation = new Translation();
        
        public string replicText
        {
            get
            {
                return translation.Get(currentLang);
            }

            set
            {
                translation.Set(currentLang, value);
            }
        }

        //===================================EDITOR===============================
#if UNITY_EDITOR

        public override void Init(Dialog parentGraph, Vector2 position, string name)
        {
            base.Init(parentGraph, position, name);
        }



        public override void SwitchLangTo(Lang lang)
        {
            currentLang = lang;
        }

        public bool IsVocalNodeCorrupted()
        {
            if (replicText == "")
            {
                corruptionReport = "replic text not set";
                return true;
            }
            if (speaker == null)
            {
                corruptionReport = "speaker not set";
                return true;
            }
            return false;
        }


#endif
        public string GetText(Lang lang)
        {
            currentLang = lang;
            return replicText;
        }

    }


}