﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;
using Jineni.Game;
using UnityEngine.Assertions;
using Jineni.Utils;

namespace Jineni.DialogSystem.Graph
{
    //[System.Serializable]
    public abstract class Node : ScriptableObject
    {
        public const string OUTPORT = "outPort";
        public const string INPORT = "inPort";

        public string uniqueId;

        public string nameWithinDialog;

        /* in-outy porty
         * 1-1 replic 
         * 0-1 entrypoint 
         * 1-n multiopt
         * 1-2 conditional
         * 1-1 trigger
         * */
        public Dialog parentGraph;
        public Port inPort;
        public List<Port> outPorts;

        public List<Node> pointsTo = new List<Node>();

#if UNITY_EDITOR
        public Vector2 position;
        public Rect rect;
        // public string title;
        [HideInInspector]
        public bool isDragged;
        [HideInInspector]
        public bool isSelected;


        public static int defaultWidth = 250;
        public static int defaultHeight = 150;


        public static int inputPortBaseY = 25;
        public static int outputPortBaseY = 25;

        [HideInInspector]
        public Rect nodeNameRect;

        [HideInInspector]
        public string corruptionReport;

        //===================================EDITOR===============================


        public virtual void Init(Dialog parentGraph, Vector2 position, string nameWithinDialog)
        {
            this.parentGraph = parentGraph;
            rect = new Rect(position.x, position.y, GetWidth(), GetHeight());
            this.nameWithinDialog = ObjectNames.NicifyVariableName(nameWithinDialog);

            SetUniquiIdAndFileName();
        }

        public void ChangePosition(Vector2 pos)
        {
            rect = new Rect(pos.x, pos.y, GetWidth(), GetHeight());
        }

        protected void GenerateNodeNameRect(int nodeWidth)
        {
            nodeNameRect = new Rect(15, 10, Mathf.Max(90, (nodeWidth - 30) * 2 / 3), 30);
        }

        public virtual void SwitchLangTo(Lang locale)
        {

        }

        protected virtual int GetWidth()
        {
            return defaultWidth;
        }

        protected virtual int GetHeight()
        {
            return defaultHeight;
        }


        public abstract void CreatePorts();



        public virtual void RunAfterAssetSaved()
        {
            CreatePorts();
        }

        public void DestoryPortsFromAssets()
        {
            if(inPort!=null)
            {
                UnityEngine.Object.DestroyImmediate(inPort, true);
            }
            if(outPorts.Count!=0)
            {
                for (int i = 0; i < outPorts.Count; i++)
                {
                    UnityEngine.Object.DestroyImmediate(outPorts[i], true);
                }
            }
        }




        //=============================================== GUI ==========================================================

        public void Drag(Vector2 delta)
        {
            rect.position += delta;
        }

        public void Draw()
        {
           // GUILayout.BeginVertical();
            //Debug.Log(backgroundColor);
            GUI.color = GetBackgroundColor();
            GUILayout.BeginArea(rect, DialogEditorStyles.styles.nodeBody);

            // GUI.color = 
            //GUIStyle highlightStyle = new GUIStyle(KrumloficationStyles.styles.nodeHighlight);
            //GUILayout.BeginVertical(new GUIStyle(highlightStyle));

            GUI.color = DialogEditorStyles.headerTextColor;
            OnHeaderGUI();
            OnBodyGUI();
            DrawPorts();


            
            GUILayout.EndArea();
             //GUILayout.EndVertical();
        }

        protected abstract Color GetBackgroundColor();


        protected abstract void DrawPorts();


        void OnHeaderGUI()
        {



            //EditorGUILayout.BeginHorizontal();
            /*if(nodeNameRect==null)
            {
                nodeNameRect = new Rect(15, 10, GetWidth() - 15, 30);
            }*/

            //nodeNameRect = new Rect(15, 10, Mathf.Max(90, (GetWidth() - 30) * 2 / 3), 30);
            GUILayout.BeginArea(nodeNameRect);

            EditorGUI.BeginChangeCheck();


            //nameWithinDialog = GUILayout.TextField(nameWithinDialog);//
            GUILayout.BeginHorizontal(EditorStyles.helpBox);
            nameWithinDialog = GUILayout.TextField(nameWithinDialog, DialogEditorStyles.styles.nodeHeaderStyle);
            GUILayout.EndHorizontal();

            if (EditorGUI.EndChangeCheck())
            {
                SetUniquiIdAndFileName();
            }

            GUILayout.EndArea();

            //EditorGUILayout.EndHorizontal();
            GUILayout.Space(30);

        }

        string CreateUniquiIdFromName()
        {
            //TODO checkuniqueness
            string newName = string.Concat(parentGraph.name, "-", nameWithinDialog);
            if(Finder.ExistsNameAndType<Node>(newName))
            {
                Siren.EditorWarn("node named "+newName+"* already exists, is it problem?");
            }
            return newName;
        }

        void SetUniquiIdAndFileName()
        {
            uniqueId = CreateUniquiIdFromName();
            name = uniqueId;
            EditorUtility.SetDirty(this);

        }

        protected abstract void OnBodyGUI();

        public bool ProcessEvents(Event e)
        {
            switch (e.type)
            {
                case EventType.MouseDown:
                    if (e.button == 0)
                    {
                        if (rect.Contains(e.mousePosition))
                        {
                            isDragged = true;
                            GUI.changed = true;
                            if(isSelected==false)
                            {
                                //Debug.Log("node selected"+this.id + Time.timeSinceLevelLoad);
                            }
                            isSelected = true;
                            //style = selectedNodeStyle;
                        }
                        else
                        {
                            GUI.changed = true;
                            if (isSelected == true)
                            {
                                SaveNodeParams();
                                //Debug.Log("node deselected"+this.id + Time.timeSinceLevelLoad);

                            }
                            isSelected = false;
                            //style = defaultNodeStyle;
                        }
                    }
                    //!! pokud by mela byt pred odmazanim vybrana e.button == 1 && isSelected && rect.Contains(e.mousePosition)
                    if (e.button == 1 && rect.Contains(e.mousePosition))
                    {
                        ProcessContextMenu();
                        e.Use();
                    }
                    break;

                case EventType.MouseUp:
                    isDragged = false;
                    break;

                case EventType.MouseDrag:
                    if (e.button == 0 && isDragged)
                    {
                        Drag(e.delta);
                        e.Use();
                        return true;
                    }
                    break;
            }

            return false;
        }

        protected void SaveNodeParams()
        {
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
        }

        private void ProcessContextMenu()
        {
            GenericMenu genericMenu = new GenericMenu();
            genericMenu.AddItem(new GUIContent("Remove node"), false, OnClickRemoveNode);
            genericMenu.ShowAsContext();
        }
        
        protected virtual void OnClickRemoveNode()
        {
            DialogEditor.OnClickRemoveNode(this);
        }

        public virtual void OnRemoveNode()
        {

        }
        public virtual bool IsCorrupted()
        {
            if (nameWithinDialog == "")
            {
                corruptionReport = "node name not set";
                return true;
            }

            return false;
        }
#endif

        //============================================== FOR GAME ==================================



        public virtual Node GetNextNode()
        {
            if (outPorts.Count != 1)
            {
                //!! hra je dialog driven, kdyz je tohle posrany tak bude asi i cela hra
                Assert.IsTrue(false, "using general GetNext(), but outPorts.Count!=1");
                Siren.Error("using general GetNext(), but outPorts.Count!=1");
            }

            return GetNodeByPortIndex(0);
        }

        protected Node GetNodeByPortIndex(int i)
        {
            if (!ArrayHelper.IsIndexInArrayRange(i, outPorts.Count)) //i > outPorts.Count)
            {
                //!! hra je dialog driven, kdyz je tohle posrany tak bude asi i cela hra
                Assert.IsTrue(false, "using GetNodeByPortIndex(), but index > outPorts.Count, bigger index than ports(options):" + i + " " + outPorts.Count);
                Siren.Error("using GetNodeByPortIndex(), but index > outPorts.Count, bigger index than ports(options):" + i + " " + outPorts.Count);
            }
            int posibilities = outPorts[i].connections.Count;
            //Debug.Log("posibilities"+ posibilities);
            if (posibilities == 0)
            {
                return null;
            }
            else if (posibilities == 1)
            {
                //!!in case of no next node
                if(outPorts[i].connections==null || outPorts[i].connections[0]==null)
                {
                    return null;
                }
                return outPorts[i].connections[0].outPort.attachedToNode;
            }
            else
            {
                int random = UnityEngine.Random.Range(0, posibilities);
                return outPorts[i].connections[random].outPort.attachedToNode;
            }
        }

    }

   // public class Lang
}
