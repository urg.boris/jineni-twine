﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Jineni.Game.BuildingBlocks;
using Jineni.DialogSystem.Utils;

namespace Jineni.DialogSystem.Graph
{
    public class Replic : VocalNode
    {


        //===================================EDITOR===============================
#if UNITY_EDITOR
        public static Color backgroundColor = DialogEditorStyles.replicColor;//new Color(0.2f, 0.6f, 0.1f);
        public override void Init(Dialog parentGraph, Vector2 position, string name)
        {
            base.Init(parentGraph, position, name);
            GenerateNodeNameRect(GetWidth());
            //locale = new Locale() { cs = "", en = "" };

        }


        public override void CreatePorts()
        {
            //!! kdyz se to da do vlastni fce tak to nefunguje, zjistit proc by bylo na dlouho, fakt nevim, nejak se neprolinkuje reference

            inPort = ScriptableObject.CreateInstance<Port>();
            inPort.Init(this, Port.IO.In);
            inPort.name = string.Concat(name, "-", INPORT);
            AssetDatabase.AddObjectToAsset(inPort, this);
            //AssetDatabase.SaveAssets();

            outPorts = new List<Port>(1);
            Port outPort = ScriptableObject.CreateInstance<Port>();
            outPort.Init(this, Port.IO.Out);
            outPort.name = string.Concat(name, "-", OUTPORT);
            AssetDatabase.AddObjectToAsset(outPort, this);
            outPorts.Add(outPort);

            AssetDatabase.SaveAssets();

        }
/*
        void AttachPort(ref Port port, Port.IO direction, string suffix)
        {
            Port newPort = ScriptableObject.CreateInstance(typeof(Port)) as Port;
            newPort.Init(this, direction);
            newPort.name = name + suffix;
            AssetDatabase.AddObjectToAsset(newPort, this as Object);
            AssetDatabase.SaveAssets();
            port = newPort;
        }*/


        protected override Color GetBackgroundColor()
        {
            return backgroundColor;
        }

        protected override void OnBodyGUI()
        {

            //replicText = EditorGUILayout.TextField(replicText, GUILayout.Height(50));

            EditorGUI.BeginChangeCheck();


            GUILayout.BeginHorizontal();
            speaker = EditorGUILayout.ObjectField(speaker, typeof(CharacterCore), false) as CharacterCore;
            GUILayout.EndHorizontal();

            if (EditorGUI.EndChangeCheck())
            {
                SaveNodeParams();
            }


            GUILayout.BeginHorizontal();
            EditorGUI.BeginChangeCheck();


            EditorStyles.textField.wordWrap = true;
            scroll = EditorGUILayout.BeginScrollView(scroll, GUILayout.Height(50));
            replicText = EditorGUILayout.TextArea(replicText, GUILayout.ExpandHeight(true));
            EditorGUILayout.EndScrollView();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            replicAudio = EditorGUILayout.ObjectField(replicAudio, typeof(AudioClip), false) as AudioClip;
            if (GUILayout.Button("►", GUILayout.Width(20)))
            {
                if (replicAudio)
                    AudioUtils.PlayClip(replicAudio);
            }
            GUILayout.EndHorizontal();

            if (EditorGUI.EndChangeCheck())
            {
                EditorUtility.SetDirty(this);
            }            

            
        }


        protected override void DrawPorts() {
            Color lastColor = GUI.color;
            GUI.color = GetBackgroundColor();

            inPort.Draw();
            outPorts[0].Draw();

            GUI.color = lastColor;
        }

        public override bool IsCorrupted()
        {
            if (base.IsCorrupted())
            {
                return true;
            }

            if (IsVocalNodeCorrupted())
            {
                return true;
            }

            if (!inPort.IsConnected())
            {
                corruptionReport = "inport is not connected, redundant?";
                return true;
            }

            return false;
        }
#endif

    }


}