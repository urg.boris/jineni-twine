﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Localization;
using Jineni.Game;
using System;

namespace Jineni.DialogSystem.Graph
{
    public class DialogOption : ScriptableObject
    {
        //!!indexWithinMultiOption is only for runtime option picking in dialog box
        public int indexWithinMultiOption;
        public Translation translation = new Translation();
        public Lang currentLang;

        public string text
        {
            get
            {
                return translation.Get(currentLang);
            }

            set
            {
                translation.Set(currentLang, value);
            }
        }


        public bool isOriginalyActive;
        public bool isCurrentlyActive;
        public bool isVisited;
/*
        public int GetStateHash
        {
            //!! default stav je 00 -  isVisited=0 isCurrentlyActive=1 ; 10 isVisited=1 isCurrentlyActive=1
            get
            {
                int activeAsBit = isCurrentlyActive ? 0 : 1;
                int visitedAsBit = (isVisited ? 1 : 0)<<1;
                return activeAsBit | visitedAsBit;
            }
        }*/

        public void ResetToDefaultState()
        {
            //Debug.Log("resetuju" + name);
            isCurrentlyActive = isOriginalyActive;
            isVisited = false;
        }

        public void Init()
        {
            isOriginalyActive = true;
            isCurrentlyActive = true;
            isVisited = false;
        }

        public void WasVisited()
        {
            isVisited = true;
        }

        public string GetText(Lang lang)
        {
            currentLang = lang;
            return text;
        }
    }
}
