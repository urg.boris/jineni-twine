﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Jineni.Game.BuildingBlocks;
using Jineni.Game.GameStateBlocks;
using Jineni.DialogSystem.Utils;
using UnityEngine.Assertions;
using Jineni.Utils;
using System;

namespace Jineni.DialogSystem.Graph
{
    public class MultiOption : VocalNode
    {


        public List<DialogOption> dialogOptions;
        public int originalStateHash;

        //===================================EDITOR===============================
#if UNITY_EDITOR
        //inherit
        public static Color backgroundColor = DialogEditorStyles.multiColor;//new Color(0.2f, 0.1f, 0.8f);
        private static int nodeWidth = 300;
        private static int nodeHeight = 200;

        [HideInInspector]
        public int i = 0;

        public override void Init(Dialog parentGraph, Vector2 position, string name)
        {
            base.Init(parentGraph, position, name);
            GenerateNodeNameRect(GetWidth());

            dialogOptions = new List<DialogOption>(2);
        }

        public override void RunAfterAssetSaved()
        {
           
            //!! kdyz se to da do vlastni fce tak to nefunguje, zjistit proc by bylo na dlouho
            inPort = ScriptableObject.CreateInstance<Port>();
            inPort.Init(this, Port.IO.In);
            inPort.name = string.Concat(name, "-", INPORT);
            AssetDatabase.AddObjectToAsset(inPort, this);

           
            outPorts = new List<Port>();
            for (int i = 0; i < 2; i++)
            {
                AddNewDialogOption();
            }

            AssetDatabase.SaveAssets();
        }

        protected override Color GetBackgroundColor()
        {
            return backgroundColor;
        }

        protected override int GetHeight()
        {
            //Debug.Log("dasdadsa");
            return nodeHeight;
        }

        protected override int GetWidth()
        {
            return nodeWidth;
        }
        
        public override void CreatePorts()
        {
            //vytvori se v RunAfterAssetSaved, je tady pouze pro inheritance problems

        }

        private void CreateOptionOutPort()
        {
            Port outPort = ScriptableObject.CreateInstance<Port>();
            outPort.Init(this, Port.IO.Out);
            outPort.name = string.Concat(name, "-", OUTPORT, i++);
            AssetDatabase.AddObjectToAsset(outPort, this);
            outPorts.Add(outPort);
        }

        /*public override void LanguageChangedTo()
        {
            base.LanguageChangedTo();
        }*/

        protected override void OnBodyGUI()
        {
            EditorGUI.BeginChangeCheck();

            GUILayout.BeginHorizontal();
            speaker = EditorGUILayout.ObjectField(speaker, typeof(CharacterCore), false) as CharacterCore;
            GUILayout.EndHorizontal();

            if (EditorGUI.EndChangeCheck())
            {
                SaveNodeParams();
            }

            EditorGUI.BeginChangeCheck();

            GUILayout.BeginHorizontal();
            EditorStyles.textField.wordWrap = true;
            scroll = EditorGUILayout.BeginScrollView(scroll, GUILayout.Height(50));
            replicText = EditorGUILayout.TextArea(replicText, GUILayout.ExpandHeight(true));
            EditorGUILayout.EndScrollView();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            replicAudio = EditorGUILayout.ObjectField(replicAudio, typeof(AudioClip), false) as AudioClip;
            if (GUILayout.Button("►", GUILayout.Width(20)))
            {
                if (replicAudio)
                    AudioUtils.PlayClip(replicAudio);
            }
            GUILayout.EndHorizontal();

            if (EditorGUI.EndChangeCheck())
            {
                EditorUtility.SetDirty(this);
            }
            GUILayout.Space(5);

            DrawOptions();            

            GUILayout.BeginHorizontal();
            GUILayout.Space(nodeWidth - 50);
            if (GUILayout.Button("+", GUILayout.Width(20)))
            {
                AddNewDialogOption();
            }

            GUILayout.EndHorizontal();
        }
        /*
        private void AddNewOption()
        {
            options.Add("");
            //nodeHeight = nodeHeight + 20;
            rect.yMax += 25;
            CreateOptionOutPort();
        }*/

        public void CreateDialogOptions(int length)
        {
            for (int j = 0; j < length; j++)
            {
                DialogOption newOption = CreateInstance<DialogOption>();
                newOption.Init();

                newOption.name = name + "-DO" + i;
                dialogOptions.Add(newOption);
                rect.yMax += 25;
                CreateOptionOutPort();
                AssetDatabase.AddObjectToAsset(newOption, this);
 
            }
            AssetDatabase.SaveAssets();
            RecalculateDialogOptionIndexes();
        } 

        private void AddNewDialogOption()
        {
            DialogOption newOption = CreateInstance<DialogOption>();
            newOption.Init();

            newOption.name = name + "-DO" + i;
            dialogOptions.Add(newOption);
            rect.yMax += 25;
            CreateOptionOutPort();
            AssetDatabase.AddObjectToAsset(newOption, this);
            AssetDatabase.SaveAssets();

            RecalculateDialogOptionIndexes();
        }

        private void RemoveDialogOptionAt(int i)
        {
            //!!nejdrif destroy scriptable, pak odebra z listu
            UnityEngine.Object.DestroyImmediate(dialogOptions[i], true);
            dialogOptions.RemoveAt(i);
            //!!nejdrif destroy scriptable, pak odebra z listu
            UnityEngine.Object.DestroyImmediate(outPorts[i], true);
            RemoveConnectionsOnOutPort(i);
            
            AssetDatabase.SaveAssets();
           // RecalculateOrigStateHash();
            RecalculateDialogOptionIndexes();
        }

        void RecalculateDialogOptionIndexes()
        {
            for (int i = 0; i < dialogOptions.Count; i++)
            {
                dialogOptions[i].indexWithinMultiOption = i;
            }
        }

        private void RemoveConnectionsOnOutPort(int i)
        {
            for (int j = 0; j < outPorts[i].connections.Count; j++)
            {
                parentGraph.RemoveConnection(outPorts[i].connections[j]);
            }
            outPorts.RemoveAt(i);
        }

        private void DrawOptions()
        {
            if (dialogOptions.Count > 0)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Space(5);
                EditorGUILayout.BeginVertical();
                for (var i = 0; i < dialogOptions.Count; i++)
                {
                    GUILayout.BeginVertical();
                    GUILayout.BeginHorizontal();

                    EditorGUI.BeginChangeCheck();
                    dialogOptions[i].isOriginalyActive = EditorGUILayout.Toggle(dialogOptions[i].isOriginalyActive, GUILayout.MaxWidth(20));
                    if(EditorGUI.EndChangeCheck())
                    {
                        dialogOptions[i].isCurrentlyActive = dialogOptions[i].isOriginalyActive;
                       // RecalculateOrigStateHash();
                    }
                    dialogOptions[i].text = EditorGUILayout.TextArea(dialogOptions[i].text);
    
                    if (GUILayout.Button("‒", GUILayout.Width(20)))
                    {
                        if (i == 0 && dialogOptions.Count == 1)
                        {
                            Assert.IsTrue(false, "cant remove the only outport");
                        }
                        RemoveDialogOptionAt(i);
                        i--;
                        //!! dont draw the rest!
                        continue;
                    }

                    Color lastColor = GUI.color;
                    GUI.color = GetBackgroundColor();

                    outPorts[i].DrawWithOffset(GUILayoutUtility.GetLastRect().center.y);

                    GUI.color = lastColor;

                    GUILayout.EndHorizontal();
                    GUILayout.EndVertical();
                    GUILayout.Space(4);
                }
                GUILayout.EndVertical();
                GUILayout.EndHorizontal();
            }
        }


        

        protected override void DrawPorts()
        {
            Color lastColor = GUI.color;
            GUI.color = GetBackgroundColor();

            inPort.Draw();
            //!!outports drawn in DrawOptions()

            GUI.color = lastColor;
        }

        public override void SwitchLangTo(Game.Lang lang)
        {
            currentLang = lang;
            for (int i = 0; i < dialogOptions.Count; i++)
            {
                dialogOptions[i].currentLang = lang;

            }
        }

        public override bool IsCorrupted()
        {

            if (IsVocalNodeCorrupted())
            {
                return true;
            }

            for (int i = 0; i < dialogOptions.Count; i++)
            {
                if (dialogOptions[i].GetText(Game.Lang.cs) == "")
                {
                    corruptionReport = "option " + i + " text is empty";
                    return true;
                }

                if (!outPorts[i].IsConnected())
                {
                    corruptionReport = "port on option " + i + " is not connected";
                    return true;
                }
            }
            return false;
        }

#endif

        //============================================== COMMON ==================================
        public string[] GetDialogOptionTexts()
        {
            string[] optionTexts = new string[dialogOptions.Count];
            for (int i = 0; i < dialogOptions.Count; i++)
            {
                optionTexts[i] = dialogOptions[i].text;
            }
            return optionTexts;
        }

        //============================================== FOR GAME ==================================

        public void ResetToDefaultState()
        {
            for (int i = 0; i < dialogOptions.Count; i++)
            {
                dialogOptions[i].ResetToDefaultState();
            }
        }


        public Node GetNextNodeFromOption(int optionIndex)
        {
            if (!ArrayHelper.IsIndexInArrayRange(optionIndex, outPorts.Count))// optionIndex > outPorts.Count)
            {
                //!! hra je dialog driven, kdyz je tohle posrany tak bude asi i cela hra
                Assert.IsTrue(false, "using GetNextNodeFromOption(), but optionIndex > outPorts.Count, bigger index than ports(options)");
                Siren.Error("using GetNextNodeFromOption(), but optionIndex > outPorts.Count, bigger index than ports(options)");
            }


            return GetNodeByPortIndex(optionIndex);
        }



        public void SetOptionState(int index, bool active)
        {
            if(!ArrayHelper.IsIndexInArrayRange(index, dialogOptions.Count))
            {
                //!! hra je dialog driven, kdyz je tohle posrany tak bude asi i cela hra
                Assert.IsTrue(false, "SetOptionState index cant be " + index + ", limits are [0, " + (dialogOptions.Count - 1) + "]");
                Siren.Error("SetOptionState index cant be "+ index + ", limits are [0, "+ (dialogOptions.Count-1)+ "]");
                return;
            }
            dialogOptions[index].isCurrentlyActive = active;
        }

        public List<DialogOption> GetActiveDialogOptions()
        {
            List<DialogOption> activeDialogOptions = new List<DialogOption>();
            for (int i = 0; i < dialogOptions.Count; i++)
            {
                if (dialogOptions[i].isCurrentlyActive==true)
                {
                    activeDialogOptions.Add(dialogOptions[i]);
                }
            }
            return activeDialogOptions;
        }

        public void SetDialogOptionStatesTo(List<DialogOptionState> dialogOptionStates)
        {
            Debug.Log(uniqueId);
            Assert.IsTrue(dialogOptionStates.Count == dialogOptions.Count);
            for (int i = 0; i < dialogOptionStates.Count; i++)
            {
                //Debug.Log(uniqueId + " " + i + " " + dialogOptionStates[i].isActive + " " + dialogOptionStates[i].isVisited);
                dialogOptions[i].isCurrentlyActive = dialogOptionStates[i].isActive;
                dialogOptions[i].isVisited = dialogOptionStates[i].isVisited;
                //Debug.Log(uniqueId + " " + i + "changed " + dialogOptions[i].isCurrentlyActive + " " + dialogOptions[i].isVisited);
            }
        }


    }
}
