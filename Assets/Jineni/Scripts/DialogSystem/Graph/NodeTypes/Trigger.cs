﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Jineni.Game.BuildingBlocks;
using Jineni.DialogSystem.FlowControl.Triggers;
using Jineni.Utils;
using UnityEngine.Assertions;

namespace Jineni.DialogSystem.Graph
{
    public class Trigger : Node
    {


        public List<ConcreteTrigger> triggers;



        //===================================EDITOR===============================
#if UNITY_EDITOR
        [SerializeField]
        //private List<string[]> triggerMultiOptionOptionTexts = new List<string[]>(10);// string[1] { "" };
        private int triggerMultiOptionIndex;
        //private string[] optionNames = new string[0];
        //[SerializeField]
        //Dictionary<int, string[]> triggerOption = new Dictionary<int, string[]>();
        //inherit
        public static Color backgroundColor = DialogEditorStyles.triggerColor;
        private static int nodeWidth = 300;
        private static int nodeHeight = 200;
        public override void Init(Dialog parentGraph, Vector2 position, string name)
        {
            base.Init(parentGraph, position, name);
            GenerateNodeNameRect(GetWidth());
            triggers = new List<ConcreteTrigger>();

        }

        protected override Color GetBackgroundColor()
        {
            return backgroundColor;
        }

        protected override int GetHeight()
        {
            return nodeHeight;
        }

        protected override int GetWidth()
        {
            return nodeWidth;
        }

        public override void CreatePorts()
        {
            //!! kdyz se to da do vlastni fce tak to nefunguje, zjistit proc by bylo na dlouho
            inPort = ScriptableObject.CreateInstance<Port>();
            inPort.Init(this, Port.IO.In);
            inPort.name = string.Concat(name, "-", INPORT);
            AssetDatabase.AddObjectToAsset(inPort, this);

            outPorts = new List<Port>(1);

            //!! kdyz se to da do vlastni fce tak to nefunguje, zjistit proc by bylo na dlouho
            Port outPort = ScriptableObject.CreateInstance<Port>();
            outPort.Init(this, Port.IO.Out);
            outPort.name = string.Concat(name, "-", OUTPORT);
            AssetDatabase.AddObjectToAsset(outPort, this);
            outPorts.Add(outPort);

            AssetDatabase.SaveAssets();
        }

        public override void RunAfterAssetSaved()
        {
            CreatePorts();
            AttachNewTrigger();
        }

        private void AttachNewTrigger()
        {
            ConcreteTrigger trigger = ScriptableObject.CreateInstance<ConcreteTrigger>();
            trigger.name = name + "-T";
            triggers.Add(trigger);

            AssetDatabase.AddObjectToAsset(trigger, this);
            AssetDatabase.SaveAssets();
        }

        public override void OnRemoveNode()
        {
            if (triggers.Count != 0)
            {
                for (int i = 0; i < triggers.Count; i++)
                {
                    Object.DestroyImmediate(triggers[i], true);
                }
            }
        }


        protected override void OnBodyGUI()
        {
            EditorGUI.BeginChangeCheck();


            DrawTriggers();

            if (EditorGUI.EndChangeCheck())
            {
                SaveNodeParams();
            }
            

            GUILayout.BeginHorizontal();
            GUILayout.Space(nodeWidth - 50);
            if (GUILayout.Button("+", GUILayout.Width(20)))
            {
                AddNewTrigger();
            }
            GUILayout.EndHorizontal();
            
        }

        private void AddNewTrigger()
        {
           // triggers.Add("");
            //nodeHeight = nodeHeight + 20;
            rect.yMax += 75;
            AttachNewTrigger();
            //            CreateOptionOutPort();
        }


        private void RemoveTrigger(int i)
        {
            triggers.RemoveAt(i);
        }

        private void RemoveTriggerAt(int i)
        {
            //!!nejdrif destroy scriptable, pak odebra z listu
            Object.DestroyImmediate(triggers[i], true);
            triggers.RemoveAt(i);

            AssetDatabase.SaveAssets();
       
        }

        private void DrawTriggers()
        {
            if (triggers.Count > 0)
            {
                //!!pod sebou triggery
                GUILayout.BeginVertical();
                //
                
                for (var i = 0; i < triggers.Count; i++)
                {
                    //!! typ plus tlacitko -
                    GUILayout.BeginHorizontal();
                    triggers[i].type = EditorGUILayout.ObjectField(triggers[i].type, typeof(TriggerType), false) as TriggerType;


                    if (GUILayout.Button("‒", GUILayout.Width(20)))
                    {
                        if (i == 0 && triggers.Count == 1)
                        {
                            Assert.IsTrue(false, "cant remove the only trigger");
                        }
                        RemoveTriggerAt(i);

                        i--;
                        //!! dont draw the rest!
                        continue;
                    }

                    //!! typ plus tlacitko -
                    GUILayout.EndHorizontal();
                    
                    //!! odsazeni promenych triggeru doleva
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(20);
                    if (triggers[i].type != null)
                    {
                        if (triggers[i].type is InventoryTrigger)
                        {
                            triggers[i].variable = EditorGUILayout.ObjectField(triggers[i].variable, typeof(ItemCore), false) as ItemCore;
                        }
                        else if (triggers[i].type is LocationEntryPointChange)
                        {
                            //EditorGUI.BeginChangeCheck();

                            triggers[i].variable = EditorGUILayout.ObjectField(triggers[i].variable, typeof(LocationCore), false) as LocationCore;

                            //!!aby se zalomil novy radek
                            GUILayout.EndHorizontal();
                            GUILayout.BeginHorizontal();
                            //!!aby se zalomil novy radek
                            GUILayout.Space(20);
                            triggers[i].entryPoint = EditorGUILayout.ObjectField(triggers[i].entryPoint, typeof(EntryPoint), false) as EntryPoint;
                            /*if (EditorGUI.EndChangeCheck())
                            {
                                triggerMultiOptionOptionTexts = (triggers[i].type as OptionChangeTrigger).GetOptionTextsFrom(triggers[i].variable as MultiOption);
                            }*/

                        }
                        /*
                        else if (triggers[i].type is LocationChangeCharacterTransform)
                        { //!!MUST BE BEFORE LocationTrigger
                            triggers[i].variable = EditorGUILayout.ObjectField(triggers[i].variable, typeof(CharacterCore), false) as CharacterCore;

                            //!!aby se zalomil novy radek
                            GUILayout.EndHorizontal();
                            GUILayout.BeginHorizontal();
                            //!!aby se zalomil novy radek
                            GUILayout.Space(20);
                            triggers[i].stringParam = EditorGUILayout.TextField(triggers[i].stringParam);
                        }*/
                        else if (triggers[i].type is LocationTrigger)
                        {
                            triggers[i].variable = EditorGUILayout.ObjectField(triggers[i].variable, typeof(LocationCore), false) as LocationCore;
                        }
                        else if (triggers[i].type is OptionChangeTrigger)
                        {
                            EditorGUI.BeginChangeCheck();

                            triggers[i].variable = EditorGUILayout.ObjectField(triggers[i].variable, typeof(MultiOption), false) as MultiOption;

                            //!!aby se zalomil novy radek
                            GUILayout.EndHorizontal();
                            GUILayout.BeginHorizontal();
                            //!!aby se zalomil novy radek
                            GUILayout.Space(20);
                            if (EditorGUI.EndChangeCheck())
                            {
                                if (triggers[i].variable != null)
                                {

                                    triggers[i].stringOptions = (triggers[i].variable as MultiOption).GetDialogOptionTexts();
                                    //optionNames = (triggers[i].variable as LocationCore).GetPossibleCharacterActivityNames(triggers[i].character as CharacterCore);
                                    //triggerOption.Add(i, (triggers[i].variable as LocationCore).GetPossibleCharacterActivityNames(triggers[i].character as CharacterCore));
                                }
                                //triggerMultiOptionOptionTexts = (triggers[i].type as OptionChangeTrigger).GetOptionTextsFrom(triggers[i].variable as MultiOption);
                            }
                            triggers[i].intParam = EditorGUILayout.Popup(triggers[i].intParam, triggers[i].stringOptions != null ? triggers[i].stringOptions : new string[0]);
                            //triggers[i].intParam = EditorGUILayout.Popup(triggers[i].intParam, triggerMultiOptionOptionTexts);
                        }
                        else if (triggers[i].type is CharacterChangeDialogEntryPoint)
                        {
                            //EditorGUI.BeginChangeCheck();

                            triggers[i].variable = EditorGUILayout.ObjectField(triggers[i].variable, typeof(CharacterCore), false) as CharacterCore;

                            //!!aby se zalomil novy radek
                            GUILayout.EndHorizontal();
                            GUILayout.BeginHorizontal();
                            //!!aby se zalomil novy radek
                            GUILayout.Space(20);
                            triggers[i].entryPoint = EditorGUILayout.ObjectField(triggers[i].entryPoint, typeof(EntryPoint), false) as EntryPoint;
                            /*if (EditorGUI.EndChangeCheck())
                            {
                                triggerMultiOptionOptionTexts = (triggers[i].type as OptionChangeTrigger).GetOptionTextsFrom(triggers[i].variable as MultiOption);
                            }*/

                        }
                        else if (triggers[i].type is CharacterChangeExamineEntryPoint)
                        {
                            //EditorGUI.BeginChangeCheck();

                            triggers[i].variable = EditorGUILayout.ObjectField(triggers[i].variable, typeof(CharacterCore), false) as CharacterCore;

                            //!!aby se zalomil novy radek
                            GUILayout.EndHorizontal();
                            GUILayout.BeginHorizontal();
                            //!!aby se zalomil novy radek
                            GUILayout.Space(20);
                            triggers[i].entryPoint = EditorGUILayout.ObjectField(triggers[i].entryPoint, typeof(EntryPoint), false) as EntryPoint;
                            /*if (EditorGUI.EndChangeCheck())
                            {
                                triggerMultiOptionOptionTexts = (triggers[i].type as OptionChangeTrigger).GetOptionTextsFrom(triggers[i].variable as MultiOption);
                            }*/

                        }
                        else if (triggers[i].type is ItemChangeExamineEntryPoint)
                        {
                            //EditorGUI.BeginChangeCheck();

                            triggers[i].variable = EditorGUILayout.ObjectField(triggers[i].variable, typeof(ItemCore), false) as ItemCore;

                            //!!aby se zalomil novy radek
                            GUILayout.EndHorizontal();
                            GUILayout.BeginHorizontal();
                            //!!aby se zalomil novy radek
                            GUILayout.Space(20);
                            triggers[i].entryPoint = EditorGUILayout.ObjectField(triggers[i].entryPoint, typeof(EntryPoint), false) as EntryPoint;
                            /*if (EditorGUI.EndChangeCheck())
                            {
                                triggerMultiOptionOptionTexts = (triggers[i].type as OptionChangeTrigger).GetOptionTextsFrom(triggers[i].variable as MultiOption);
                            }*/

                        }
                        else if(triggers[i].type is CharacterChangeName)
                        {
                            triggers[i].variable = EditorGUILayout.ObjectField(triggers[i].variable, typeof(CharacterCore), false) as CharacterCore;

                            //!!aby se zalomil novy radek
                            GUILayout.EndHorizontal();
                            GUILayout.BeginHorizontal();
                            //!!aby se zalomil novy radek
                            GUILayout.Space(20);

                            EditorGUILayout.LabelField("name index", GUILayout.Width(50));
                            //TODO Popup selector
                            triggers[i].intParam = EditorGUILayout.IntField(triggers[i].intParam);

                        }
                        else if (triggers[i].type is LocationChangeCharacterActivityTrigger)
                        {
                            EditorGUI.BeginChangeCheck();
                            triggers[i].variable = EditorGUILayout.ObjectField(triggers[i].variable, typeof(LocationCore), false) as LocationCore;

                            //!!aby se zalomil novy radek
                            GUILayout.EndHorizontal();
                            GUILayout.BeginHorizontal();
                            //!!aby se zalomil novy radek
                            GUILayout.Space(20);
                            triggers[i].character = EditorGUILayout.ObjectField(triggers[i].character, typeof(CharacterCore), false) as CharacterCore;
                            //TODO loading activities?
                            if (EditorGUI.EndChangeCheck())
                            {
                                if(triggers[i].variable!=null && triggers[i].character!=null)
                                {

                                    triggers[i].stringOptions = (triggers[i].variable as LocationCore).GetPossibleCharacterActivityNames(triggers[i].character as CharacterCore);
                                    //optionNames = (triggers[i].variable as LocationCore).GetPossibleCharacterActivityNames(triggers[i].character as CharacterCore);
                                    //triggerOption.Add(i, (triggers[i].variable as LocationCore).GetPossibleCharacterActivityNames(triggers[i].character as CharacterCore));
                                }
                                
                            }

                            //!!aby se zalomil novy radek
                            GUILayout.EndHorizontal();
                            GUILayout.BeginHorizontal();
                            //!!aby se zalomil novy radek
                            GUILayout.Space(20);


                            //triggers[i].intParam = EditorGUILayout.IntField("activity", triggers[i].intParam);
                            EditorGUILayout.LabelField("activity", GUILayout.Width(50));
                            triggers[i].intParam = EditorGUILayout.Popup(triggers[i].intParam, triggers[i].stringOptions!=null ? triggers[i].stringOptions : new string[0]);
                        }
                        else if (triggers[i].type is LocationChangeCharacterTransformTrigger)
                        {
                            EditorGUI.BeginChangeCheck();
                            triggers[i].variable = EditorGUILayout.ObjectField(triggers[i].variable, typeof(LocationCore), false) as LocationCore;

                            //!!aby se zalomil novy radek
                            GUILayout.EndHorizontal();
                            GUILayout.BeginHorizontal();
                            //!!aby se zalomil novy radek
                            GUILayout.Space(20);
                            triggers[i].character = EditorGUILayout.ObjectField(triggers[i].character, typeof(CharacterCore), false) as CharacterCore;
                            //TODO loading activities?
                            if (EditorGUI.EndChangeCheck())
                            {
                                if (triggers[i].variable != null && triggers[i].character != null)
                                {
                                    //optionNames = (triggers[i].variable as LocationCore).GetPossibleCharacterTransformNames(triggers[i].character as CharacterCore);
                                    triggers[i].stringOptions = (triggers[i].variable as LocationCore).GetPossibleCharacterTransformNames(triggers[i].character as CharacterCore);
                                }

                            }

                            //!!aby se zalomil novy radek
                            GUILayout.EndHorizontal();
                            GUILayout.BeginHorizontal();
                            //!!aby se zalomil novy radek
                            GUILayout.Space(20);


                            //triggers[i].intParam = EditorGUILayout.IntField("activity", triggers[i].intParam);
                            EditorGUILayout.LabelField("transform", GUILayout.Width(50));
                            triggers[i].intParam = EditorGUILayout.Popup(triggers[i].intParam, triggers[i].stringOptions != null ? triggers[i].stringOptions : new string[0]);


                        }
                        else if (triggers[i].type is CharacterChangeYesNoPropertyTrigger)
                        {
                            //EditorGUI.BeginChangeCheck();
                            triggers[i].variable = EditorGUILayout.ObjectField(triggers[i].variable, typeof(CharacterCore), false) as CharacterCore;
                           /* if (EditorGUI.EndChangeCheck())
                            {
                                characterActivities = (triggers[i].variable as CharacterCore).activities;
                            }*/

                            //!!aby se zalomil novy radek
                            GUILayout.EndHorizontal();
                            GUILayout.BeginHorizontal();
                            //!!aby se zalomil novy radek
                            GUILayout.Space(20);

                            //EditorGUILayout.LabelField("visibility", GUILayout.Width(50));
                            triggers[i].boolParam  = EditorGUILayout.Toggle("is it/is it not?", triggers[i].boolParam);


                        }
                        else if (triggers[i].type is OddnessIncrement)
                        {
                            EditorGUILayout.LabelField("intensity(% increment)");
                           
                            //!!aby se zalomil novy radek
                            GUILayout.EndHorizontal();
                            GUILayout.BeginHorizontal();
                            //!!aby se zalomil novy radek
                            GUILayout.Space(20);
                            triggers[i].intParam = EditorGUILayout.IntSlider(triggers[i].intParam, 0, 20);

                        }
                        else if (triggers[i].type is OddnessDecrement)
                        {
                            EditorGUILayout.LabelField("intensity(% decrement)");

                            //!!aby se zalomil novy radek
                            GUILayout.EndHorizontal();
                            GUILayout.BeginHorizontal();
                            //!!aby se zalomil novy radek
                            GUILayout.Space(20);
                            triggers[i].intParam = EditorGUILayout.IntSlider(triggers[i].intParam, 0, 20);
                        } else if (triggers[i].type is DelayTrigger)
                        {
                            EditorGUILayout.LabelField("delay in seconds");

                            //!!aby se zalomil novy radek
                            GUILayout.EndHorizontal();
                            GUILayout.BeginHorizontal();
                            //!!aby se zalomil novy radek
                            GUILayout.Space(20);
                            triggers[i].floatParam = EditorGUILayout.FloatField(triggers[i].floatParam);
                        }
                        else if (triggers[i].type is SomeTimePassed)
                        {
                            //!!aby se zalomil novy radek
                            GUILayout.EndHorizontal();
                            GUILayout.BeginHorizontal();
                            //!!aby se zalomil novy radek
                            GUILayout.Space(20);

                            EditorGUILayout.LabelField("time in secs", GUILayout.Width(100));
                            triggers[i].floatParam = EditorGUILayout.FloatField(triggers[i].floatParam);

                            //!!aby se zalomil novy radek
                            GUILayout.EndHorizontal();
                            GUILayout.BeginHorizontal();
                            //!!aby se zalomil novy radek
                            GUILayout.Space(20);

                            EditorGUILayout.LabelField("text", GUILayout.Width(100));
                            triggers[i].stringParam = EditorGUILayout.TextField(triggers[i].stringParam);
                        }
                        else if (triggers[i].type is CharacterTrigger)
                        {
                            triggers[i].variable = EditorGUILayout.ObjectField(triggers[i].variable, typeof(CharacterCore), false) as CharacterCore;
                        }
                        

                        else
                        {
                            Assert.IsFalse(true, (triggers[i].type.ToString() + "is not recognized trigger type"));
                        }

                    }
                    //!! odsazeni promenych triggeru doleva
                    GUILayout.EndHorizontal();
                    GUILayout.Space(10);
                    //triggers[i].variable = EditorGUILayout.ObjectField(triggers[i].type, typeof(TriggerType), false) as TriggerType;
                }
                

                //!!pod sebou triggery
                GUILayout.EndVertical();
            }
        }

        protected override void DrawPorts()
        {
            Color lastColor = GUI.color;
            GUI.color = GetBackgroundColor();

            inPort.Draw();
            outPorts[0].Draw();

            GUI.color = lastColor;
        }




        public override bool IsCorrupted()
        {
            if (base.IsCorrupted())
            {
                return true;
            }

            if (!inPort.IsConnected())
            {
                corruptionReport = "inport is not connected, redundant?";
                return true;
            }

            bool containsVariable;
            for (int i = 0; i < triggers.Count; i++)
            {
                /*if(triggers[i]==null)
                {
                    corruptionReport = "trigger[" + i + "] not set";
                    return true;
                }*/
                if (triggers[i].type == null)
                {
                    corruptionReport = "trigger["+i+"] type not set";
                    return true;
                }

                containsVariable = !(triggers[i].type is OddnessTrigger || triggers[i].type is FloatTrigger);

                if(containsVariable)
                {
                    if (triggers[i].variable == null)
                    {
                        corruptionReport = "trigger[" + i + "] variable not set";
                        return true;
                    }
                }



                //!!bude takle prasacky, nebot v ConcreteTrigger je to taky prasacky, a uz s tim moc nic nechci delat

                if (triggers[i].type is CharacterChangeDialogEntryPoint)
                {
                    if (triggers[i].entryPoint == null)
                    {
                        corruptionReport = "trigger[" + i + "] entryPoint cant be null in case of " + triggers[i].type.GetType().Name;
                        return true;
                    }
                }
                else if (triggers[i].type is CharacterChangeName)
                {
                    int limit = (triggers[i].variable as CharacterCore).names.Length;// GetOptionTextsFrom(triggers[i].variable as MultiOption).Length;

                    if (!ArrayHelper.IsIndexInArrayRange(triggers[i].intParam, limit))
                    {
                        corruptionReport = "trigger[" + i + "] intParam(" + triggers[i].intParam + ") for "+ triggers[i].type.GetType().Name + " is not in index limit - [0," + limit + "]";
                        return true;
                    }
                }
                else if (triggers[i].type is OptionChangeTrigger)
                {
                    int limit = (triggers[i].type as OptionChangeTrigger).GetOptionTextsFrom(triggers[i].variable as MultiOption).Length;
                    if (!ArrayHelper.IsIndexInArrayRange(triggers[i].intParam, limit))
                    {
                        corruptionReport = "trigger[" + i + "] intParam(" + triggers[i].intParam + ") for " + triggers[i].type.GetType().Name + " is not in index limit - [0," + limit + "]";
                        return true;
                    }
                }

                else if (triggers[i].type is InventoryTrigger)
                {
                    if ((triggers[i].type as InventoryTrigger).inventory==null)
                    {
                        corruptionReport = "inventory for " + triggers[i].type.GetType().Name + " cant be null";
                        return true;
                    }
                }

                else if (triggers[i].type is LocationChangeCharacterTransformTrigger)
                {
                    //LocationChangeCharacterActivityTrigger locationChangeCharacterActivityTrigger = ();
                    int limit = (triggers[i].variable as LocationCore).GetTransformsFor(triggers[i].character).GetCount();
                    if (!ArrayHelper.IsIndexInArrayRange(triggers[i].intParam, limit))//TODO if (triggers[i].intParam < 0 || triggers[i].intParam >= characterActivities.Length)
                    {
                        corruptionReport = "trigger[" + i + "] intParam(" + triggers[i].intParam + ") for " + triggers[i].type.GetType().Name + " is not in index limit - [0,..]";
                        //corruptionReport = "trigger[" + i + "] intParam(" + triggers[i].intParam + ") for CharacterChangeActivityTrigger is not in index limit - [0," + characterActivities.Length + "]";
                        return true;
                    }
                }
                else if (triggers[i].type is LocationChangeCharacterActivityTrigger)
                {
                    //LocationChangeCharacterActivityTrigger locationChangeCharacterActivityTrigger = ();
                    int limit = (triggers[i].variable as LocationCore).GetActivitiesFor(triggers[i].character).GetCount();
                    if (!ArrayHelper.IsIndexInArrayRange(triggers[i].intParam, limit))//TODO if (triggers[i].intParam < 0 || triggers[i].intParam >= characterActivities.Length)
                    {
                        corruptionReport = "trigger[" + i + "] intParam(" + triggers[i].intParam + ") for " + triggers[i].type.GetType().Name + " is not in index limit - [0,..]";
                        //corruptionReport = "trigger[" + i + "] intParam(" + triggers[i].intParam + ") for CharacterChangeActivityTrigger is not in index limit - [0," + characterActivities.Length + "]";
                        return true;
                    }
                }

            }

            return false;
        }
#endif

        public void Invoke()
        {
            for (int i = 0; i < triggers.Count; i++)
            {
                //Debug.Log(i + "trig" + triggers[i].name);
                triggers[i].Invoke();
            }
        }
    }
}
