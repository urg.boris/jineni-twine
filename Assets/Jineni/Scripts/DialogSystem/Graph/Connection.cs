﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Serialization;
using UnityEngine;
using UnityEditor;

namespace Jineni.DialogSystem.Graph
{
    //[System.Serializable]
    public class Connection : ScriptableObject
    {

        public Port inPort;
        public Port outPort;


        /*
                public Connection(Port inPort, Port outPort)
                {
                    this.inPort = inPort;
                    this.outPort = outPort;
                    //InPort.AddConnection(this);
                    //OutPort.AddConnection(this);
                }*/

            //===================================EDITOR===============================
#if UNITY_EDITOR
        public void Init(Port inPort, Port outPort)
        {
            
            this.inPort = inPort;
            this.outPort = outPort;
            inPort.connections.Add(this);
            outPort.connections.Add(this);
            //Debug.Log("this"+this);
        }

        public void Draw()
        {

            //Debug.Log("draw con " + InPort.AbsCenter.ToString() + " " + OutPort.AbsCenter.ToString());

            //!! difer random wayes
            if(inPort.connections.Count>1)
            {
                Handles.DrawBezier(
                    inPort.centerInAbsolute,
                    outPort.centerInAbsolute,
                    inPort.centerInAbsolute - Vector2.left * 50f,
                    outPort.centerInAbsolute + Vector2.left * 50f,
                    DialogEditorStyles.randomConnection,
                    null,
                    DialogEditorStyles.connectionLineSize
                );
            } else
            {
                Handles.DrawBezier(
                    inPort.centerInAbsolute,
                    outPort.centerInAbsolute,
                    inPort.centerInAbsolute - Vector2.left * 50f,
                    outPort.centerInAbsolute + Vector2.left * 50f,
                    DialogEditorStyles.connectionColor,
                    null,
                    DialogEditorStyles.connectionLineSize
                );
            }
            

            //if(outPort.attachedToNode.pointsTo.Count>1)
            //!!pro random
            /*Vector3[] points = Handles.MakeBezierPoints(
                inPort.center,
                outPort.center,
                inPort.center + Vector2.left * 50f,
                outPort.center - Vector2.left * 50f,
                5);

            Handles.DrawAAPolyLine(points);*/
            Vector2 center = (inPort.centerInAbsolute + outPort.centerInAbsolute) *0.5f;

            //!!use corect color
            Color lastColor = GUI.color;
            GUI.color = DialogEditorStyles.colors.red;

            if (GUI.Button(new Rect(center - Port.halfPort, Port.portSize), "-"))
            {
                DialogEditor.OnClickRemoveConnection(this);
            }


            //!!use corect color
            GUI.color = lastColor;
            /*
            if (Handles.Button((inPort.centerInAbsolute + outPort.centerInAbsolute) * 0.5f, Quaternion.identity, 4, 4, Handles.RectangleHandleCap))
            {
                 KrumloficationEditor.Instance.OnClickRemoveConnection(this);
            }*/
        }
#endif
    }
}
