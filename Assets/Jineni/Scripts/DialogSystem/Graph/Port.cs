﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Jineni.DialogSystem.Graph
{
    [System.Serializable]
    public class Port:ScriptableObject
    {
        public enum IO { In, Out }
        public IO Direction;
        public bool IsInput { get { return Direction == IO.In; } }
        public bool IsOutput { get { return Direction == IO.Out; } }
        public Node attachedToNode;
        public Vector2 centerInAbsolute;
        public List<Connection> connections = new List<Connection>();

        public static int width = 12;
        public static int halfWidth = 6;
        public static Vector2 halfPort = new Vector2(halfWidth, halfWidth);
        public static Vector2 portSize = new Vector2(width, width);

        public Rect buttonRectInRelative;

        //===================================EDITOR===============================
#if UNITY_EDITOR
        public void Init(Node node, IO direction)
        {
            attachedToNode = node;
            Direction = direction;

            if (IsInput)
            {
                buttonRectInRelative = new Rect(0, Node.inputPortBaseY, width, width);
            }
            else
            {
                //!!parentRect.width-dotWidth nedava smysl ale to jedno
                buttonRectInRelative = new Rect(node.rect.width - width, Node.outputPortBaseY, width, width);
            }
        }

        public void Draw()
        {
            //!!MUST BE DRAING ACTUAL DATA FROM PARENT TO CONNECT TO ACTUAL(MOVED) LOCATIONS
            if (IsInput)
            {
                centerInAbsolute = new Vector2(attachedToNode.rect.x + halfWidth, attachedToNode.rect.y + Node.inputPortBaseY + halfWidth);
            }
            else
            {
                centerInAbsolute = new Vector2(attachedToNode.rect.x + attachedToNode.rect.width - halfWidth, attachedToNode.rect.y + Node.outputPortBaseY + halfWidth);
            }
            //Debug.Log(center);
            /**
            Vector2 inPortCenter = new Vector2(position.x + halfDotWidth, position.y + inputPortBaseY + halfDotWidth);
            Vector2 outPortCenter = new Vector2(position.x + defaultWidth - halfDotWidth, position.y + inputPortBaseY + halfDotWidth);*/
            // GUI.DrawTexture(buttonRect, KrumloficationStyles.dot);
            //nevim proc nejde, ale to je jedno
            if (GUI.Button(buttonRectInRelative, ""))
            {
                //Debug.Log("click on port");
                //!! jinak se fce blbe posila jako parametr konstruktoru a pak mi to nefunguje
                if(IsInput)
                {
                    DialogEditor.OnClickNodeInPort(this);
                } else
                {
                    DialogEditor.OnClickNodeOutPort(this);
                }
            }
        }

        public void DrawAsMultiOutPort(int i)
        {
            //!!MUST BE DRAING ACTUAL DATA FROM PARENT TO CONNECT TO ACTUAL(MOVED) LOCATIONS
           
            
            centerInAbsolute = new Vector2(attachedToNode.rect.x + attachedToNode.rect.width - halfWidth, attachedToNode.rect.y + Node.outputPortBaseY+width*i + halfWidth);
            buttonRectInRelative = new Rect(attachedToNode.rect.width - width, Node.outputPortBaseY + width * i, width, width);
            // center = new Vector2(attachedToNode.rect.x , attachedToNode.rect.y);



           // Debug.Log("new"+buttonRect);
            //GUI.changed = true;
            // Debug.Log(i + "vdasfcasdd a das" + center);
         
            //nevim proc nejde, ale to je jedno
            if (GUI.Button(buttonRectInRelative, ""))
            {
                //Debug.Log("click on port");
                //!! jinak se fce blbe posila jako parametr konstruktoru a pak mi to nefunguje
                DialogEditor.OnClickNodeOutPort(this);
            }
        }

        public void DrawWithOffset(float topOffset)
        {

            //!!MUST BE DRAING ACTUAL DATA FROM PARENT TO CONNECT TO ACTUAL(MOVED) LOCATIONS


            centerInAbsolute = new Vector2(attachedToNode.rect.x + attachedToNode.rect.width - halfWidth, attachedToNode.rect.y + topOffset);
            buttonRectInRelative = new Rect(attachedToNode.rect.width - width, topOffset - halfWidth, width, width);

            //nevim proc nejde, ale to je jedno
            if (GUI.Button(buttonRectInRelative, ""))
            {
                //Debug.Log("click on port");
                //!! jinak se fce blbe posila jako parametr konstruktoru a pak mi to nefunguje
                DialogEditor.OnClickNodeOutPort(this);
            }

        }

#endif


        public bool IsConnected()
        {
            /*
            if(IsInput)
            {
                
            } else
            {

            }*/
            return connections.Count > 0;
        }
    }
}
