﻿#if UNITY_EDITOR
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using Jineni.Game;
using Jineni.Game.BuildingBlocks;
using Jineni.DialogSystem;
using Jineni.Utils;
using Jineni.DialogSystem.Graph;
using Jineni.Game.BuildingBlocks.Internals;
using System.Linq;
using HtmlAgilityPack;
using UnityEngine.Assertions;


namespace Jineni.EditorWindows
{
    public class ImportTwineEditor : EditorWindow
    {
        public static Dialog importToDialog;
        static int currentNodeXPos = 0;
        static int currentNodeYPos = 0;
        private static ReferenceLinkHelper _referenceLinkHelper;

        private static Dictionary<int, Node> nodeIdToNode;

        [MenuItem("Jínění/Twine import")]
        public static void OpenEditorWindow()
        {
            // Get existing open window or if none, make a new one:
            ImportTwineEditor window = GetWindow<ImportTwineEditor>();

            // window.minSize = new Vector2(300, 400);

            window.titleContent = new GUIContent("Twine importer");
            window.wantsMouseMove = true;
            window.Show();
            FocusWindowIfItsOpen<ImportTwineEditor>();
        }

        public static void StartTwineImport(Dialog dialog)
        {
            _referenceLinkHelper = Finder.GetLoneScriptableObjects<ReferenceLinkHelper>();
            if (dialog == null)
            {
                Siren.EditorWarn("import destination emty dialog=null ");
                return;
            }

            string path = EditorUtility.OpenFilePanel("Open Twine", "", "html");
            if (path.Length != 0)
            {
                HtmlDocument document2 = new HtmlDocument();
                document2.Load(path);
                //  var fileContent = File.ReadAllText(path);
                //  Debug.Log(document2.Text);
                HtmlNode[] nodes = document2.DocumentNode.SelectNodes("//tw-passagedata").ToArray();
                var twineDialog = Finder.GetLoneScriptableObjects<TwineDialog>();
                twineDialog.FillWithData(nodes);
                ImportTwineDialogIntoJineniDialog(twineDialog, importToDialog);
                twineDialog.Empty();
                /* foreach (HtmlNode item in nodes)
                 {
                     Debug.Log(item.OuterHtml);
                 }*/
            }
            else
            {
            }
        }

        static void ResetNodePos()
        {
            currentNodeXPos = 100;
            currentNodeYPos = 500;
        }

        static Vector2 GetCurrentNodePos()
        {
            return new Vector2(currentNodeXPos, currentNodeYPos);
        }

        static Vector2 IncrementNodeXPos()
        {
            currentNodeXPos += 300;
            var currentPos = new Vector2(currentNodeXPos, currentNodeYPos);

            return currentPos;
        }

        static Vector2 IncrementNodeYPos()
        {
            currentNodeYPos += 200;
            var currentPos = new Vector2(currentNodeXPos, currentNodeYPos);
            //currentNodeXPos += 250;

            return currentPos;
        }

        public static Node CreateNode(Dialog dialog, TwineDialog twineDialog, TwineNode twineNode)
        {
            Node currentNode = null;
//            Debug.Log("twineNode.nextNodes"+twineNode.id);

            if (twineNode.nextNodes == null || twineNode.nextNodes.Length == 0)
            {
                //    Debug.Log("id="+twineNode.id+" next is empty");
                currentNode = dialog.CreateNode<Replic>(GetCurrentNodePos());

                //jineniDialog.CreateConnection( prevNode.outPorts[0], currentNode.inPort);
                ((VocalNode) currentNode).speaker = _referenceLinkHelper.GetCharacterByChar(twineNode.character);
                ((VocalNode) currentNode).translation.cs = twineNode.text;
                nodeIdToNode.Add(twineNode.id, currentNode);
                return currentNode;
            }

            if (twineNode.nextNodes.Length == 1)
            {
                currentNode = dialog.CreateNode<Replic>(GetCurrentNodePos());
                ((VocalNode) currentNode).speaker = _referenceLinkHelper.GetCharacterByChar(twineNode.character);
                ((VocalNode) currentNode).translation.cs = twineNode.text;
                nodeIdToNode.Add(twineNode.id, currentNode);
                return currentNode;
                //jineniDialog.CreateConnection( prevNode.outPorts[0], currentNode.inPort);
            }

            if (twineNode.nextNodes.Length > 1)
            {
                currentNode = dialog.CreateNode<MultiOption>(GetCurrentNodePos());
                ((VocalNode) currentNode).translation.cs = twineNode.text;
                ((VocalNode) currentNode).speaker = _referenceLinkHelper.GetCharacterByChar(twineNode.character);
                var multiOptionNode = (currentNode as MultiOption);
                multiOptionNode.dialogOptions = new List<DialogOption>(5);
                multiOptionNode.CreateDialogOptions(twineNode.nextNodes.Length);
                for (int i = 0; i < twineNode.nextNodes.Length; i++)
                {
                    multiOptionNode.dialogOptions[i].translation.cs = twineNode.nextNodes[i].text;
                }

                nodeIdToNode.Add(twineNode.id, currentNode);
                return currentNode;
            }

            return currentNode;
        }

        public static void ImportTwineDialogIntoJineniDialog(TwineDialog twineDialog, Dialog jineniDialog)

        {
            jineniDialog.RemoveAllNodesAndConnections();

            ResetNodePos();
            Node firstNode;
            Node emptyPointNode;
            emptyPointNode = jineniDialog.CreateNode<EntryPoint>(GetCurrentNodePos());
            IncrementNodeXPos();
            nodeIdToNode = new Dictionary<int, Node>(100);
            bool firstNodeConnected = false;
            foreach (var twineDialogItem in twineDialog.twineNodes)
            {
                var twineNode = twineDialogItem.value;
                /*    if (nodeIdToNode.ContainsKey(twineNode.id))
                    {
                        continue;
                    }*/

                if (!firstNodeConnected)
                {
                    firstNode = CreateNode(jineniDialog, twineDialog, twineNode);
                    firstNodeConnected = true;
                    jineniDialog.CreateConnection(
                        emptyPointNode.outPorts[0],
                        firstNode.inPort
                    );
                }
                else
                {
                    CreateNode(jineniDialog, twineDialog, twineNode);
                }
            }

            foreach (var twineDialogItem in twineDialog.twineNodes)
            {
                var twineNode = twineDialogItem.value;
                if (twineNode.nextNodes == null || twineNode.nextNodes.Length == 0)
                {
                    continue;
                }

                if (twineNode.nextNodes.Length == 1)
                {
                    //move only when is not already moved
                    if (nodeIdToNode[twineNode.nextNodes[0].nextId].inPort.IsConnected())
                    {
                        //dont move
                    }
                    else
                    {
                        Vector2 pos = IncrementNodeXPos();
                        nodeIdToNode[twineNode.nextNodes[0].nextId].ChangePosition(pos);
                    }


                    jineniDialog.CreateConnection(
                        nodeIdToNode[twineNode.id].outPorts[0],
                        nodeIdToNode[twineNode.nextNodes[0].nextId].inPort
                    );
                    continue;
                }

                if (twineNode.nextNodes.Length > 1)
                {
                    // multiOptionNode.CreateDialogOptions(twineNode.nextNodes.Length);
                    for (int i = 0; i < twineNode.nextNodes.Length; i++)
                    {
                        //move only when is not already moved
                        if (nodeIdToNode[twineNode.nextNodes[i].nextId].inPort.IsConnected())
                        {
                            //dont move
                        }
                        else
                        {
                            Vector2 pos;
                            if (i == 0)
                            {
                                pos = IncrementNodeXPos();
                            }
                            else
                            {
                                pos = IncrementNodeYPos();
                            }

                            nodeIdToNode[twineNode.nextNodes[i].nextId].ChangePosition(pos);
                        }

                        jineniDialog.CreateConnection(
                            nodeIdToNode[twineNode.id].outPorts[i],
                            nodeIdToNode[twineNode.nextNodes[i].nextId].inPort
                        );
                    }

                    continue;
                }
            }

            nodeIdToNode.Clear();
            DialogEditor.OpenEditor(jineniDialog);
        }

        private void OnGUI()
        {
            DrawControlButtons();

            if (GUI.changed)
            {
                Repaint();
            }
        }

        void DrawControlButtons()
        {
            GUILayout.BeginHorizontal();
            EditorGUI.BeginChangeCheck();
            importToDialog = EditorGUILayout.ObjectField(importToDialog, typeof(Dialog), false) as Dialog;
            if (EditorGUI.EndChangeCheck())
            {
                if (importToDialog && !importToDialog.IsEmpty())
                {
                    Siren.EditorError("dialog node " + importToDialog.name + " is not empty, will be overriten!");
                }
            }


            //------------------------------------------------------------------------------------------------------------------
            if (GUILayout.Button("Import twine file"))
            {
                StartTwineImport(importToDialog);
            }

            GUILayout.EndHorizontal();
        }
    }
}
#endif