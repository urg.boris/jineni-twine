﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using Jineni.Game.BuildingBlocks.Internals;
using Jineni.Utils;
using UnityEngine;

namespace Jineni.DialogSystem
{
    [Serializable]
    public class KeyValuePair
    {
        public int key;
        public TwineNode value;

        public KeyValuePair(int key, TwineNode value)
        {
            this.key = key;
            this.value = value;
        }
    }
    [Serializable]
    public class NextNode
    {
        public int nextId;
        public string text;

        public NextNode(int nextId, string text)
        {
            this.nextId = nextId;
            this.text = text;
        }
    }
    [Serializable]
    public class TwineNode
    {
        public int id;
        public string character;
        public string text;
        public NextNode[] nextNodes;//==-1 no next
        public const string CHARACTER_ENVIROMENT = "e";
        public bool isOneOfMultiOptionNode = false;
        public TwineNode(int id, string text)
        {

            this.id = id;
//            Debug.Log("stripping");
  //          Debug.Log(text);
            String strippedText = Regex.Replace(text, @"\(set.*\)", String.Empty);
            string characterString = CHARACTER_ENVIROMENT;
            Regex regex = new Regex(@"\$(.*):.*");
            var matched = regex.Matches(strippedText);
           // Debug.Log("matched.Count="+matched.Count);
            if (matched.Count > 0)
            {
               // Debug.Log("groups="+matched[0].Groups.Count);
                characterString = matched[0].Groups[1].Value;
                strippedText  = Regex.Replace(strippedText, @"\$(.*):", String.Empty);
                //Debug.Log(matched[0].Groups[2].Value);
            }
            
            

            strippedText = strippedText.Trim();
//            Debug.Log(strippedText);
  //          Debug.Log(characterString);
            this.character = characterString;
            this.text = strippedText;
        }

        public bool IsOneOfMultiOptionNode()
        {
            return isOneOfMultiOptionNode;
        }

        public override string ToString()
        {
            return id + ": " + text;
        }
    }
    [CreateAssetMenu(fileName = "TwineDialog", menuName = "Jineni/internal/TwineDialog")]
    public class TwineDialog : ScriptableObject
    {
        public List<KeyValuePair> twineNodes;

        private Dictionary<string, int> nodeNameToId;
        private Dictionary<int, string> nodeIdToText;
        private Dictionary<int, TwineNode> nodeIdToNode;
       // public List<int, TwineNode> twineNodes;

       public void Empty()
       {
           /*
           nodeIdToNode.Clear();
           nodeIdToText.Clear();
           nodeNameToId.Clear();
           */
       }

       public int GetNodeIdByName(string name)
       {
          // Debug.Log("--geting id by name:"+name);
           if (nodeNameToId.ContainsKey(name))
           {
               return nodeNameToId[name];
           }
           else
           {
               return -1;
           }
           
       }
       public string GetNodeTextById(int id)
       {
           if (nodeIdToText.ContainsKey(id))
           {
               return nodeIdToText[id];
           }
           else
           {
               return "empty";
           }
       }
       
       public TwineNode GetNodeById(int id)
       {
           if (nodeIdToNode.ContainsKey(id))
           {
               return nodeIdToNode[id];
           }
           else
           {
               return null;
           }
       }

       public string RemoveTwineLinks(string nodeText)
       {
           nodeText = nodeText.Trim();
           //Regex regex = new Regex(@"\[\[(.*?)\]\]");
           //Debug.Log("----cleaning");
           if (nodeText == "")
           {
             //  Debug.Log("==is empty");
               return "";
           }
           String result = Regex.Replace(nodeText, @"\[.*\]", String.Empty).Trim();

//           Debug.Log(nodeText);
  //         Debug.Log(" => ");
    //       Debug.Log(result);
           return result;
       }

        public void FillWithData(HtmlNode[] nodes)
        {
            //Debug.Log("filling data");
            
            twineNodes = new List<KeyValuePair>(100);
            nodeNameToId = new Dictionary<string, int>(100);
            nodeIdToText = new Dictionary<int, string>(100);
            nodeIdToNode = new Dictionary<int, TwineNode>(100);
            //create list then link ids
            foreach (HtmlNode item in nodes)
            {
                int id = int.Parse(item.GetAttributeValue("pid", "-1"));
                string name = item.GetAttributeValue("name", "");
                if (name != "")
                {
                    nodeNameToId.Add(name, id);
                }

                string text = item.InnerHtml;
                var newNode = new TwineNode(id, text);
//                Debug.Log("==================creating new node============ "+newNode);
                twineNodes.Add(new KeyValuePair(id, newNode));
                //TODO add characters
                nodeIdToText.Add(id, RemoveTwineLinks(newNode.text));
                nodeIdToNode.Add(id, newNode);
                
            }
            foreach (KeyValuePair item in twineNodes)
            {
                var node = item.value;
                Regex regex = new Regex(@"\[\[(.*?)\]\]");
 
                var matched = regex.Matches(node.text);
               // Debug.Log("----show sublinks for "+matched.Count);
                if (matched.Count == 0)
                {
                    //int[] nextIds = new[] {-1};
                    node.nextNodes = null;
                    node.text = GetNodeTextById(node.id);
                }
                else
                {
                    List<NextNode> nextNodes = new List<NextNode>();
                    foreach (Match m in matched)
                    {
                        string nextNodeName = "";
                        int nextNodeId;
                        string nextNodeText = "";
                        //TODO split by | text node or link node text
                        var linkContent = m.Groups[1].Value;
                        var linkSplitted = linkContent.Split('|');
                        if (linkSplitted.Length == 1)
                        {
                            
                            nextNodeName = linkSplitted[0];
                            nextNodeId = GetNodeIdByName(nextNodeName);
                            nextNodeText = linkSplitted[0]; //GetNodeTextById(nextNodeId);
                        }
                        else
                        {
                            
                            nextNodeName = linkSplitted[1];
                            nextNodeId = GetNodeIdByName(nextNodeName);
                            nextNodeText = linkSplitted[0];
                        }

                        
                        nextNodes.Add(new NextNode(nextNodeId, nextNodeText));

                    }

                    node.nextNodes = nextNodes.ToArray();
                    node.text = GetNodeTextById(node.id);
                    if (nextNodes.Count > 1)
                    {
                        for (int i = 0; i < node.nextNodes.Length; i++)
                        {
                            if (node.nextNodes[i] == null || node.nextNodes[i].nextId == -1)
                            {
                                continue;
                            }

                            GetNodeById(node.nextNodes[i].nextId).isOneOfMultiOptionNode = true;
                        }
                    }

                    
                }
            }
            
        }
    }
}