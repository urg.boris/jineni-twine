﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game;
using Jineni.DialogSystem;

namespace Jineni.EditorWindows
{
    [CreateAssetMenu(fileName = "SettingsOfEditor", menuName = "Jineni/Internal/Base/SettingsOfEditor", order = 62)]
    //!! cant be named EditorEttings - conflicting with unity editorsetting and then troubles
    public class SettingsOfEditor : ScriptableObject
    {
        public Lang currentLang;
        public Dialog lastOpenedDialog;
    }
}
