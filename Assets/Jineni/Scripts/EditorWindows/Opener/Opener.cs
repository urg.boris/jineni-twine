﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Jineni.Game;
using Jineni.Game.BuildingBlocks;
using Jineni.DialogSystem;
using Jineni.Utils;
using Jineni.DialogSystem.Graph;
using Jineni.Game.BuildingBlocks.Internals;
using System.Linq;
using UnityEngine.Assertions;
using UnityEditor.SceneManagement;
using Jineni.Game.GameStateBlocks;

namespace Jineni.EditorWindows
{
    /*
    [SerializeField]
    public class OpenableList
    {
        public int index = 0;
        public string[] names;
        public List<IEditorNamable> items;

        public OpenableList(List<IEditorNamable> items)
        {
            this.items = items;
            PrepareNames();
        }
        
        public void PrepareNames()
        {
            names = new string[items.Count];
            for (int i = 0; i < items.Count; i++)
            {
                names[i] = items[i].GetEditorName();
            }
        }

    }*/
    public class Opener: EditorWindow
    {
        //public Inventory inventory;

        public List<CharacterCore> characters;
        public List<ItemCore> items;
        public List<LocationCore> locations;
        public List<GameState> gameStates;

        public List<string> scenePaths;

        public string[] charNames;
        public string[] itemNames;
        public string[] locationNames;
        public string[] gameStateNames;
        public string[] sceneNamesArr;


        int charIndex;
        int locIndex;
        int itemIndex;
        int sceneIndex;
        int gameStateIndex;

        //public OpenableList gameStatesList;

        [MenuItem("Jínění/Open...")]
        public static void OpenEditorWindow()
        {
            // Get existing open window or if none, make a new one:
            Opener window = GetWindow<Opener>();

            
            // window.minSize = new Vector2(300, 400);

            window.titleContent = new GUIContent("Opener");
            window.wantsMouseMove = true;
            window.Show();
            FocusWindowIfItsOpen<Opener>();
        }       

        void PrepareListsIfNot()
        {
            if (locationNames == null)
            {
                if (GameManager.Instance==null)
                {
                    Siren.EditorError("no GameManager in scene");
                    return;
                }
                locations = GameManager.Instance.locations;
                characters = GameManager.Instance.characters;
                items = GameManager.Instance.items;
                gameStates = new List<GameState>(Finder.GetScriptableObjects<GameState>());
                //gameStatesList = new OpenableList(new List<IEditorNamable>(Finder.GetScriptableObjects<GameState>()));

                Debug.Log("reload lists");
                locationNames = new string[locations.Count];
                charNames = new string[characters.Count];
                itemNames = new string[items.Count];
                gameStateNames = new string[gameStates.Count];


                for (int i = 0; i < locations.Count; i++)
                {
                    locationNames[i] = locations[i].GetEditorName();
                }

                for (int i = 0; i < characters.Count; i++)
                {
                    charNames[i] = characters[i].GetEditorName();
                }

                for (int i = 0; i < items.Count; i++)
                {
                    itemNames[i] = items[i].GetEditorName();
                }
                
                for (int i = 0; i < gameStates.Count; i++)
                {
                    gameStateNames[i] = gameStates[i].name;
                }

                string[] guids = AssetDatabase.FindAssets("t: SceneAsset");
                string path;
                string sceneName;
                List<string> sceneNames = new List<string>();
                scenePaths = new List<string>();
                for (int i = 0; i < guids.Length; i++)         //probably could get optimized 
                {
                    path = AssetDatabase.GUIDToAssetPath(guids[i]);
                    sceneName = System.IO.Path.GetFileNameWithoutExtension(path);
                    if(sceneName[0] == 'L')
                    {
                        sceneNames.Add(sceneName);
                        scenePaths.Add(path);
                    }
                }

                sceneNamesArr = sceneNames.ToArray();

                /*
                int sceneCount = UnityEngine.SceneManagement.SceneManager.sceneCountInBuildSettings;
                sceneNames = new string[sceneCount];
                for (int i = 0; i < sceneCount; i++)
                {
                    sceneNames[i] = System.IO.Path.GetFileNameWithoutExtension(UnityEngine.SceneManagement.SceneUtility.GetScenePathByBuildIndex(i));
                    //scenePaths[i] = UnityEngine.SceneManagement.SceneUtility.GetScenePathByBuildIndex(i);
                }*/
/*
                Debug.Log(sceneNames);
                Debug.Log(scenePaths);*/
                /*
                List<string> scenes = new List<string>();
                foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
                {
                    if (scene.enabled)
                    {
                        scenes.Add(scene.path);
                    }
                }*/
            }
        }
        private void OnGUI()
        {
            DrawControls();
        }

        void DrawControls()
        {
            PrepareListsIfNot();

            EditorGUILayout.LabelField("Open Location(Scene)");
            EditorGUI.BeginChangeCheck();
            sceneIndex = EditorGUILayout.Popup(sceneIndex, sceneNamesArr);
            if (EditorGUI.EndChangeCheck())
            {
                if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
                {
                    EditorSceneManager.OpenScene(scenePaths[sceneIndex]);
                    EditorSceneManager.OpenScene(scenePaths[sceneIndex]);
                    //!! dont do that, crashing
                    /*Opener window = GetWindow<Opener>();
                    window.Close();*/
                }
                else
                {
                    //Debug.Log("Don't save Scene(s)");
                }

            }

            EditorGUILayout.LabelField("Open CoreBuilingBlock(DataContainer)");
            EditorGUI.BeginChangeCheck();
            charIndex = EditorGUILayout.Popup(charIndex, charNames);
            if(EditorGUI.EndChangeCheck())
            {
                OpenObject(characters[charIndex]);
            }

            EditorGUI.BeginChangeCheck();
            locIndex = EditorGUILayout.Popup(locIndex, locationNames);
            if (EditorGUI.EndChangeCheck())
            {
                OpenObject(locations[locIndex]);
            }

            EditorGUI.BeginChangeCheck();
            itemIndex = EditorGUILayout.Popup(itemIndex, itemNames);
            if (EditorGUI.EndChangeCheck())
            {
                OpenObject(items[itemIndex]);
            }

            EditorGUILayout.LabelField("Open GameState(Saved Game)");
            EditorGUI.BeginChangeCheck();
        //   Debug.Log(gameStatesList.index);
          //  Debug.Log(gameStatesList.names);
            gameStateIndex = EditorGUILayout.Popup(gameStateIndex, gameStateNames);
            if (EditorGUI.EndChangeCheck())
            {
                OpenObject(gameStates[gameStateIndex]);
            }

        }

        void OpenObject(ScriptableObject SObject)
        {
            EditorGUIUtility.PingObject(SObject);
            Selection.activeObject = SObject;
        }
    }

}
#endif