﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Jineni.Game;
using Jineni.Game.BuildingBlocks;
using Jineni.DialogSystem;
using Jineni.Utils;
using Jineni.DialogSystem.Graph;
using Jineni.Game.BuildingBlocks.Internals;
using UnityEngine.Assertions;
#if UNITY_EDITOR
using UnityEditor.Callbacks;

namespace Jineni.EditorWindows
{
    public class ItemCombinationsEditor : EditorWindow
    {
        public ItemCombinationsManager itemCombinationsManager;

        private ItemCore[] items;
        private string[] itemNames;
        int currentItemIndex;
        List<ItemItemCombination> selectedItemCombinations = new List<ItemItemCombination>();
        Vector2 itemScrollPos;

        private CharacterCore[] characters;
        private string[] characterNames;
        int currentCharacterIndex;
        List<ItemCharacterCombination> selectedCharacterCombinations = new List<ItemCharacterCombination>();
        Vector2 charScrollPos;

        [MenuItem("Jínění/Item Combination Editor")]
        static void OpenEditorWindow()
        {
            // Get existing open window or if none, make a new one:
            ItemCombinationsEditor window = GetWindow<ItemCombinationsEditor>();

            window.minSize = new Vector2(400, 300);
            window.titleContent = new GUIContent("Item Combination");
            window.wantsMouseMove = true;
            window.Show();
            FocusWindowIfItsOpen<ItemCombinationsEditor>();

        }


        //!!open by doublecklick
        [OnOpenAsset(0)]
        public static bool OnOpen(int instanceID, int line)
        {
            ItemCombinationsManager associatedObject = EditorUtility.InstanceIDToObject(instanceID) as ItemCombinationsManager;
            if (associatedObject != null)
            {
                OpenEditorWindow();
                return true;
            }
            return false;
        }

        void PrepareLists()
        {
            items = Finder.GetScriptableObjects<ItemCore>();
            itemNames = new string[items.Length];
            for (int i = 0; i < items.Length; i++)
            {
                itemNames[i] = items[i].name;
            }

            //Debug.Log(itemNames.Length);

            characters = Finder.GetScriptableObjects<CharacterCore>();
            characterNames = new string[characters.Length];
            for (int i = 0; i < characters.Length; i++)
            {
                characterNames[i] = characters[i].name;
            }
        }


        private void OnGUI()
        {
            DrawControlButtons();

            if (GUI.changed)
            {
                Repaint();
            }
        }

        void GetDataIfNeeded()
        {
            if (itemCombinationsManager == null)
            {
                GetManager();
            }
            if(items == null)
            {
                PrepareLists();
            }
            
        }

        void GetManager()
        {
            ItemCombinationsManager[] list = Finder.GetScriptableObjects<ItemCombinationsManager>();
            if (list.Length != 1)
            {
                Assert.IsTrue(false, "internal objects failed - " + typeof(ItemCombinationsManager).Name+" must have 1 instance  -" + list.Length+" found");
                return;
            }
            itemCombinationsManager = list[0];
        }

        void DrawControlButtons()
        {

            
            GetDataIfNeeded();

            //Debug.Log(Screen.width);
            //!!itemitem box - 4/5 - experimentalne zjisteno
            GUILayout.BeginArea(new Rect(0, 0, Screen.width*4/5, 900));

            //!!itemitem box razeni odzhora dolu
            GUILayout.BeginVertical();

            if (GUILayout.Button("Test All Items Combinations"))
            {
                IntegrityCheckerEditor.TestItemCombinations();                    
            }

            if (GUILayout.Button("Test All Items Combinations With Its Dialogs(long)"))
            {
                IntegrityCheckerEditor.TestItemCombinations(true, true);
            }


            GUILayout.Label("Item Item Combinations", EditorStyles.boldLabel);
            EditorGUI.BeginChangeCheck();
            currentItemIndex = EditorGUILayout.Popup(currentItemIndex, itemNames);
            if (EditorGUI.EndChangeCheck())
            {
                FindSelectedItemCombination(items[currentItemIndex]);
            }

            itemScrollPos = EditorGUILayout.BeginScrollView(itemScrollPos, false, true, GUILayout.Height(200-50));

            if (selectedItemCombinations.Count > 0)
            {
                for (int i = 0; i < selectedItemCombinations.Count; i++)
                {
                    GUILayout.BeginHorizontal();
                    EditorGUI.BeginChangeCheck();
                    selectedItemCombinations[i].object1 = EditorGUILayout.ObjectField(selectedItemCombinations[i].object1, typeof(ItemCore), false) as ItemCore;
                    selectedItemCombinations[i].object2 = EditorGUILayout.ObjectField(selectedItemCombinations[i].object2, typeof(ItemCore), false) as ItemCore;
                    selectedItemCombinations[i].entryPoint = EditorGUILayout.ObjectField(selectedItemCombinations[i].entryPoint, typeof(EntryPoint), false) as EntryPoint;
                    if (EditorGUI.EndChangeCheck())
                    {
                        EditorUtility.SetDirty(selectedItemCombinations[i]);
                    }
                    if (GUILayout.Button("-", GUILayout.Width(20)))
                    {
                        RemoveItemItemComb(selectedItemCombinations[i]);
                        i--;
                        //!!to reload empty
                        FindSelectedItemCombination(items[currentItemIndex]);
                        //!! dont draw the rest!
                        continue;

                    }
                    GUILayout.Space(10);
                    GUILayout.EndHorizontal();
                }
            }
            EditorGUILayout.EndScrollView();


            GUILayout.Space(10);
            if (GUILayout.Button("+", GUILayout.Width(20)))
            {
                AddNewItemItemComb();
                //!!to reload empty
                FindSelectedItemCombination(items[currentItemIndex]);

            }
            GUILayout.Space(10);

            GUILayout.Label("Item Character Combinations", EditorStyles.boldLabel);
            EditorGUI.BeginChangeCheck();
            currentCharacterIndex = EditorGUILayout.Popup(currentCharacterIndex, characterNames);
            if (EditorGUI.EndChangeCheck())
            {
                //Debug.Log(" EditorGUILayout.Popup");
                FindSelectedCharacterCombination(characters[currentCharacterIndex]);
            }

            charScrollPos = EditorGUILayout.BeginScrollView(charScrollPos, false, true, GUILayout.Height(200 - 50));

            if (selectedCharacterCombinations.Count > 0)
            {
                for (int i = 0; i < selectedCharacterCombinations.Count; i++)
                {
                    GUILayout.BeginHorizontal();
                    EditorGUI.BeginChangeCheck();
                    selectedCharacterCombinations[i].object1 = EditorGUILayout.ObjectField(selectedCharacterCombinations[i].object1, typeof(ItemCore), false) as ItemCore;
                    selectedCharacterCombinations[i].object2 = EditorGUILayout.ObjectField(selectedCharacterCombinations[i].object2, typeof(CharacterCore), false) as CharacterCore;
                    selectedCharacterCombinations[i].entryPoint = EditorGUILayout.ObjectField(selectedCharacterCombinations[i].entryPoint, typeof(EntryPoint), false) as EntryPoint;
                    if (EditorGUI.EndChangeCheck())
                    {
                        EditorUtility.SetDirty(selectedCharacterCombinations[i]);
                    }
                    if (GUILayout.Button("-", GUILayout.Width(20)))
                    {
                        RemoveItemCharComb(selectedCharacterCombinations[i]);
                        i--;
                        //!!to reload empty
                        FindSelectedCharacterCombination(characters[currentCharacterIndex]);
                        //!! dont draw the rest!
                        continue;

                    }
                    GUILayout.Space(10);
                    GUILayout.EndHorizontal();
                }
            }
            EditorGUILayout.EndScrollView();

            GUILayout.Space(10);
            if (GUILayout.Button("+", GUILayout.Width(20)))
            {
                AddNewItemCharComb();
                //!!to reload empty
                FindSelectedCharacterCombination(characters[currentCharacterIndex]);
            }

                        //!!itemitem box razeni odzhora dolu
            GUILayout.EndVertical();
            //!!itemitem box
            GUILayout.EndArea();
            
        }

        void FindSelectedItemCombination(ItemCore item)
        {
            selectedItemCombinations = itemCombinationsManager.GetFilledOrUnfilledEntriesBy(item);
        }

        void FindSelectedCharacterCombination(CharacterCore character)
        {
            //Debug.Log("FindSelectedCharacterCombination");
            selectedCharacterCombinations = itemCombinationsManager.GetFilledOrUnfilledEntriesBy(character);
        }


        

        void RemoveItemItemComb(ItemItemCombination itemItemCombinations)
        {
            //!!nejdrif destroy scriptable, pak odebra z listu
            Object.DestroyImmediate(itemItemCombinations, true);
            itemCombinationsManager.itemEntries.Remove(itemItemCombinations);
            //RemoveAt(i);

            AssetDatabase.SaveAssets();
        }


        private void AddNewItemItemComb()
        {
            ItemItemCombination newComb = CreateInstance<ItemItemCombination>();

            newComb.name = "ItemItemComb-" + itemCombinationsManager.lastEntryIndex++;
            itemCombinationsManager.itemEntries.Add(newComb);

            AssetDatabase.AddObjectToAsset(newComb, itemCombinationsManager);
            AssetDatabase.SaveAssets();
        }


        void RemoveItemCharComb(ItemCharacterCombination itemCharacterCombination)
        {
            //!!nejdrif destroy scriptable, pak odebra z listu
            Object.DestroyImmediate(itemCharacterCombination, true);
            itemCombinationsManager.characterEntries.Remove(itemCharacterCombination);

            AssetDatabase.SaveAssets();
        }


        private void AddNewItemCharComb()
        {
            ItemCharacterCombination newComb = CreateInstance<ItemCharacterCombination>();

            newComb.name = "ItemCharacterComb-" + itemCombinationsManager.lastEntryIndex++;
            itemCombinationsManager.characterEntries.Add(newComb);

            AssetDatabase.AddObjectToAsset(newComb, itemCombinationsManager);
            AssetDatabase.SaveAssets();
        }
    }

}
#endif