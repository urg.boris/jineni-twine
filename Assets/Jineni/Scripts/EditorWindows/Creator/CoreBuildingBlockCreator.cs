﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Jineni.Game;
using Jineni.Game.BuildingBlocks;
using Jineni.DialogSystem;
using Jineni.Utils;
using Jineni.DialogSystem.Graph;
using Jineni.Game.BuildingBlocks.Internals;
using System.Linq;
using UnityEngine.Assertions;

namespace Jineni.EditorWindows
{
    public class CoreBuildingBlockCreator : EditorWindow
    {
        //public Inventory inventory;

        const string BASE_DIALOG_DIR = "Assets/Jineni/Resources/Dialogs/Soustredeni/";
        const string CHARACTERS_DIR = "Assets/Jineni/Game/BuildingBlocks/Characters/";
        const string LOCATIONS_DIR = "Assets/Jineni/Game/BuildingBlocks/Locations/";
        const string ITEMS_DIR = "Assets/Jineni/Game/BuildingBlocks/Items/";

        public string[] objects = new string[] { "Character", "Location", "Item" };
        public int objectIndex = 0;

        int uniqueId;
        string objectName;



        [MenuItem("Jínění/Create Core Block")]
        public static void OpenEditorWindow()
        {
            // Get existing open window or if none, make a new one:
            CoreBuildingBlockCreator window = GetWindow<CoreBuildingBlockCreator>();

           // window.minSize = new Vector2(300, 400);
            
            window.titleContent = new GUIContent("Create Core Block");
            window.wantsMouseMove = true;
            window.Show();
            FocusWindowIfItsOpen<CoreBuildingBlockCreator>();
        }
/*
        public static void OpenEditorWindowWithDialogToTest(Dialog dialog)
        {
            importToDialog = dialog;
            // Get existing open window or if none, make a new one:
            IntegrityCheckerEditor window = GetWindow<IntegrityCheckerEditor>();

            // window.minSize = new Vector2(300, 400);

            window.titleContent = new GUIContent("Integrity Checker");
            window.wantsMouseMove = true;
            window.Show();
            FocusWindowIfItsOpen<IntegrityCheckerEditor>();
        }*/


        private void OnGUI()
        {
            DrawControls();
        }

        void DrawControls()
        {
            objectIndex = EditorGUILayout.Popup(objectIndex, objects);


            switch (objectIndex)
            {
                case 0:

                    GUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("unique id");
                    uniqueId = EditorGUILayout.IntField(uniqueId);
                    GUILayout.EndHorizontal();
                    GUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("name");
                    objectName = EditorGUILayout.TextField(objectName);
                    GUILayout.EndHorizontal();

                    if (GUILayout.Button("Create New Character"))
                    {
                        CreateCharacter(uniqueId, objectName);
                    }

                    
                    break;
                case 1:

                    GUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("unique id");
                    uniqueId = EditorGUILayout.IntField(uniqueId);
                    GUILayout.EndHorizontal();
                    GUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("name");
                    objectName = EditorGUILayout.TextField(objectName);
                    GUILayout.EndHorizontal();

                    if (GUILayout.Button("Create New Location"))
                    {
                        CreateLocation(uniqueId, objectName);
                    }
                    
                    break;
                case 2:

                    GUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("unique id");
                    uniqueId = EditorGUILayout.IntField(uniqueId);
                    GUILayout.EndHorizontal();
                    GUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("name");
                    objectName = EditorGUILayout.TextField(objectName);
                    GUILayout.EndHorizontal();

                    if (GUILayout.Button("Create New Item"))
                    {
                        CreateItem(uniqueId, objectName);
                    }

                    
                    break;
                default:
                    break;
            }



            /*
            //------------------------------------------------------------------------------------------------------------------
            if (GUILayout.Button("Test If Internal Objects Are Set"))
            {
                TestInternals();
            }
            //------------------------------------------------------------------------------------------------------------------
            GUILayout.BeginHorizontal();
            importToDialog = EditorGUILayout.ObjectField(importToDialog, typeof(Dialog), false) as Dialog;

            if (GUILayout.Button("Test This Dialog Integrity"))
            {
                TestOneDialog(importToDialog);
            }
            GUILayout.EndHorizontal();
            //------------------------------------------------------------------------------------------------------------------
            if (GUILayout.Button("Test All Dialogs Integrity(long)"))
            {
                TestDialogs();
            }
            //------------------------------------------------------------------------------------------------------------------
            if (GUILayout.Button("Test Item Combinations"))
            {
                TestItemCombinations();
            }
            //------------------------------------------------------------------------------------------------------------------
            if (GUILayout.Button("Test Examinations"))
            {
                TestExaminations();
            }

            if (GUILayout.Button("Test Uniqueness Where Needed"))
            {
                TestUniqueness(false);
            }
            //------------------------------------------------------------------------------------------------------------------
            if (GUILayout.Button("Test This Location"))
            {
                FindObjectOfType<LocationController>().CheckIntegrity();
            }*/
        }
        /*
        void CreateCoreObject()
        {
            switch (objectIndex)
            {
                case 0:
                    CreateCharacter(uniqueId, objectName);
                    break;
                case 1:
                    CreateLocation(uniqueId, objectName);
                    break;
                case 2:
                    CreateItem(uniqueId, objectName);
                    break;
                default:
                    break;
            }
        }*/

        void CreateCharacter(int id, string name)
        {
            string type = (id >= 100 ? "O" : "P"); 
            EntryPoint examineEP = DialogEditor.CreateNewDialog(type + id + "e - " + name, BASE_DIALOG_DIR); 
            EntryPoint dialogEP = DialogEditor.CreateNewDialog(type + id + " - " + name, BASE_DIALOG_DIR);
            CharacterCore coreExists =  GameManager.Instance.GetCharacterById(id);
            if(coreExists != null)
            {
                Siren.EditorWarn("character with id=" + id + " already exists");
                EditorGUIUtility.PingObject(coreExists);
            }
            CharacterCore coreObject = CreateInstance<CharacterCore>();
            coreObject.editorName = type + id + " - " + name;
            coreObject.uniqueId = id;
            coreObject.currentNameIndex = 0;
            coreObject.currentDialogEntryPoint = dialogEP;
            coreObject.currentExamineEntryPoint = examineEP;
            coreObject.defaultDialogEntryPoint = dialogEP;
            coreObject.defaultExamineEntryPoint = examineEP;
            coreObject.names = new Localization.Translation[1]
            {
                new Localization.Translation()
                {
                    cs = name
                }
            };
            AssetDatabase.CreateAsset(coreObject, CHARACTERS_DIR + coreObject.editorName + ".asset");
            AssetDatabase.SaveAssets();
            EditorGUIUtility.PingObject(coreObject);
        }

        void CreateLocation(int id, string name)
        {
            LocationCore coreExists = GameManager.Instance.GetLocationById(id);
            if (coreExists != null)
            {
                Siren.EditorWarn("location with id=" + id + " already exists");
                EditorGUIUtility.PingObject(coreExists);
            }
            LocationCore coreObject = CreateInstance<LocationCore>();
            coreObject.editorName = "L" + id + " - " + name;
            coreObject.uniqueId = id;
            coreObject.sceneIndex = id;
            coreObject.currentNameIndex = 0;
            coreObject.names = new Localization.Translation[1]
            {
                new Localization.Translation()
                {
                    cs = name
                }
            };
            AssetDatabase.CreateAsset(coreObject, LOCATIONS_DIR + coreObject.editorName + ".asset");
            AssetDatabase.SaveAssets();
            EditorGUIUtility.PingObject(coreObject);
        }

        void CreateItem(int id, string name)
        {
            EntryPoint dialogEP = DialogEditor.CreateNewDialog("I" + id + "e - " + name, BASE_DIALOG_DIR);
            ItemCore coreExists = GameManager.Instance.GetItemById(id);
            if (coreExists != null)
            {
                Siren.EditorWarn("item with id=" + id + " already exists");
                EditorGUIUtility.PingObject(coreExists);
            }
            ItemCore coreObject = CreateInstance<ItemCore>();
            coreObject.editorName = "I" + id + " - " + name;
            coreObject.uniqueId = id;
            coreObject.currentNameIndex = 0;
            coreObject.currentExamineEntryPoint = dialogEP;
            coreObject.names = new Localization.Translation[1] 
            {
                new Localization.Translation()
                {
                    cs = name
                }
            };
            AssetDatabase.CreateAsset(coreObject, ITEMS_DIR + coreObject.editorName + ".asset");
            AssetDatabase.SaveAssets();
            EditorGUIUtility.PingObject(coreObject);
        }

    }

}
#endif