﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Jineni.Game;
using Jineni.Game.BuildingBlocks;
using Jineni.DialogSystem;
using Jineni.Utils;
using Jineni.DialogSystem.Graph;
using Jineni.Game.BuildingBlocks.Internals;
using System.Linq;
using UnityEngine.Assertions;

namespace Jineni.EditorWindows
{
    public class IntegrityCheckerEditor : EditorWindow
    {
        //public Inventory inventory;

        [HideInInspector]
        public static Dialog dialogToTest;


        [MenuItem("Jínění/Integrity Checker")]
        public static void OpenEditorWindow()
        {
            // Get existing open window or if none, make a new one:
            IntegrityCheckerEditor window = GetWindow<IntegrityCheckerEditor>();

           // window.minSize = new Vector2(300, 400);
            
            window.titleContent = new GUIContent("Integrity Checker");
            window.wantsMouseMove = true;
            window.Show();
            FocusWindowIfItsOpen<IntegrityCheckerEditor>();
        }

        public static void OpenEditorWindowWithDialogToTest(Dialog dialog)
        {
            dialogToTest = dialog;
            // Get existing open window or if none, make a new one:
            IntegrityCheckerEditor window = GetWindow<IntegrityCheckerEditor>();

            // window.minSize = new Vector2(300, 400);

            window.titleContent = new GUIContent("Integrity Checker");
            window.wantsMouseMove = true;
            window.Show();
            FocusWindowIfItsOpen<IntegrityCheckerEditor>();
        }


        private void OnGUI()
        {


            DrawControlButtons();

            /*
            ProcessNodeEvents(Event.current);
            ProcessEvents(Event.current);*/

            if (GUI.changed)
            {
                Repaint();
            }
        }

        void DrawControlButtons()
        {
            //------------------------------------------------------------------------------------------------------------------
            if (GUILayout.Button("Test If Internal Objects Are Set"))
            {
                TestInternals();
            }
            //------------------------------------------------------------------------------------------------------------------
            GUILayout.BeginHorizontal();
            dialogToTest = EditorGUILayout.ObjectField(dialogToTest, typeof(Dialog), false) as Dialog;

            if (GUILayout.Button("Test This Dialog Integrity"))
            {
                TestOneDialog(dialogToTest);
            }
            GUILayout.EndHorizontal();
            //------------------------------------------------------------------------------------------------------------------
            if (GUILayout.Button("Test All Dialogs Integrity(long)"))
            {
                TestDialogs();
            }
            //------------------------------------------------------------------------------------------------------------------
            if (GUILayout.Button("Test Item Combinations"))
            {
                TestItemCombinations();
            }
            //------------------------------------------------------------------------------------------------------------------
            if (GUILayout.Button("Test Examinations"))
            {
                TestExaminations();
            }

            if (GUILayout.Button("Test Uniqueness Where Needed"))
            {
                TestUniqueness(false);
            }
            //------------------------------------------------------------------------------------------------------------------
            if (GUILayout.Button("Test This Location"))
            {
                FindObjectOfType<LocationController>().CheckIntegrity();
            }
        }

        static bool TestDuplicateIdsForBuildingBlocks<T>() where T : CoreBuildingBlock
        {
            T[] nodeList = Finder.GetScriptableObjects<T>();
            int[] ids = new int[nodeList.Length];
            for (int i = 0; i < nodeList.Length; i++)
            {
                ids[i] = nodeList[i].uniqueId;
                if(ids[i]==-1)
                {
                    Assert.IsTrue(false, nodeList[i].name +"-"+ typeof(T).ToString() + ": id cant be -1");
                    return true;
                }
            }

            var duplicate = nodeList.Select(e => e.uniqueId)
               .GroupBy(x => x)
              .Where(g => g.Count() > 1)
              .Select(y => y.Key)
              .ToList();

            if (duplicate.Count() > 0)
            {
                foreach (var x in duplicate)
                {
                    T[] objectsToPing = nodeList.Where(y => y.uniqueId == x).ToArray();
                    foreach (var item in objectsToPing)
                    {
                        //!!this ping will select only the last duplicate if endOnFirstError is set to false
                        EditorGUIUtility.PingObject(item);
                        Assert.IsTrue(false, "duplicated " + typeof(T).ToString() + " nodes with name:" + x);
                        return true;
                    }

                }
                return true;
            }
            return false;
        }
        static bool TestDuplicateIdsForNodeType<T>() where T : Node
        {
            T[] nodeList = Finder.GetSubScriptableObjects<T>();

            //T[] nodeList = Finder.GetScriptableObjects<T>();
            string[] ids = new string[nodeList.Length];
            for (int i = 0; i < nodeList.Length; i++)
            {
                //Debug.Log(nodeList[i].uniqueId + " " + AssetDatabase.GetAssetPath(nodeList[i]));
                ids[i] = nodeList[i].uniqueId;
                if (ids[i] == "")
                {
                    Assert.IsTrue(false, nodeList[i].name + "-" + typeof(T).ToString() + ": id cant be empty");
                    return true;
                }
            }

            var duplicate = nodeList.Select(e => e.uniqueId)
               .GroupBy(x => x)
              .Where(g => g.Count() > 1)
              .Select(y => y.Key)
              .ToList();

            if (duplicate.Count() > 0)
            {
                foreach (var x in duplicate)
                {
                    T[] objectsToPing = nodeList.Where(y => y.uniqueId == x).ToArray();
                    foreach (var item in objectsToPing)
                    {
                        //!!this ping will select only the first duplicate if endOnFirstError is set to false
                        EditorGUIUtility.PingObject(item);
                        Assert.IsTrue(false, "duplicated " + typeof(T).ToString() + " nodes with name:" + x + " " + item.uniqueId);
                        return true;
                    }

                }
                return true;
            }
            return false;
        }
            
        /*
        static bool TestDuplicateIdsForNodeTypeOLD<T>() where T : Node
        {
            T[] nodeList = Finder.GetScriptableObjects<T>();
            string[] ids = new string[nodeList.Length];
            for (int i = 0; i < nodeList.Length; i++)
            {
                Debug.Log(nodeList[i].uniqueId + " " + AssetDatabase.GetAssetPath(nodeList[i]));
                ids[i] = nodeList[i].uniqueId;
                if (ids[i] == "")
                {
                    Assert.IsTrue(false, nodeList[i].name + "-" + typeof(T).ToString() + ": id cant be empty");
                    return true;
                }
            }
            
            var duplicate = nodeList.Select(e => e.uniqueId)
               .GroupBy(x => x)
              .Where(g => g.Count() > 1)
              .Select(y => y.Key)
              .ToList();

            if (duplicate.Count() > 0)
            {
                foreach (var x in duplicate)
                {
                    T[] objectsToPing = nodeList.Where(y => y.uniqueId == x).ToArray();
                    foreach (var item in objectsToPing)
                    {
                        //!!this ping will select only the last duplicate if endOnFirstError is set to false
                        EditorGUIUtility.PingObject(item);
                        Assert.IsTrue(false, "duplicated " + typeof(T).ToString() + " nodes with name:" + x+" "+item.uniqueId);
                        //return true;
                    }
                   
                }
                return true;
            }
            return false;
        }*/

        public static void TestUniqueness(bool endOnFirstError = true)
        {
            //TODO character loc, item ids
            Siren.EditorQuestion("testing uniqueness where needed");
            TestDuplicateIdsForNodeType<EntryPoint>();
            TestDuplicateIdsForNodeType<MultiOption>();

            TestDuplicateIdsForBuildingBlocks<LocationCore>();
            TestDuplicateIdsForBuildingBlocks<CharacterCore>();
            TestDuplicateIdsForBuildingBlocks<ItemCore>();
            
            /* MultiOption[] multiOptions =  Finder.GetScriptableObjects<MultiOption>();
             string[] multiOptionIds = new string[multiOptions.Length];
             for (int i = 0; i < multiOptions.Length; i++)
             {
                 multiOptionIds[i] = multiOptions[i].uniqueId;
             }

             var duplicate = multiOptions.Select(e=>e.uniqueId)
                .GroupBy(x => x)
               .Where(g => g.Count() > 1)
               .Select(y => y.Key)
               .ToList(); 

             if (duplicate.Count() > 0)
             {
                 foreach (var x in duplicate)
                 {
                     Assert.IsTrue(false, "duplicated MultipleOption nodes with name:" + x);
                     MultiOption[] objectsToPing = multiOptions.Where(y => y.uniqueId == x).ToArray();
                     foreach (var item in objectsToPing)
                     {
                         //!!this ping will select only the last duplicate if endOnFirstError is set to false
                         EditorGUIUtility.PingObject(item);
                     }
                     if (endOnFirstError)
                     {
                         return;
                     }

                 }
                 return;
             }*/
            //TODO zvuky atd
            Siren.EditorOkLog("uniqueness looks good");
        }

        public static void TestInternals(bool endOnFirstError = true)
        {
            Siren.EditorQuestion("internal objects checking?");

            CheckSingleScriptableObjectExistens<Inventory>();
            CheckSingleScriptableObjectExistens<ItemCombinationsManager>();
            CheckSingleScriptableObjectExistens<ReferenceLinkHelper>();
            CheckSingleScriptableObjectExistens<Settings>();

            //TODO others internals triggers, condions???
            Siren.EditorOkLog("internal objects look good");
            //EditorGUIUtility.PingObject(settings[0]);
        }

        static bool CheckSingleScriptableObjectExistens<T>() where T: ScriptableObject
        {
            T[] list = Finder.GetScriptableObjects<T>();

            if (list.Length == 1)
            {
                return true;
            }

            Assert.IsTrue(false, "internal objects failed - " + typeof(T).ToString()+" must have 1 instance  - " + list.Length + " found");
            return false;
            
        }

        public static void TestDialogs(bool endOnFirstError = true)
        {
            Siren.EditorQuestion("testing dialogs integrity");
            Dialog[] dialogList = Finder.GetScriptableObjects<Dialog>();
            //Debug.Log("dialogList" + dialogList.Length);
            Node corruptedNode;
            if (dialogList.Length > 0)
            {
                for (int i = 0; i < dialogList.Length; i++)
                {
                    corruptedNode = dialogList[i].FindCorruptedNode();
                    if (corruptedNode != null)
                    {
                        Assert.IsTrue(false, "dialogs integrity failed - in dialog " + dialogList[i].name + ", node " + corruptedNode.name + ", report: " + corruptedNode.corruptionReport);
                        if (endOnFirstError)
                        {
                            EditorGUIUtility.PingObject(dialogList[i]);
                            return;
                        }
                    }
                }
            }
            Siren.EditorOkLog("dialogs integrity look good");
        }



        public static void TestOneDialog(Dialog dialog, bool endOnFirstError = true)
        {
            dialogToTest = dialog;
            Siren.EditorQuestion("testing one dialog integrity");
            if (dialogToTest == null)
            {
                Assert.IsTrue(false, "no dialog to test");
                return;
            }
            Node corruptedNode;

            corruptedNode = dialogToTest.FindCorruptedNode();
            if (corruptedNode != null)
            {
                EditorGUIUtility.PingObject(dialogToTest);
                Assert.IsTrue(false, "dialogs integrity failed - in dialog " + dialogToTest.name + ", node " + corruptedNode.name + ", report: " + corruptedNode.corruptionReport);
                if (endOnFirstError)
                {
                    return;
                }
            }

            Siren.EditorOkLog("dialog integrity look good");
        }

        public static void TestItemCombinations(bool endOnFirstError = true, bool testWithDialogs = false)
        {
            Siren.EditorQuestion("testing item combinations integrity");            
            ItemCombinationsManager manager = Finder.GetLoneScriptableObjects<ItemCombinationsManager>();
            //-------------------------------------------------------------------------------------------
            ItemItemCombination corruptedItemEntry;
            Node corruptedNode;
            if (manager.itemEntries.Count > 0)
            {
                for (int i = 0; i < manager.itemEntries.Count; i++)
                {
                    if(manager.itemEntries[i].IsCorrupted())
                    {
                        corruptedItemEntry = manager.itemEntries[i];
                        EditorGUIUtility.PingObject(corruptedItemEntry);
                        Assert.IsTrue(false, "combination integrity failed - in combination " + corruptedItemEntry.name + ", report: " + corruptedItemEntry.corruptionReport);
                        if (endOnFirstError)
                        {
                            return;
                        }
                    }
                    else
                    {
                        if (testWithDialogs)
                        {
                            corruptedNode = manager.itemEntries[i].entryPoint.parentGraph.FindCorruptedNode();
                            if (corruptedNode != null)
                            {
                                corruptedItemEntry = manager.itemEntries[i];
                                EditorGUIUtility.PingObject(corruptedItemEntry);
                                Assert.IsTrue(false, "combination integrity failed - in combination " + corruptedItemEntry.name + ",dialog node: " + corruptedNode.uniqueId + ", report: " + corruptedNode.corruptionReport);
                                if (endOnFirstError)
                                {
                                     return;
                                }
                            }
                        }
                    }
                }
            }
            //-------------------------------------------------------------------------------------------
            ItemCharacterCombination corruptedCharacterEntry;

            if (manager.characterEntries.Count > 0)
            {
                for (int i = 0; i < manager.characterEntries.Count; i++)
                {
                    if (manager.characterEntries[i].IsCorrupted())
                    {
                        corruptedCharacterEntry = manager.characterEntries[i];
                        EditorGUIUtility.PingObject(corruptedCharacterEntry);
                        Assert.IsTrue(false, "combination integrity failed - in combination " + corruptedCharacterEntry.name + ", report: " + corruptedCharacterEntry.corruptionReport);
                        if (endOnFirstError)
                        {

                            return;
                        }
                    }
                    else
                    {
                        if (testWithDialogs)
                        {
                            corruptedNode = manager.characterEntries[i].entryPoint.parentGraph.FindCorruptedNode();
                            if (corruptedNode != null)
                            {
                                corruptedCharacterEntry = manager.characterEntries[i];
                                EditorGUIUtility.PingObject(corruptedCharacterEntry);
                                Assert.IsTrue(false, "combination integrity failed - in combination " + corruptedCharacterEntry.name + ",dialog node: " + corruptedNode.uniqueId + ", report: " + corruptedNode.corruptionReport);
                                if (endOnFirstError)
                                {
                                    return;
                                }
                            }
                        }
                    }
                }
            }
            Siren.EditorOkLog("item combinations looking good");
        }

        public static void TestExaminations(bool endOnFirstError = true)
        {
            ReferenceLinkHelper referenceLinkHelper = Finder.GetLoneScriptableObjects<ReferenceLinkHelper>();
            Siren.EditorQuestion("testing examinations integrity");

            CharacterCore[] chars = Finder.GetScriptableObjects<CharacterCore>();
            for (int i = 0; i < chars.Length; i++)
            {
                if(chars[i] == referenceLinkHelper.enviromentCharacter || chars[i] == referenceLinkHelper.playerCharacter)
                {
                    continue;
                }
                if (chars[i].currentExamineEntryPoint == null)
                {
                    EditorGUIUtility.PingObject(chars[i]);
                    Siren.EditorNotice("no examination entry point for " + chars[i].editorName+", purpously?");
                  /*  if (endOnFirstError)
                    {
                        return;
                    }*/
                }
            }

            ItemCore[] items = Finder.GetScriptableObjects<ItemCore>();
            for (int i = 0; i < items.Length; i++)
            {
                if(items[i].currentExamineEntryPoint==null)
                {
                    EditorGUIUtility.PingObject(items[i]);
                    Siren.EditorNotice("no examination entry point for " + items[i].editorName + ", purpously?");
                   
                    /*if (endOnFirstError)
                    {
                        return;
                    }*/
                }
            }


            //TODO for chars, items
            /*
            ExaminationsManager manager = Finder.GetLoneScriptableObjects<ExaminationsManager>();
            //-------------------------------------------------------------------------------------------
            ItemExamination corrupterEntry;
            Node corruptedNode;
            if (manager.itemEntries.Count > 0)
            {
                for (int i = 0; i < manager.itemEntries.Count; i++)
                {
                    if (manager.itemEntries[i].IsCorrupted())
                    {
                        corrupterEntry = manager.itemEntries[i];
                        EditorGUIUtility.PingObject(corrupterEntry);
                        Assert.IsTrue(false, "examinations integrity failed - in examination " + corrupterEntry.name + ", report: " + corrupterEntry.corruptionReport);
                        if (endOnFirstError)
                        {
                            return;
                        }
                    } else
                    {
                        if(testWithDialogs)
                        {
                            corruptedNode = manager.itemEntries[i].entryPoint.parentGraph.FindCorruptedNode();
                            if(corruptedNode!=null)
                            {
                                corrupterEntry = manager.itemEntries[i];
                                EditorGUIUtility.PingObject(corrupterEntry);
                                Assert.IsTrue(false, "examinations integrity failed - in examination " + corrupterEntry.name + ",dialog node: "+corruptedNode.uniqueId+", report: " + corruptedNode.corruptionReport);
                                if (endOnFirstError)
                                {
                                    return;
                                }
                            }
                        }
                    }
                }
            }
            //-------------------------------------------------------------------------------------------
            CharacterExamination corruptedCharacterEntry;

            if (manager.characterEntries.Count > 0)
            {
                for (int i = 0; i < manager.characterEntries.Count; i++)
                {
                    if (manager.characterEntries[i].IsCorrupted())
                    {
                        corruptedCharacterEntry = manager.characterEntries[i];
                        EditorGUIUtility.PingObject(corruptedCharacterEntry);
                        Assert.IsTrue(false, "examinations integrity failed - in examination " + corruptedCharacterEntry.name + ", report: " + corruptedCharacterEntry.corruptionReport);
                        if (endOnFirstError)
                        {
                            return;
                        }
                    }
                    else
                    {
                        if (testWithDialogs)
                        {
                            corruptedNode = manager.characterEntries[i].entryPoint.parentGraph.FindCorruptedNode();
                            if (corruptedNode != null)
                            {
                                corruptedCharacterEntry = manager.characterEntries[i];
                                EditorGUIUtility.PingObject(corruptedCharacterEntry);
                                Assert.IsTrue(false, "examinations integrity failed - in examination " + corruptedCharacterEntry.name + ", dialog node: " + corruptedNode.uniqueId + ", report: " + corruptedNode.corruptionReport);
                                if (endOnFirstError)
                                {
                                    return;
                                }
                            }
                        }
                    }
                }
            }*/
            Siren.EditorOkLog("examinations looking good");
        }
    }

}
#endif